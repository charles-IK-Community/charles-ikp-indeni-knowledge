package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, EndsWithRepetition, Equals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ClusterConfigNotSyncedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cluster_config_unsynced", "Clustered Devices: Cluster configuration not synced",
    "For devices that support full configuration synchronization, indeni will alert if the configuration is out of sync.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val tsToTestAgainst = TimeSeriesExpression[Double]("cluster-config-synced")
    val activeMemberValue = TimeSeriesExpression[Double]("cluster-member-active").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-config-synced", "cluster-member-active")),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(
              EndsWithRepetition(tsToTestAgainst, ConstantExpression(0.0), 3),
              Equals(activeMemberValue, ConstantExpression[Option[Double]](Some(1.0)))
            )
          ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The configuration has been changed on this device, but has not yet been synced to other members of the cluster. This may result in an unexpected behavior of other cluster members should this member go down."),
        ConstantExpression("Log into the device and synchronize the configuration across the cluster.")
    )
  }
}
