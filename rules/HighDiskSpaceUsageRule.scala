package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.AverageExpression
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class HighDiskSpaceUsageRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val excludeDisks = Set("/dev", "/mnt/cdrom", "/proc", "/dev/shm", "/dev/shm", "/junos", "/junos/dev")

  val highThresholdParameter: ParameterDefinition = new ParameterDefinition(
    "High_Threshold_of_Space_Usage",
    "",
    "High Threshold of Space Usage",
    "What is the threshold for the mount point's disk usage for which once it is crossed " + "an alert will be issued.",
    ParameterDefinition.UIType.DOUBLE,
    80.0)

  override def metadata: RuleMetadata =
    RuleMetadata("high_mountpoint_space_use_by_device",
                     "All Devices: High disk space utilization",
                     "Many systems store vital configuration and operational data on their " +
                       "storage devices at different mount points and on different " +
                       "disk devices. It is important to monitor the usage of the " +
                       "different storage devices to ensure they do not fill up and " +
                       "cause issues in the on-going operation of the system.",
                     AlertSeverity.ERROR,
                     highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {

    val diskUsage = AverageExpression(TimeSeriesExpression[Double]("disk-usage-percentage"))
    val fileSizeValue = TimeSeriesExpression[Double]("file-size").last

    val diskNotExcluded = ScopeValueExpression("file-system").visible().isIn(excludeDisks).not

    val diskUsageGreaterThanThreshold =
      GreaterThanOrEqual(diskUsage, getParameterDouble(context, highThresholdParameter))

    val filesSubInformer = MultiIssueInformer(
        headline = scopableStringFormatExpression("${scope(\"path\")}"),
        description = scopableStringFormatExpression("File size: %.0f bytes", fileSizeValue),
        title = "Large Files")
      .iterateOver(
        collection = SelectTimeSeriesExpression[Double](context.tsDao, Set("file-size")),
        condition = GreaterThanOrEqual(fileSizeValue, ConstantExpression[Option[Double]](Some(10000)))
      )

    val filesInformer = MultiIssueInformer(
      collection = SelectTagsExpression(context.tsDao, Set("path"), True),
      subInformer = filesSubInformer)

    StatusTreeExpression(
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      StatusTreeExpression(
        SelectTagsExpression(context.tsDao, Set("file-system"), True),
        And(
          diskNotExcluded,
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("disk-usage-percentage")),
            diskUsageGreaterThanThreshold
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"file-system\")}"),
              scopableStringFormatExpression("Current disk space utilization is: %.0f%%", diskUsage),
            // TODO this remediation info was not used. see if it is still needed (if not, then delete)
//              ConditionalRemediationSteps("Consider deleting files to free up space.",
//                ConditionalRemediationSteps.VENDOR_PANOS -> "Review https://live.paloaltonetworks.com/t5/Management-Articles/How-and-When-to-Clear-Disk-Space-on-the-Palo-Alto-Networks/ta-p/55736 and https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/FeaturedArticles/article-id/89"
//              ),
              title = "Affected Disks / File Systems"
          ).asCondition()
        ),
        multiInformers = Set(filesInformer)
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("Some disks or file systems are under high usage."),
      ConditionalRemediationSteps("Determine the cause for the high disk usage of the listed file systems.",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Review https://live.paloaltonetworks.com/t5/Management-Articles/How-and-When-to-Clear-Disk-Space-on-the-Palo-Alto-Networks/ta-p/55736 and https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/FeaturedArticles/article-id/89",
        ConditionalRemediationSteps.VENDOR_JUNIPER -> "Running \"request system storage cleanup\" will clean up certain old log files and debug data."
      )
    )
  }
}
