package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.Change
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ClusterMemberNoLongerActiveRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_cluster_member_no_longer_active", "Clustered Devices: Cluster member no longer active",
    "Alert if the cluster member used to be active and is now standby or down.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val stateTs = TimeSeriesExpression[Double]("cluster-member-active")

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("cluster-member-active")),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-member-active")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              Change(
                stateTs,
                ConstantExpression(Some(1.0)),
                ConstantExpression(Some(0.0)))

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                EMPTY_STRING,
                title = "Clustering Mechanisms Affected"
            ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("This cluster member used to be active. It no longer is."),
        ConstantExpression("Review the possible cause and troubleshoot.")
    )
  }
}
