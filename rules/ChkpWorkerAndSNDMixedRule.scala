package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ChkpWorkerAndSNDMixedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("chkp_worker_and_SND_mixed", "Check Point Firewalls: In CoreXL a single core shouldn't handle both interface interrupts and fw worker",
    "Best practices for CoreXL dictate that a single core should not be assigned to an interface AND an FW worker. indeni will alert if this is the case.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("corexl-core-assigned-workers-and-nics").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("corexl-core-assigned-workers-and-nics")),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Equals(
              inUseValue,
              ConstantExpression(Some(1.0)))
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The output of \"fw ctl affinity -l\" shows that at least one CPU is assigned to both interfaces and fw workers. This is contrary to Check Point's best practices."),
        ConstantExpression("Ensure that each CPU (such as CPU 0, CPU 1) only appears in lines which start with an interface name OR only appears in lines which start with \"fw_\".")
    )
  }
}
