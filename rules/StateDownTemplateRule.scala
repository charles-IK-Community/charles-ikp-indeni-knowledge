package com.indeni.server.rules.library

import com.indeni.data.SingleDimensionSnapshot
import com.indeni.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectSnapshotsExpression, SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.regex.RegexExpression
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

import scala.util.matching.Regex

class StateDownTemplateRule(context: RuleContext, ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                 severity: AlertSeverity = AlertSeverity.ERROR,
                                 metricName: String,
                                 applicableMetricTag: String = null, alertItemsHeader: String = null,
                                 descriptionMetricTag: String = null,
                                 descriptionStringFormat: String = "", alertDescription: String, baseRemediationText: String, metaCondition: TagsStoreCondition = True,
                                 alertIfDown: Boolean = true, itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""),
                                 itemsToIgnore: Set[Regex] = Set("^$".r),
                                 stateDescriptionComplexMetricName: String = null,
                                 howManyRepetitions: Int = 1)
                                (vendorToRemediationText: (String, String)*)
  extends PerDeviceRule with RuleHelper {

  val complexStateDescription = stateDescriptionComplexMetricName match {
    case s: String => new ScopableExpression[String] {

      private val selectStateDesc = SelectSnapshotsExpression(context.snapshotsDao, Set(s)).single()

      override protected def evalWithScope(time: Long, scope: Scope): String = {
        val snapshotSelect = selectStateDesc.eval(time)
        if (snapshotSelect.isEmpty) "" else snapshotSelect.head.getInvisible(s).asInstanceOf[Option[SingleDimensionSnapshot]].get.mostRecent.getOrElse("value", "")
      }

      override def args: Set[Expression[_]] = Set(selectStateDesc)
    }
    case _ => ConstantExpression("")
  }
  override val metadata: RuleMetadata = RuleMetadata(ruleName, ruleFriendlyName, ruleDescription, severity)

  override def expressionTree: StatusTreeExpression = {
    val currentValue = TimeSeriesExpression[Double](metricName).last
    val tsToTestAgainst = TimeSeriesExpression[Double](metricName)
    val stateToLookFor = ConstantExpression(if (alertIfDown) 0.0 else 1.0)
    val condition = EndsWithRepetition(tsToTestAgainst, stateToLookFor, howManyRepetitions)
    // If we need more than one repetition, we need more than the default history
    val history = if (howManyRepetitions == 1) SelectTimeSeriesExpression.DEFAULT_HISTORY_LENGTH else ConstantExpression(TimeSpan.fromHours(24))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

        // What constitutes an issue
        if (applicableMetricTag != null)
          StatusTreeExpression(

            // The additional tags we care about (we'll be including this in alert data)
            SelectTagsExpression(context.tsDao, if (descriptionMetricTag == null) Set(applicableMetricTag) else Set(applicableMetricTag, descriptionMetricTag), withTagsCondition(metricName)),

              StatusTreeExpression(
                // The time-series we check the test condition against:
                SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), historyLength = history),

                // The condition which, if true, we have an issue. Checked against the time-series we've collected
                And(
                  condition,
                  Or(
                    itemsToIgnore.map(r => ResultsFound(RegexExpression(ScopeValueExpression(applicableMetricTag).visible().asString(), r))).toSeq
                  ).not)

                // The Alert Item to add for this specific item
              ).withSecondaryInfo(
                  scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}" + (if (descriptionMetricTag != null) " (${scope(\"" + descriptionMetricTag + "\")})" else "")),
                  new ScopableExpression[String] {
                    val innerDescription = scopableStringFormatExpression(descriptionStringFormat, currentValue)

                    override protected def evalWithScope(time: Long, scope: Scope): String = {
                      val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
                      val descriptionTagValue = if (descriptionMetricTag != null) scope.getVisible(descriptionMetricTag).get.toString else ""

                      innerDescription.eval(time) + itemSpecificDescription.collectFirst {
                        case item if (item._1.findFirstMatchIn(metricTagValue).nonEmpty || item._1.findFirstMatchIn(descriptionTagValue).nonEmpty) => item._2
                      }.get + complexStateDescription.eval(time)
                    }

                    override def args: Set[Expression[_]] = Set(innerDescription, complexStateDescription)
                  },
                  title = alertItemsHeader
              ).asCondition()
          ).withoutInfo().asCondition()
        else {
          val treeFactory = StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), historyLength = history),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            condition
          )
          val tree = if (complexStateDescription != null) {
            treeFactory.withSecondaryInfo(
              complexStateDescription,
              EMPTY_STRING,
              title = "Status Description"
            )
          } else {
            treeFactory.withoutInfo()
          }
          tree.asCondition()
        }

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression(alertDescription),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
    )
  }
}


