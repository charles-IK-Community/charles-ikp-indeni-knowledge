package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class ContractExpirationRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before expiration should indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(56))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_contract_expiration", "All Devices: Contract(s) about to expire",
    "indeni will alert when a contract is about to expire or has expired.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("contract-expiration").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("contract-expiration")),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("contract-expiration")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              LesserThan(
                actualValue,
                PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(context, highThresholdParameter)))

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                scopableStringFormatExpression("Will expire on %s", doubleToDateExpression(actualValue)),
                title = "Affected Contracts"
            ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more contracts have expired or are about to expire. See the list below."),
        ConditionalRemediationSteps("Renew any contracts that need to be renewed.",
          ConditionalRemediationSteps.VENDOR_CP -> "Make sure you have purchased the required contracts and have updated them in your management server: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33089",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Review this page on licensing: https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/getting-started/activate-licenses-and-subscriptions"
        )
    )
  }
}
