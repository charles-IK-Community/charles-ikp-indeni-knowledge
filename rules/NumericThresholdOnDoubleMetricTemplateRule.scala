package com.indeni.server.rules.library

import com.indeni.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class NumericThresholdOnDoubleMetricTemplateRule(context: RuleContext, ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                                      severity: AlertSeverity = AlertSeverity.ERROR,
                                                      metricName: String, threshold: Any, metaCondition: TagsStoreCondition = True,
                                                      alertDescriptionFormat: String, baseRemediationText: String, thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE)
                                                     (vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  private[library] val thresholdParameterName = "threshold"
  private val thresholdParameter = new ParameterDefinition(thresholdParameterName,
    "",
    "Alerting Threshold",
    "indeni will alert if the value is " + (if (thresholdDirection == ThresholdDirection.ABOVE) "above" else "below") + " this value.",
    UIType.fromObjectClass(threshold.getClass),
    threshold)

  override val metadata: RuleMetadata = RuleMetadata(ruleName, ruleFriendlyName, ruleDescription, AlertSeverity.ERROR, thresholdParameter)

  override def expressionTree: StatusTreeExpression = {

    val value = TimeSeriesExpression[Double](metricName).last
    val thresholdValue = getParameter(context, thresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName)),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            generateCompareCondition(
              thresholdDirection,
              value,
              thresholdValue)

            // The Alert Item to add for this specific item
          ).withRootInfo(
              getHeadline(),
              scopableStringFormatExpression(alertDescriptionFormat, value),
              ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
        ).asCondition()
    ).withoutInfo()
  }
}
