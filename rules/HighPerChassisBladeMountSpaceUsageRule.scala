package com.indeni.server.rules.library

import com.indeni.data.conditions.{Equals, True}
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.AverageExpression
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

/**
  * Created by amir on 04/02/2016.
  */
case class HighPerChassisBladeMountSpaceUsageRule(context: RuleContext) extends PerDeviceRule {

  private val remediation = "Review the contents of the mount points to see what can be deleted " +
    "or moved and attempt to identify whether there's a specific cause for this."

  private val excludeDisks = Set("/dev", "/mnt/cdrom", "/proc", "/dev/shm", "/dev/shm")

  private[library] val highThresholdParameterName: String = "High_Threshold_of_Space_Usage"

  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Space Usage",
    "What is the threshold for the mount point's disk usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    80.0)

  override val metadata: RuleMetadata = RuleMetadata("high_per_chassis_blade_mount_space", "High Mount Space per Chassis and Blade", "Alert when Disk usage is high", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    import HighPerChassisBladeMountSpaceUsageRule._

    val usagePercentage = AverageExpression(TimeSeriesExpression[Double](DiskUsagePercentageTimeSeries))
    val usagePercentageThreshold = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, highThresholdParameter.getName, ConfigurationSetId, highThresholdParameter.getDefaultValue.asDouble.toDouble).noneable
    val isUsagePercentageAboveThreshold = GreaterThanOrEqual(usagePercentage, usagePercentageThreshold)

    val shouldCheckDisk = ScopeValueExpression(FileSystemKey).visible().isIn(excludeDisks).not

    val isDiskWithIssue = com.indeni.ruleengine.expressions.conditions.And(isUsagePercentageAboveThreshold, shouldCheckDisk)

    val mountSpaceFailDescription = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String =
        "Storage usage (" + "%.2f".format(usagePercentage.eval(time).get) + "%) above threshold (" + "%.2f".format(usagePercentageThreshold.eval(time).get) + "%) " +
          "for chassis: " + scope.getVisible(ChassisKey).get + ", blade: " + scope.getVisible(BladeKey).get + ", mount point: " + scope.getVisible(FileSystemKey).get

      override def args: Set[Expression[_]] = Set(usagePercentage, usagePercentageThreshold)
    }
    val mountSpaceFailHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = "chassis: " + scope.getVisible(ChassisKey).get + ", blade: " + scope.getVisible(BladeKey).get + ", mount point: " + scope.getVisible(FileSystemKey).get

      override def args: Set[Expression[_]] = Set()
    }

    val tsQuery = SelectTimeSeriesExpression[Double](context.tsDao, Set(DiskUsagePercentageTimeSeries))
    val forTsCondition = StatusTreeExpression(tsQuery, isDiskWithIssue).withSecondaryInfo(
      mountSpaceFailHeadline, mountSpaceFailDescription, title = "Problematic Mount Points"
    ).asCondition()

    val disksQuery = SelectTagsExpression(context.tsDao, Set(ChassisKey, BladeKey, FileSystemKey), True)
    val highMountSpacePerDevicePerDiskLogic = StatusTreeExpression(disksQuery, forTsCondition).withoutInfo().asCondition()

    val headline = ConstantExpression("High storage usage has been measured")
    val description = ConstantExpression("Some mounts/drives have reached a high level of storage use. This may result in system failure in the near future.")

    val devicesFilter = Equals(HighPerChassisBladeMountSpaceUsageRule.ModelKey, HighPerChassisBladeMountSpaceUsageRule.ModelValue)
    val devicesQuery = SelectTagsExpression(context.metaDao, Set(DeviceKey), devicesFilter)

    StatusTreeExpression(devicesQuery, highMountSpacePerDevicePerDiskLogic).withRootInfo(
      headline, description, ConstantExpression(remediation)
    )
  }
}

object HighPerChassisBladeMountSpaceUsageRule {
  val ModelKey = "model"
  val ModelValue = "CheckPoint61k"

  val BladeKey = "Blade"
  val ChassisKey = "Chassis"
  val FileSystemKey = "file-system"
  val DiskUsagePercentageTimeSeries = "disk-usage-percentage"
}
