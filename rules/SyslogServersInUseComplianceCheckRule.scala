package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility.SeqDiffWithoutOrderExpression
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.{RuleMetadata, _}
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * Created by amir on 19/02/2017.
  */
case class SyslogServersInUseComplianceCheckRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  import SyslogServersInUseComplianceCheckRule._

  private[library] val linesParameterName = "syslog_config_lines"
  private val linesParameter = new ParameterDefinition(
    linesParameterName,
    "",
    "Syslog servers config needed (multi-line, <ip-address>, <min-severity>)",
    "List the syslog servers need to be configured, and their minimal severity.\ne.g. 1.1.1.1, info.",
    UIType.MULTILINE_TEXT,
    "")


  override def expressionTree: StatusTreeExpression = {
    val linesExpected = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, linesParameter.getName, ConfigurationSetId, Seq[Seq[String]]()).withLazy

    val snapshotKey = "syslog-servers"
    val missings =
      SeqDiffWithoutOrderExpression(
        MultiSnapshotExtractVectorExpression(SnapshotExpression(snapshotKey).asMulti().mostRecent(), "host", "severity"),
        linesExpected
      ).missings
        .withLazy

    val missingLineHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible(SyslogConfigTag).get.toString

      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(snapshotKey)).multi(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        missings.nonEmpty,
        multiInformers = Set(
          MultiIssueInformer(
            missingLineHeadline,
            EMPTY_STRING,
            "Missing configuration"
          ).iterateOver(
            missings,
            SyslogConfigTag,
            conditions.True
          ))

        // Details of the alert itself
      ).withRootInfo(
          getHeadline(),
          ConstantExpression("indeni has found that some syslog servers are missing or misconfigured. " +
            "These are listed below."),
          ConstantExpression("Modify the device's configuration as required.")
      ).asCondition()
    ).withoutInfo()
  }

  /**
    * @return The rule's metadata.
    */
  override def metadata: RuleMetadata =
    RuleMetadata(
      "verification_syslog_servers_in_use",
      "Compliance Check: Syslog Servers In Use",
      "indeni can verify that certain syslog servers are configured on a monitored device.",
      AlertSeverity.ERROR,
      AutomationPolicyItemAlertSetting.NEVER,
      linesParameter
    )
}


object SyslogServersInUseComplianceCheckRule {
  val SyslogConfigTag = "syslogConfig"
}
