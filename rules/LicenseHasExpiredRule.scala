package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class LicenseHasExpiredRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_license_has_expired", "All Devices: License expired",
    "indeni will alert when a license has expired.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("license-expiration").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("license-expiration")),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("license-expiration")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              LesserThan(
                actualValue,
                NowExpression())

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                scopableStringFormatExpression("Expired on %s", doubleToDateExpression(actualValue)),
                title = "Affected Licenses"
            ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more licenses have expired. See the list below."),
        ConditionalRemediationSteps("Renew any licenses that need to be renewed.",
          ConditionalRemediationSteps.VENDOR_CP -> "Make sure you have purchased the required licenses and have updated them in your management server: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33089",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Review this page on licensing: https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/getting-started/activate-licenses-and-subscriptions"
        )
    )
  }
}
