package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.Context
import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.config.DynamicParameterExpression
import com.indeni.ruleengine.expressions.core.{StatusCoreInformerExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectSnapshotsExpression, SelectTagsExpression, SingleSnapshotExtractExpression, SnapshotExpression}
import com.indeni.ruleengine.expressions.casting.ToBooleanExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * Created by amir on 19/02/2017.
  */
case class CoredumpingEnabledComplianceCheckRule(context: Context) extends PerDeviceRule with RuleHelper {


  private[library] val coredumpingEnabledName = "coredumping_state"
  private val coredumpingEnabled = new ParameterDefinition(
    coredumpingEnabledName,
    "",
    "Should Core Dumping Be Enabled",
    "If this is set to on or ticked, indeni will alert when a device that supports core dumping does not have core dumping enabled.",
    UIType.BOOLEAN,
    true)


  override def expressionTree: StatusTreeExpression = {
    val expected = DynamicParameterExpression.withConstantDefault(context.parametersService, metadata.name, coredumpingEnabled.getName, ConfigurationSetKey, false).noneable.withLazy
    val isEnabled = ToBooleanExpression(SingleSnapshotExtractExpression(SnapshotExpression("coredumping-enabled").asSingle().mostRecent(), "value")).withLazy
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("coredumping-enabled")).single(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        Equals(isEnabled, expected).not,
        // Details of the alert itself
        StatusCoreInformerExpression(
          getHeadline(),
          scopableStringFormatExpression("Core dumping configured is not as expected:\nExpected: %s, Actual: %s.", expected, isEnabled),
          ConditionalRemediationSteps("",
            ConditionalRemediationSteps.VENDOR_CP -> "Follow SK53363.",
            ConditionalRemediationSteps.VENDOR_CISCO -> "Use the \"coredump enable\" command.",
            ConditionalRemediationSteps.VENDOR_OTHER -> "Refer to your device's vendor documentation for details on how to change the coredump settings."
          ))
      ).asCondition()
    )
  }

  /**
    * @return The rule's metadata.
    */
  override def metadata: RuleMetadata =
    RuleMetadata(
      "verification_coredumping_enablement_compliance_check",
      "Compliance Check: Coredumping setting not as desired",
      "indeni can verify that coredumping is specifically enabled or disabled on a monitored device.",
      AlertSeverity.ERROR,
      Set(coredumpingEnabled),
      defaultAction = Some(AutomationPolicyItemAlertSetting.NEVER)
    )
}
