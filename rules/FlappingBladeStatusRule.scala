package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.FlappingDetection
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

/**
  * Created by amir on 05/02/2016.
  */
case class FlappingBladeStatusRule(context: Context) extends PerDeviceRule with RuleHelper {

  override val executionInterval: TimeSpan = TimeSpan.fromSeconds(60)

  private[library] val numOfFlappingParameterName = "num_of_flaps"

  private val numOfFlappingParameter = new ParameterDefinition(numOfFlappingParameterName,
    "",
    "Number of Flaps",
    "The number of flaps detected in blade state before an alert is issued.",
    UIType.INTEGER,
    2)

  private[library] val reviewedTimeframeParameterName: String = "reviewed_timeframe"

  private val reviewedTimeframeParameter = new ParameterDefinition(reviewedTimeframeParameterName,
    "",
    "Reviewed Timeframe", "The amount of time to detect changes.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(60))

  override val metadata: RuleMetadata = RuleMetadata("flapping_blade_status", "Chassis Devices: Blade(s) status is flapping", "Alert when the blade status is flapping between up and down.",
    AlertSeverity.WARN, numOfFlappingParameter, reviewedTimeframeParameter)

  override def expressionTree: StatusTreeExpression = {
    val sgmStateTs = TimeSeriesExpression[Double]("blade-state")

      StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), True),

          StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("blade-state")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              FlappingDetection(sgmStateTs, getParameterInt(context, numOfFlappingParameter))

              // The Alert Item to add for this specific item
          ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                EMPTY_STRING,
                title = "Blades Affected"
            ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
      ).withRootInfo(
          getHeadline(),
          ConstantExpression("One or more chassis blades have switched multiple times between ACTIVE and DOWN/STANDBY."),
          ConstantExpression("Review the cause for the blades flapping.")
    )
  }
}
