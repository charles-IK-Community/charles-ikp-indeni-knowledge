package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class F5ServiceCheckRule(context: Context) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before expiration should indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(56))

  override val metadata: RuleMetadata = RuleMetadata("f5_service_check", "F5 Devices: Service check approaching",
    "indeni will alert several weeks before a service check occurs on F5 devices.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("f5-service-check").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("f5-service-check")),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          LesserThan(
            actualValue,
            PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(context, highThresholdParameter)))

          // The Alert Item to add for this specific item
        ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("The service check for this device is on %s. Upgrades post the service check date may fail.", doubleToDateExpression(actualValue)),
            ConstantExpression("Contact your F5 reseller.")
        ).asCondition()
    ).withoutInfo()
  }
}
