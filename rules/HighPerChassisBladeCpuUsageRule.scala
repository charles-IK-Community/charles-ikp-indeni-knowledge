package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.MinExpression
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

/**
  * Created by amir on 03/02/2016.
  */
case class HighPerChassisBladeCpuUsageRule(context: RuleContext) extends PerDeviceRule {

  import HighPerChassisBladeCpuUsageRule._

  private[library] val highThresholdParameterName = "High_Threshold_of_CPU_Usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of CPU Usage",
    "What is the threshold for the CPU usage for which once it is crossed an alert will be issued. The CPU usage must be above this threshold constantly for a certain time frame in order for an alert to be issued.",
    UIType.DOUBLE,
    70.0)

  private[library] val numOfCpusParameterName = "higher_than_threshold_cpues"
  private val numOfCpusParameter = new ParameterDefinition(numOfCpusParameterName,
    "",
    "Number of CPUs",
    "The number of CPUs with usage above the value set in " + "\"" + highThresholdParameter.getFriendlyName + "\"" + " before an alert is issued.",
    UIType.INTEGER,
    1)

  private[library] val reviewedTimeframeParameterName: String = "reviewed_timeframe"

  private val reviewedTimeframeParameter = new ParameterDefinition(reviewedTimeframeParameterName,
    "",
    "Reviewed Timeframe",
    "The CPU usage must be above the value set in " + "\"" + highThresholdParameter.getFriendlyName + "\" for this amount of time before an alert is issued.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(10))

  override val metadata: RuleMetadata = RuleMetadata("high_per_chassis_blade_cpu_usage", "High CPU Usage per Chassis and Blade", "Alert when CPU usage is high.", AlertSeverity.ERROR, highThresholdParameter, numOfCpusParameter, reviewedTimeframeParameter)

  override def expressionTree: StatusTreeExpression = {

    val usageHistory = TimeSeriesExpression[Double](CpuUsageTimeSeries)
    val minimumUsage = MinExpression(usageHistory)
    val usageThreshold = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, highThresholdParameter.getName, ConfigurationSetId, highThresholdParameter.getDefaultValue.asDouble.toDouble).noneable
    val isUsageAboveThreshold = GreaterThanOrEqual(minimumUsage, usageThreshold)

    val cpuFailDescription = new ScopableExpression[String] {

      override protected def evalWithScope(time: Long, scope: Scope): String =
        "Cpu usage (" + minimumUsage.eval(time).get.round + "%) above threshold (" + usageThreshold.eval(time).get +
          "%) of " + scope.getVisible(CpuKey).get + " of chassis " + scope.getVisible(ChassisKey).get + ", blade " + scope.getVisible(BladeKey).get

      override def args: Set[Expression[_]] = Set(minimumUsage, usageThreshold)
    }

    val cpuFailHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = "chassis " + scope.getVisible(ChassisKey).get + ", blade " + scope.getVisible(BladeKey).get + ", cpu " + scope.getVisible(CpuKey).get

      override def args: Set[Expression[_]] = Set()
    }

    val historyLength = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, reviewedTimeframeParameter.getName, ConfigurationSetId, reviewedTimeframeParameter.getDefaultValue.asTimeSpan)
    val tsQuery = SelectTimeSeriesExpression[Double](context.tsDao, Set(CpuUsageTimeSeries), historyLength)

    val forTsCondition = StatusTreeExpression(tsQuery, isUsageAboveThreshold).withSecondaryInfo(
      cpuFailHeadline, cpuFailDescription, title = "Blades with High CPU Usage"
    ).asCondition()

    val cpusQuery = SelectTagsExpression(context.tsDao, Set(BladeKey, ChassisKey, CpuKey), True)
    val minimumCpusWithIssue = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, numOfCpusParameter.getName, ConfigurationSetId, numOfCpusParameter.getDefaultValue.asInteger().toInt)

    val loopOnCpus = StatusTreeExpression(cpusQuery, forTsCondition).withoutInfo().asCondition(minimumIssueCount = minimumCpusWithIssue)

    val headline = ConstantExpression("High CPU usage of specific blade CPUs")
    val description = ConstantExpression("Some CPUs are under high usage.")
    val remediation = ConstantExpression("Determine the cause for the high CPU usage of the listed CPUs.")

    val devicesFilter = Equals(HighPerChassisBladeCpuUsageRule.ModelKey, HighPerChassisBladeCpuUsageRule.ModelValue)
    val devicesQuery = SelectTagsExpression(context.metaDao, Set(DeviceKey), devicesFilter)

    StatusTreeExpression(devicesQuery, loopOnCpus).withRootInfo(
      headline, description, remediation
    )
  }
}

object HighPerChassisBladeCpuUsageRule {

  val ModelKey = "model"
  val ModelValue = "CheckPoint61k"

  val BladeKey = "Blade"
  val ChassisKey = "Chassis"
  val CpuKey = "cpu-id"
  val CpuIsAvgKey = "cpu-is-avg"
  val CpuUsageTimeSeries = "cpu-usage"
}
