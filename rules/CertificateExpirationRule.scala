package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, _}
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class CertificateExpirationRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before expiration should indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(56))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_certificate_expiration", "All Devices: Certificate(s) about to expire, or expired",
    "indeni will alert when a certificate is about to expire or has expired.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("certificate-expiration").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("certificate-expiration")),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("certificate-expiration")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              LesserThan(
                actualValue,
                PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(context, highThresholdParameter)))

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                scopableStringFormatExpression("Will expire on %s", doubleToDateExpression(actualValue)),
                title = "Affected Certificates"
            ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more certificates have expired or are about to expire. See the list below.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/itzik-assaraf/2/870/1b5\">Itzik Assaraf</a> (Leumi Card)."),
        ConditionalRemediationSteps("Renew any certificates that need to be renewed.",
          ConditionalRemediationSteps.VENDOR_CP -> "Read sk104400 (https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk104400) and the SK articles to which it links at the bottom.",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Read https://www.paloaltonetworks.com/documentation/60/pan-os/pan-os/certificate-management/revoke-and-renew-certificates"
        )
    )
  }
}
