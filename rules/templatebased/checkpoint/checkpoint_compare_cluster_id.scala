package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class checkpoint_compare_cluster_id(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_cluster_id",
  ruleFriendlyName = "Check Point Cluster: Cluster ID mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the cluster ID settings are different.",
  metricName = "cluster-id-number",
  isArray = false,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same cluster ID set.",
  baseRemediationText = """Follow https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk25977 to ensure the cluster ID is configured the same on both members.""")()
