package com.indeni.server.rules.library.templatebased

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityWithItemsTemplateRule

case class f5_snatpool_exhaustion(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "f5_snatpool_exhaustion",
  ruleFriendlyName = "F5 Devices: SNAT pool near maximum allocated",
  ruleDescription = "Source Network Address Translation (SNAT) is used by an F5 load balancer to allocate a dedicated IP and port for a connection facing a pool member. This allows the pool member to return the traffic to the correct session. SNAT uses ports to do the translation, and there is a limit to the number of ports one can use concurrently for a given IP address. Indeni will alert when the SNAT pool is nearing its capacity.",
  usageMetricName = "lb-snatpool-items",
  limitMetricName = "lb-snatpool-limit",
  applicableMetricTag = "name",
  threshold = 80.0,
  alertDescription = "Some SNAT pools are nearing their capacity. Capacity is calculated as 64000 multiplied by the number of IP addresses in the pool. If the traffic flow increases and the pool(s) reach capacity, traffic will be dropped.\n\nThis alert was added per the request of a senior network engineer at a Fortune 50 financial services company.",
  alertItemDescriptionFormat = "The number of ports in use is %.0f where the limit is %.0f.",
  baseRemediationText = "Monitor the pool(s) utilization. Investigate the source of traffic increase and consider adding an IP to the pool if needed. For more information read https://support.f5.com/csp/article/K33355231 (the log message referred to in the article will be generated at 100% of pool utilization)",
  alertItemsHeader = "Affected Pools")()
