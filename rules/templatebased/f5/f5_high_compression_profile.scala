package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{NumericThresholdOnComplexMetricWithItemsTemplateRule, ThresholdDirection}

/**
  *
  */
case class f5_high_compression_profile(context: RuleContext) extends NumericThresholdOnComplexMetricWithItemsTemplateRule(context,
  ruleName = "f5_high_compression_profile",
  ruleFriendlyName = "F5 Devices: Compression profile gzip level too high",
  ruleDescription = "Setting a higher compression level makes compressed content smaller, but the cost of higher CPU usage and longer time to compress the content. The difference in terms of percentage gets lower the higher the level and setting this too high is not recommended. indeni will alert if the gzip compression level is too high.",
  metricName = "f5-high-compression-profile-high-gzip-level",
  threshold = 6.0,
  thresholdDirection = ThresholdDirection.ABOVE,
  applicableMetricTag = "name",
  alertItemsHeader = "Affected Profiles",
  alertDescription = "Setting a higher compression level makes compressed content smaller, but the cost of higher CPU usage and longer time to compress the content. The difference in terms of percentage gets lower the higher the level and setting this too high is not recommended. In general the increments after level 4 comes in the ranges of 0.15-0.25%. Of course this depends on the content being served.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  alertItemDescriptionFormat = "The compression level used is %.0f",
  baseRemediationText = "Set the compression level to a value RuleEquals, or below 6 for the reported profiles\nhttps://support.f5.com/csp/article/K3393")()
