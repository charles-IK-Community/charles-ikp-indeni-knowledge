package com.indeni.server.rules.library.templatebased.f5

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_cookie_persistence(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_cookie_persistence",
  ruleFriendlyName = "F5 Devices: Unencrypted cookie persistence profiles found",
  ruleDescription = "According to best practices, cookies should be encrypted when persisting to client browser to avoid security issues. indeni will alert when this is not the case.",
  metricName = "f5-cookied-persistence-encrypted",
  applicableMetricTag = "name",
  alertItemsHeader = "Profiles Affected",
  alertDescription = "Some cookie persistence profiles do not have an encryption string configured. not encrypting persistence cookies discloses internal information such as internal IP, port and pool name. This information could be used by an attacker to gather information about your environment.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Review these instructions on how to enable persistence cookie encryption: \nhttps://support.f5.com/csp/article/K14784\n\nIt is best not to change the default profiles. Instead, create a new persistence profile with the default profile as parent. Cookie Encryption Use Policy should be set to Required in order for this alert not to be triggered.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("false"), SnapshotExpression("f5-cookied-persistence-encrypted").asSingle().mostRecent().noneable))()
