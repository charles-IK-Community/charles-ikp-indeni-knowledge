package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class f5_vcmp_disk_usage(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "f5_vcmp_disk_usage",
  ruleFriendlyName = "F5 Devices (vCMP): High disk utilization by vCMP",
  ruleDescription = "indeni will alert if the disk utilization of a vCMP is above a high threshold.",
  usageMetricName = "f5-vcmp-host-disk-usage-percentage",
  threshold = 80.0,
  alertDescriptionFormat = "The disk utilization is currently %.0f%%, when disk space runs out it is no longer possible to create new vCMP guests.",
  baseRemediationText = "Run \"show vcmp virtual-disk kil\" to see the full data per vCMP. Purchase additional hardware or delete unused virtual disks. More information about virtual disk management can be found at https://support.f5.com/kb/en-us/products/big-ip_ltm/manuals/product/vcmp-viprion-configuration-11-4-1/7.html")()
