package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_cluster_down_novsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_cluster_down_novsx",
  ruleFriendlyName = "Clustered Devices (Non-VS): Cluster down",
  ruleDescription = "indeni will alert if a cluster is down or any of the members are inoperable.",
  metricName = "cluster-state",
  applicableMetricTag = "name",
  metaCondition = !DataEquals("vsx", "true"),
  alertItemsHeader = "Clustering Elements Affected",
  alertDescription = "One or more clustering elements in this device are down.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = "Review the cause for one or more members being down or inoperable.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review other alerts for a cause for the cluster failure.",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device over SSH and run \"less mp-log ha-agent.log\" for more information."
)
