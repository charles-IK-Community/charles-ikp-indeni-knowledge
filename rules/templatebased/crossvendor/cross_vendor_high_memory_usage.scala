package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class cross_vendor_high_memory_usage(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_high_memory_usage",
  ruleFriendlyName = "All Devices: High memory usage",
  ruleDescription = "indeni will alert if the memory utilization of a device is above a high threshold. If the device has multiple memory elements, each will be inspected separately and alert for.",
  usageMetricName = "memory-usage",
  applicableMetricTag = "name",
  threshold = 92.0,
  alertDescription = "Some memory elements are nearing their maximum capacity.",
  alertItemDescriptionFormat = "Current memory utilization is: %.0f%%",
  baseRemediationText = "Determine the cause for the high memory usage of the listed elements.",
  alertItemsHeader = "Memory Elements Affected",
  itemsToIgnore = Set("^vCMP host - (swap|linux).*".r, "^PA firewall management plane.*".r))(
  ConditionalRemediationSteps.VENDOR_CP -> "Consider reading https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33781#MEMORY",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Consider opening a support ticket with Palo Alto Networks.",
  ConditionalRemediationSteps.VENDOR_CISCO -> "Review http://docwiki.cisco.com/wiki/Cisco_Nexus_7000_Series_NX-OS_Troubleshooting_Guide_--_Troubleshooting_Memory"
)
