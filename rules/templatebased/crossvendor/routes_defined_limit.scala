package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class routes_defined_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "routes_defined_limit",
  ruleFriendlyName = "All Devices: Maximum number of routes nearing (IPv4)",
  ruleDescription = "Many devices have a limit for the number of IPv4 routes that can be defined. indeni will alert prior to the number of routes reaching the limit.",
  usageMetricName = "routes-usage",
  limitMetricName = "routes-limit",
  threshold = 80.0,
  alertDescriptionFormat = "There are %.0f IPv4 routes defined where the limit is %.0f.",
  baseRemediationText = "Consider removing certain routes.")()
