package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_compare_osversion(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_osversion",
  ruleFriendlyName = "Clustered Devices: OS version mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the OS installed is different.",
  metricName = "os-version",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same OS's installed (including the same version).\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = """Install the correct versions of software on each device.""")()
