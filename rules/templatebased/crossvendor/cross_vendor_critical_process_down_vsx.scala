package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.time.TimeSpan

/**
  *
  */
case class cross_vendor_critical_process_down_vsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_critical_process_down_vsx",
  ruleFriendlyName = "All Devices: Critical process(es) down (per VS)",
  ruleDescription = "Many devices have critical processes, usually daemons, that must be up for certain functions to work. indeni will alert if any of these goes down.",
  metricName = "process-state",
  applicableMetricTag = "process-name",
  descriptionMetricTag = "vs.name",
  alertItemsHeader = "Processes Affected",
  alertDescription = "One or more processes which are critical to the operation of this device, are down.",
  baseRemediationText = "Review the cause for the processes being down.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Check if \"cpstop\" was run."
)
