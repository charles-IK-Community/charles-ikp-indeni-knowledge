package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_debug_on(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_debug_on",
  ruleFriendlyName = "All Devices: Debug mode enabled",
  ruleDescription = "indeni will alert if one of the debug mechanisms on a device is enabled when the default is for it to be disabled.",
  metricName = "debug-status",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Debugs Enabled",
  alertDescription = "One or more debug flags or components is enabled. Leaving debug on for too long may result in performance issues.",
  baseRemediationText = "Turn off the debug as soon as possible.",
  itemSpecificDescription = Seq(
    "^watchdog$".r -> "In the event of a system lock-up, the watchdog process ensures that the BIG-IP system restarts and fails over. In order to force the BIG-IP system to produce a core file for diagnostic purposes, administrators must disable the watchdog process to allow the core file to be written to disk before the system restarts. Re-enable the watchdog.",
        "^mcpd-force-reload$".r -> "If /service/mcpd/forceload exists any reboot would take longer than usual. In case of an outage together with a degraded cluster this could mean increased downtime in case a cluster member is restarted.",
        "^tm\\.rstcause\\.log$".r -> "Enabling RST cause logging uses additional system resources when connections are reset. This can be used for additional traction by an attacked performing a DDOS attack. This is not recommended to leave enabled unless it is for troubleshooting purposes.",
        "^tm\\.rstcause\\.pkt$".r -> "Enabling RST cause information in the packet payload may disclose details about your environment to a potential attacker. This is not recommended to leave enabled unless it is for troubleshooting purposes.",
    ".*".r -> ""
  )
)(
  ConditionalRemediationSteps.VENDOR_F5 -> "Follow the applicable remediation steps. mcpd-force-reload: Delete the file /service/mcpd/forceload (https://support.f5.com/csp/article/K13030), tm.rstcause.log: https://support.f5.com/csp/article/K13223, tm.rstcause.pkt: https://support.f5.com/csp/article/K13223 ",
  ConditionalRemediationSteps.VENDOR_CP -> "If the above list includes kernel debugging, run \"fw ctl debug 0\" to clear the debugs.",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device using SSH, type \"debug \" and then begin typing the items listed above. Usually the last term in the command can be replaced with a \"show\" or something similar to identify the current settings."
)
