package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityWithItemsTemplateRule

/**
  *
  */
case class cross_vendor_rx_drop(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_rx_drop",
  ruleFriendlyName = "All Devices: RX packets dropped",
  ruleDescription = "indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-rx-dropped",
  limitMetricName = "network-interface-rx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high drop rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f dropped packets identified out of a total of %.0f received.",
  baseRemediationText = "Packet drops usually occur when the rate of packets received is higher than the device's ability to handle.",
  alertItemsHeader = "Affected Ports")()
