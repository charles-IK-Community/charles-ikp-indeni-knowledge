package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class routes_defined_limit_ipv6(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "routes_defined_limit_ipv6",
  ruleFriendlyName = "All Devices: Maximum number of routes nearing (IPv6)",
  ruleDescription = "Many devices have a limit for the number of IPv6 routes that can be defined. indeni will alert prior to the number of routes reaching the limit.",
  usageMetricName = "routes-usage-ipv6",
  limitMetricName = "routes-limit-ipv6",
  threshold = 80.0,
  alertDescriptionFormat = "There are %.0f IPv6 routes defined where the limit is %.0f.",
  baseRemediationText = "Consider removing certain routes.")()
