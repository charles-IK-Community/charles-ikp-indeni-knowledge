package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityWithItemsTemplateRule

/**
  *
  */
case class cross_vendor_tx_overrun(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_tx_overrun",
  ruleFriendlyName = "All Devices: TX packets overrun",
  ruleDescription = "indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-tx-overruns",
  limitMetricName = "network-interface-tx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of error packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high overrun rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f packets overrun out of a total of %.0f transmitted.",
  baseRemediationText = "Packet overruns usually occur when there are too many packets being inserted into the port's memory buffer, faster than the rate at which the kernel is able to process them.",
  alertItemsHeader = "Affected Ports")()
