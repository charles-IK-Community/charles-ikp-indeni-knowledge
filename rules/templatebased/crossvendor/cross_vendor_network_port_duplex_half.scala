package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_network_port_duplex_half(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_network_port_duplex_half",
  ruleFriendlyName = "All Devices: Network port(s) running in half duplex",
  ruleDescription = "indeni will alert one or more network ports is running in half duplex.",
  metricName = "network-interface-duplex",
  applicableMetricTag = "name",
  alertItemsHeader = "Ports Affected",
  alertDescription = "One or more ports are set to half duplex. This is usually an error. Review the list of ports below.",
  baseRemediationText = "Many times ports are in half duplex due to an autonegotation error or a misconfiguration.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("half"), SnapshotExpression("network-interface-duplex").asSingle().mostRecent().noneable)
)(ConditionalRemediationSteps.VENDOR_CP -> "Review sk83760: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk83760",
  ConditionalRemediationSteps.VENDOR_PANOS -> "https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Display-Port-Information-Connected-Media-Interface/ta-p/61715")
