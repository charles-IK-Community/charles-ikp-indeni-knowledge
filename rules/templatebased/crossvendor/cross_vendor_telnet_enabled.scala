package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_telnet_enabled(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_telnet_enabled",
  ruleFriendlyName = "All Devices: Telnet is enabled on the device",
  ruleDescription = "indeni will check if a device has Telnet enabled. Telnet is not encrypted and is therefore a security risk.",
  metricName = "telnet-enabled",
  alertDescription = "Telnet allows unencrypted control traffic to network devices. It transmits all data in clear text, including passwords and other potentially confidential information.",
  baseRemediationText = "Disable Telnet on the device.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("telnet-enabled").asSingle().mostRecent().noneable)
)(ConditionalRemediationSteps.OS_NXOS -> "Disable Telnet by using the \"no feature telnet\" configuration command.\nYou can verify that Telnet has been disabled by using the \"show telnet server\" command."
)
