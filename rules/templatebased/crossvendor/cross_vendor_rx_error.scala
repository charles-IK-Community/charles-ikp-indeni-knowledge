package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityWithItemsTemplateRule

/**
  *
  */
case class cross_vendor_rx_error(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_rx_error",
  ruleFriendlyName = "All Devices: RX packets experienced errors",
  ruleDescription = "indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-rx-errors",
  limitMetricName = "network-interface-rx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of error packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high error rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f error packets identified out of a total of %.0f received.",
  baseRemediationText = "Packet errors usually occur when there is a mismatch in the speed and duplex settings on two sides of a cable, or a damaged cable.",
  alertItemsHeader = "Affected Ports")()
