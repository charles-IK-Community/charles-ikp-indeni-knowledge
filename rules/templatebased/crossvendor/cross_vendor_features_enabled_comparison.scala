package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_features_enabled_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_features_enabled_comparison",
  ruleFriendlyName = "Clustered Devices: Features enabled do not match across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the features they have enabled are different.",
  metricName = "features-enabled",
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same features enabled. Review the differences below.",
  baseRemediationText = "Review the licensing and enabled features or modules on each device to ensure they match.")()
