package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_network_port_speed_low(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_network_port_speed_low",
  ruleFriendlyName = "All Devices: Network port(s) running in low speed",
  ruleDescription = "indeni will alert one or more network ports is running in a speed lower than 1000Mbps.",
  metricName = "network-interface-speed",
  applicableMetricTag = "name",
  alertItemsHeader = "Ports Affected",
  alertDescription = "One or more ports are set to a speed lower than 1000Mbps. This is usually an error. Review the list of ports below.",
  baseRemediationText = "Many times ports are in a low speed due to an autonegotation error or a misconfiguration.",
  complexCondition = RuleOr(
    RuleEquals(SnapshotExpression("network-interface-speed").asSingle().mostRecent().noneable, RuleHelper.createComplexStringConstantExpression("10M")),
    RuleEquals(SnapshotExpression("network-interface-speed").asSingle().mostRecent().noneable, RuleHelper.createComplexStringConstantExpression("100M"))
  )
)()
