package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_bgp_peer_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_bgp_peer_down",
  ruleFriendlyName = "All Devices: BGP peer(s) down",
  ruleDescription = "indeni will alert one or more BGP peers isn't communicating well.",
  metricName = "bgp-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Peers Affected",
  alertDescription = "One or more BGP peers are down.",
  baseRemediationText = "Review the cause for the peers being down.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Consider reading Tobias Lachmann's blog on BGP: https://blog.lachmann.org/?p=1771",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Consider starting at https://live.paloaltonetworks.com/t5/Configuration-Articles/BGP-Routes-are-not-Injected-into-the-Routing-Table/ta-p/54938 . You can also log into the device over SSH and run \"less mp-log routed.log\"."
)
