package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.time.TimeSpan

/**
  *
  */
case class all_devices_dns_failure(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "all_devices_dns_failure",
  howManyRepetitions = 2,
  ruleFriendlyName = "All Devices: DNS lookup failure(s)",
  ruleDescription = "indeni will alert if the DNS resolution is not working on the device. It will do so by attempting to resolve www.indeni.com.",
  metricName = "dns-server-state",
  applicableMetricTag = "name",
  alertItemsHeader = "DNS Servers Affected",
  alertDescription = "One or more DNS servers configured on this device are not responding or are failing to resolve www.indeni.com.",
  baseRemediationText = "Review the cause for the DNS resolution not working.")()
