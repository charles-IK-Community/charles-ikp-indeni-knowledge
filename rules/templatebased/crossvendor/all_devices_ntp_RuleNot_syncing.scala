package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.time.TimeSpan

/**
  *
  */
case class all_devices_ntp_note_syncing(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "all_devices_ntp_not_syncing",
  howManyRepetitions = 2,
  ruleFriendlyName = "All Devices: NTP sync failure(s)",
  ruleDescription = "indeni will alert if one or more of the configured NTP servers is not syncing correctly.",
  metricName = "ntp-server-state",
  applicableMetricTag = "name",
  alertItemsHeader = "NTP Servers Affected",
  alertDescription = "One or more NTP servers configured on this device is not responding.",
  baseRemediationText = "Review the cause for the NTP sync not working.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review sk92602: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk92602",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Run \"show ntp\" and review the status of each NTP server. You can also review the dagger.log, based on https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/Management-TKB/article-id/2078"
)
