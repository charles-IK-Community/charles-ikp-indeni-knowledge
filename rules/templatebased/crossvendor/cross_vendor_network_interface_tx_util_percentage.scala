package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  * Contributed by Indeni_PJ
  */
case class crossVendorInterfaceTXUtilization(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_interface_tx_utilization",
  ruleFriendlyName = "All Devices: Interface nearing maximum Tx throughput",
  ruleDescription = "The interface is close to its maximum advertised throughput limit. Intermittent connectivity issues may occur such as network performance degradation or the inability to reach resources. This could be due to poor capacity management or user activity such as video streaming or file transfers. This could also be due to unexpected and suspicious activity.",
  usageMetricName = "network-interface-tx-util-percentage",
  applicableMetricTag = "name",
  threshold = 80.0,
  alertItemsHeader = "Affected Interfaces",
  alertItemDescriptionFormat  = "The interface is sending at %.0f%% of it's capacity.",
  alertDescription = "Interfaces are nearing their maximum throughput. Connectivity issues may occur due to this. This could be due to poor capacity management, user activity such as video streaming or file transfers. This could also be due to unexpected activity.",
  baseRemediationText = "Follow the vendor specific remediation steps below.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Check point’s CLI cpview or SmartView Monitor tool can be used to view network top talkers such as source, destination, and protocol(s). Based on this information, it can be determined as to whether selected traffic should be blocked or dropped. If the throughput threshold reached is expected, consider adding more interfaces or modifying any applicable packet shaping.")
