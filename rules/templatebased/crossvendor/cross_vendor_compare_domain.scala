package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_compare_domain(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_domain",
  ruleFriendlyName = "Clustered Devices: Cluster members' domain names mismatch",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the domain setting is different.",
  metricName = "domain",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same domain settings.",
  baseRemediationText = """Review the settings of each device in the cluster and ensure they are the same.""")()
