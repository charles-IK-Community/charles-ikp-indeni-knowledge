package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityWithItemsTemplateRule

/**
  *
  */
case class license_usage_limit(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "license_usage_limit",
  ruleFriendlyName = "All Devices: License usage limit approaching",
  ruleDescription = "Some licenses are limited to a certain number of elements (such as maximum users). If any of the licenses is nearing its limit, an alert will be issued.",
  usageMetricName = "license-elements-used",
  limitMetricName = "license-elements-limit",
  applicableMetricTag = "name",
  threshold = 80.0,
  minimumValueToAlert = 2.0, // We don't want to alert if the license capacity is 1 and we're using one item, this is a common occurence and isn't an issue
  alertDescription = "Some licenses are nearing their limit. Review the list below.",
  alertItemDescriptionFormat = "The number of elements in use is %.0f where the limit is %.0f.",
  baseRemediationText = "Consider purchasing additional licenses.",
  alertItemsHeader = "Affected Licenses")()
