package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.time.TimeSpan

/**
  *
  */
case class cross_vendor_config_unsaved(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_config_unsaved",
  howManyRepetitions = 2,
  ruleFriendlyName = "All Devices: Configuration changed but not saved",
  ruleDescription = "indeni will alert if the configuration was changed on a device, but not saved.",
  metricName = "config-unsaved",
  alertIfDown = false,
  alertDescription = "The configuration has been changed on this device, but has not yet been saved. This may result in the loss of the new configuration during a power cycle or device reboot.",
  baseRemediationText = "Log into the device and save the configuration.")(
  ConditionalRemediationSteps.VENDOR_CP -> "In clish, run \"save configuration\".",
  ConditionalRemediationSteps.VENDOR_CISCO -> "For IOS, use \"write\", for NX-OS use \"copy running-config startup-config\".",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device's web interface and click on the Commit button."
)
