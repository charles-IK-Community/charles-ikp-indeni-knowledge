package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_cluster_down_vsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_cluster_down_vsx",
  ruleFriendlyName = "Clustered Devices (VS): Cluster down",
  ruleDescription = "indeni will alert if a cluster is down or any of the members are inoperable.",
  metricName = "cluster-state",
  applicableMetricTag = "name",
  descriptionMetricTag = "vs.name",
  alertItemsHeader = "Clustering Elements Affected",
  alertDescription = "One or more clustering elements in this device are down.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = "Review the cause for one or more members being down or inoperable.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review other alerts for a cause for the cluster failure.",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device over SSH and run \"less mp-log ha-agent.log\" for more information."
)
