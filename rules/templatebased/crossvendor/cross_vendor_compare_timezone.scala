package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_compare_timezone(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_timezone",
  ruleFriendlyName = "Clustered Devices: Timezone mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the timezone setting is different.",
  metricName = "timezone",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same timezone settings.",
  baseRemediationText = """Review the settings of each device in the cluster and ensure they are the same.""")()
