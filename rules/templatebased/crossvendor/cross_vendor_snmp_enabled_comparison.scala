package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_snmp_enabled_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_snmp_enabled_comparison",
  ruleFriendlyName = "Clustered Devices: SNMP enablement setting does not match across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
  metricName = "snmp-enabled",
  isArray = false,
  alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
  baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")()
