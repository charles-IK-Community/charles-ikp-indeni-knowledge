package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SingleSnapshotComplianceCheckTemplateRule

case class crossvendor_compliance_check_os_version(context: RuleContext) extends SingleSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_os_version",
  ruleFriendlyName = "Compliance Check: OS/Software version does not match requirement",
  ruleDescription = "indeni can verify that the OS/software version installed is a specific one.",
  metricName = "os-version",
  baseRemediationText = "Install the OS/software version required.",
  parameterName = "OS/Software Version",
  parameterDescription = "The OS/software version to compare against.",
  expectedValue = ""
)()
