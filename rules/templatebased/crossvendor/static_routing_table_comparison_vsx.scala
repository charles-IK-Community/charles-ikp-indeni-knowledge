package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}

/**
  *
  */
case class static_routing_table_comparison_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "static_routing_table_comparison_vsx",
  ruleFriendlyName = "Clustered Devices (VS): Static routing table does not match across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if their static routing tables are different.",
  metricName = "static-routing-table",
  applicableMetricTag = "vs.name",
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same static routing tables. Review the differences below.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/itzik-assaraf/2/870/1b5\">Itzik Assaraf</a> (Leumi Card).",
  baseRemediationText = "Ensure the static routing table matches across devices in a cluster.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Use the \"show configuration\" command in clish to compare the calls to \"set static-route\".")
