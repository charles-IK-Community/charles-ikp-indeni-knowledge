package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class connected_tables_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "connected_tables_comparison",
  ruleFriendlyName = "Clustered Devices: Connected networks do not match across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the networks they are directly connected to do not match.",
  metricName = "connected-networks-table",
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same directly connected networks. Review the differences below.",
  baseRemediationText = "Ensure all of the required ports are configured correctly on all cluster members, including the subnet mask.")()
