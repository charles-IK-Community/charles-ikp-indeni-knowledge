package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.paloalto._

/**
  *
  */
case class panw_dataplane_ctd_dlp_flow(context: RuleContext) extends DataplanePoolUsageRule(context, "CTD DLP FLOW", 80.0)
