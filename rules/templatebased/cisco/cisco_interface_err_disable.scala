package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class cisco_interface_err_disable(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_interface_err_disable",
  ruleFriendlyName = "Cisco Devices: Interface(s) in error-disable state",
  ruleDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices. indeni will alert if this happens.",
  metricName = "network-interface-err-disable",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Affected Interfaces",
  alertDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices.\nThis includes:\n* Flapping links\n* Spanning Tree BPDUs detected with BPDU Guard enabled\n* Detected a physical loop\n* Uni-directional link detected by UDLD",
  baseRemediationText = "Use the \"show interface\" command to identify the reason for the err-disable interface state.\nAfter fixing the issue perform a \"shut/no shut\" on the port to re-enable it.\nIt is possible to enable automatic periodic err-disable recovery by using the \"errdisable recovery cause\" configuration command."
)()
