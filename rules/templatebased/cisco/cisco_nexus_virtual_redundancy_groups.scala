package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

case class cisco_nexus_virtual_redundancy_groups(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cisco_nexus_virtual_redundancy_groups",
  ruleFriendlyName = "Cisco Nexus: Virtual Redundancy Groups (VRRP/HSRP) are out of sync across vPC cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the redundancy group configuration is different.",
  metricName = "virtual-redundancy-groups",
  isArray = true,
  applicableMetricTag = "name",
  alertItemsHeader = "Clustering Protocols Affected",
  alertDescription = "All VRRP/HSRP redundancy groups must be in sync across a pair of vPC cluster members.\nThe virtual group's IP address, group number and the VLAN it is used for have to be all aligned.\nAny missing groups create a risk of an outage for the VLAN if the primary cluster member fails because the redundant device is not configured to start forwarding traffic.",
  baseRemediationText = """Configure the same groups, using the same VLAN ID, group ID and virtual IP on both devices. See:
                          |http://www.cisco.com/c/en/us/support/docs/switches/nexus-7000-series-switches/113002-nexus-hsrp-00.html
                          |http://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/4_2/nx-os/unicast/configuration/guide/l3_cli_nxos/l3_vrrp.html""")()

