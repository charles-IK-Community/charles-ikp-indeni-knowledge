package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class LbPoolMembersInsufficientRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("lb_pool_members_insufficient", "Load Balancers: Number of members in pool insufficient",
    "indeni will alert if the number of members available in a pool is less than the required amount.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("lb-pool-members-up").last
    val requiredValue = TimeSeriesExpression[Double]("lb-pool-members-required").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("lb-pool-members-up", "lb-pool-members-required")),

        StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("lb-pool-members-up", "lb-pool-members-required")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThan(
                requiredValue,
                inUseValue)

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                EMPTY_STRING,
                title = "Pools Affected"
            ).asCondition()
          ).withoutInfo().asCondition()

          // Details of the alert itself
        ).withRootInfo(
            getHeadline(),
            ConstantExpression("Certain pools have a number of members required to be considered OK. Some of those pools have less members than they should. Review list below."),
            ConstantExpression("Determine why the members are down and resolve the issue as soon as possible.")
    )
  }


}
