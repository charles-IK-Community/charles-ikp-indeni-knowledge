package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, Equals, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class ConfigChangeOnStandbyMemberRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_config_change_on_standby", "Clustered Devices: Configuration changed on standby member",
    "Generally, making configuration changes to the standby member of a device is not recommended. indeni will alert if this happens.",
    TimeSpan.fromMinutes(5), AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val configUnsavedValue = TimeSeriesExpression[Double]("config-unsaved").last
    val memberStateValue = TimeSeriesExpression[Double]("cluster-member-active").last
    val configSyncValue = TimeSeriesExpression[Double]("cluster-config-synced").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("config-unsaved", "cluster-member-active", "cluster-config-synced")),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(
              Equals(configUnsavedValue, ConstantExpression(Some(1.0))),
              Equals(memberStateValue, ConstantExpression(Some(0.0))),
              GreaterThanOrEqual(configSyncValue, ConstantExpression(Some(0.0))))
          ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The configuration has been changed on this device, but it's not the active member of the cluster. Best practices recommend making changes to the active member of a cluster and then syncing to the standby."),
        ConstantExpression("Make the configuration changes to the active member of the cluster.")
    )
  }
}
