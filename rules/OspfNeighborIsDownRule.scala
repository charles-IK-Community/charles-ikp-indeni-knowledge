package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.ts.TimeSinceLastValueExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class OspfNeighborIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Downtime"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Downtime",
    "If a peer device is down or not communicating for at least this amount of time, an alert will be issued.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(15))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_ospf_neighbor_down", "All Devices: OSPF neighbor(s) down",
    "indeni will alert one or more OSPF neighbors isn't communicating well.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val threshold = getParameterTimeSpanForRule(context, highThresholdParameter).noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("ospf-state")),

          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("ospf-state"), ConstantExpression(TimeSpan.fromDays(1))),
            GreaterThan(TimeSinceLastValueExpression(TimeSeriesExpression("ospf-state"), Some(1.0)), threshold)
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"name\")}"),
              EMPTY_STRING,
              title = "Neighbors Affected"
          ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more OSPF neighbors are down."),
        ConditionalRemediationSteps("Review the cause for the neighbors being down.",
          ConditionalRemediationSteps.VENDOR_CP -> "Review sk84520: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Review https://live.paloaltonetworks.com/t5/Configuration-Articles/OSPF-Adjacencies-are-Established-but-the-OSPF-Routes-are-Not/ta-p/58328 . You can also log into the device over SSH and run \"less mp-log routed.log\"."
        )
    )
  }
}


