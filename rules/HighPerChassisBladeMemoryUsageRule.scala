package com.indeni.server.rules.library

import com.indeni.data.conditions.{Equals, True}
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.AverageExpression
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

/**
  * Created by amir on 04/02/2016.
  */
case class HighPerChassisBladeMemoryUsageRule(context: RuleContext) extends PerDeviceRule {

  private[library] val highThresholdParameterName: String = "High_Threshold_of_Memory_Usage"

  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Memory Usage",
    "What is the threshold for the memory usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    85.0)

  override val metadata: RuleMetadata = RuleMetadata("high_per_chassis_blade_memory_usage", "High Memory Usage per Chassis and Blade", "Alert when Memory usage is high", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    import HighPerChassisBladeMemoryUsageRule._

    val usagePercentage = AverageExpression(TimeSeriesExpression[Double](MemoryUsagePercentageTimeSeries))
    val usagePercentageThreshold = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, highThresholdParameter.getName, ConfigurationSetId, highThresholdParameter.getDefaultValue.asDouble.toDouble).noneable
    val isUsagePercentageAboveThreshold = GreaterThanOrEqual(usagePercentage, usagePercentageThreshold)

    val mountSpaceFailDescription = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String =
        "Memory usage (" + usagePercentage.eval(time) + "%) above threshold (" + usagePercentageThreshold.eval(time) + "%) " +
          "for chassis: " + scope.getVisible(ChassisKey).get + ", blade: " + scope.getVisible(BladeKey).get

      override def args: Set[Expression[_]] = Set(usagePercentage, usagePercentageThreshold)
    }
    val mountSpaceFailHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = "chassis: " + scope.getVisible(ChassisKey).get + ", blade: " + scope.getVisible(BladeKey).get

      override def args: Set[Expression[_]] = Set()
    }
    val tsQuery = SelectTimeSeriesExpression[Double](context.tsDao, Set(MemoryUsagePercentageTimeSeries))
    val forTsCondition = StatusTreeExpression(tsQuery, isUsagePercentageAboveThreshold).withSecondaryInfo(
      mountSpaceFailHeadline, mountSpaceFailDescription, title = "Problematic Blades"
    ).asCondition()

    val chassisBladeQuery = SelectTagsExpression(context.tsDao, Set(ChassisKey, BladeKey), True)
    val highMemoryUsagePerDevicePerChassisBladeLogic = StatusTreeExpression(chassisBladeQuery, forTsCondition)
      .withoutInfo().asCondition()

    val headline = ConstantExpression("High memory usage")
    val description = ConstantExpression("The memory usage in the operating system is higher than the high threshold.")
    val remediation = "Review the load on this thread to see if the memory utilization is valid."

    val devicesFilter = Equals(HighPerChassisBladeMemoryUsageRule.ModelKey, HighPerChassisBladeMemoryUsageRule.ModelValue)
    val devicesQuery = SelectTagsExpression(context.metaDao, Set(DeviceKey), devicesFilter)

    StatusTreeExpression(devicesQuery, highMemoryUsagePerDevicePerChassisBladeLogic).withRootInfo(
      headline, description, ConstantExpression(remediation)
    )
  }
}

object HighPerChassisBladeMemoryUsageRule {
  val ModelKey = "model"
  val ModelValue = "CheckPoint61k"

  val BladeKey = "Blade"
  val ChassisKey = "Chassis"
  val MemoryUsagePercentageTimeSeries = "memory-usage"
}
