package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.{AbsExpression, MinusExpression}
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ClockOffRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_clock_off", "All Devices: Clock set incorrectly",
    "indeni will alert when a device's clock is more than 24 hours off of indeni's clock.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("current-datetime").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("current-datetime")),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThan(
            AbsExpression(MinusExpression(actualValue, NowExpression())),
            ConstantExpression(Some(3600.0 * 24)))

          // The Alert Item to add for this specific item
      ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("The current date/time on this device is: %s which seems to be incorrect.", doubleToDateExpression(actualValue)),
            ConstantExpression("Consider setting the date on the device and activating NTP.")
        ).asCondition()
    ).withoutInfo()
  }
}
