package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class NicRxOverrunsRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Ratio"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Alerting Threshold",
    "If the total RX packets flagged as overrun per second is at least this percentage of the total RX packets per second, indeni will alert.",
    UIType.DOUBLE,
    0.5)

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_rx_overrun", "All Devices: RX packets overrun",
    "indeni tracks the number of packets that had issues and alerts if the ratio is too high.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("network-interface-rx-overruns").last
    val limitValue = TimeSeriesExpression[Double]("network-interface-rx-packets").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-rx-overruns", "network-interface-rx-packets")),

        StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-rx-overruns", "network-interface-rx-packets")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThanOrEqual(
                TimesExpression(DivExpression(inUseValue, limitValue), ConstantExpression(Some(100.0))),
                getParameterDouble(context, highThresholdParameter))

              // The Alert Item to add for this specific item
        ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                scopableStringFormatExpression("The number of RX packets flagged as overruns per second is %.0f, out of a total of %.0f packets. This is %.0f%%, where the alerting threshold is %.0f%%.", inUseValue, limitValue, TimesExpression(ConstantExpression[Option[Double]](Some(100.0)), DivExpression(inUseValue, limitValue)), getParameterDouble(context, highThresholdParameter)),
                title = "Affected Ports/Interfaces"
            ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some network interfaces and ports are experiencing a high overrun rate. Review the ports below."),
        ConstantExpression("Packet overruns usually occur when there are too many packets being inserted into the port's memory buffer, faster than the rate at which the kernel is able to process them.")
    )
  }
}
