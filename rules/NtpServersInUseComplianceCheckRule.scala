package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.config.DynamicParameterExpression
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility.{SeqDiffMissingsExpression, SeqDiffRedundantsExpression, SeqDiffWithoutOrderExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.{RuleMetadata, _}
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * Created by amir on 14/02/2017.
  */
case class NtpServersInUseComplianceCheckRule(context: Context) extends PerDeviceRule with RuleHelper {


  import NtpServersInUseComplianceCheckRule._

  private[library] val ntpServersName = "ntp_servers_compliance_check"
  private val ntpServers = new ParameterDefinition(
    ntpServersName,
    "",
    "NTP Servers",
    "NTP Servers Required (each host in its own line, required)",
    UIType.MULTILINE_TEXT,
    "")


  override def expressionTree: StatusTreeExpression = {
    val ntpExpected = DynamicParameterExpression.withConstantDefault(context.parametersService, metadata.name, ntpServers.getName, ConfigurationSetKey, Seq[String]()).withLazy
    val diff = SeqDiffWithoutOrderExpression[String](
      MultiSnapshotExtractScalarExpression(SnapshotExpression("ntp-servers").asMulti().mostRecent(), "ipaddress"),
      ntpExpected
    ).withLazy

    val headline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible(NtpServerTag).get.toString

      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("ntp-servers")).multi(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        diff.nonEmpty,
        // Details of the alert itself
        StatusCoreInformerExpression(
          getHeadline(),
          ConstantExpression("NTP servers configured is not as expected"),
          ConstantExpression("Modify the device's configuration as required.")),
        Seq(
          MultiStatusAdditionalInformerExpression(
            SeqDiffMissingsExpression(diff),
            NtpServerTag,
            headline,
            EMPTY_STRING,
            EMPTY_STRING,
            "Missing servers"
          ),
          MultiStatusAdditionalInformerExpression(
            SeqDiffRedundantsExpression(diff),
            NtpServerTag,
            headline,
            EMPTY_STRING,
            EMPTY_STRING,
            "Redundant servers"
          )
        )
      ).asCondition()
    )
  }

  /**
    * @return The rule's metadata.
    */
  override def metadata: RuleMetadata =
    RuleMetadata(
      "ntp_servers_in_use",
      "Compliance Check: NTP servers in use",
      "indeni can verify that certain NTP servers are configured on a monitored device.",
      AlertSeverity.ERROR,
      Set(ntpServers),
      defaultAction = Some(AutomationPolicyItemAlertSetting.NEVER)
    )
}

object NtpServersInUseComplianceCheckRule {
  val NtpServerTag = "NTP_server"
}
