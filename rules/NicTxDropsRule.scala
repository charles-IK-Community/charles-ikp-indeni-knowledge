package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class NicTxDropsRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Ratio"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Alerting Threshold",
    "If the total TX packets dropped per second is at least this percentage of the total TX packets per second, indeni will alert.",
    UIType.DOUBLE,
    0.5)

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_tx_drop", "All Devices: TX packets dropped",
    "indeni tracks the number of packets that had issues and alerts if the ratio is too high.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("network-interface-tx-dropped").last
    val limitValue = TimeSeriesExpression[Double]("network-interface-tx-packets").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-tx-dropped", "network-interface-tx-packets")),

        StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-tx-dropped", "network-interface-tx-packets")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThanOrEqual(
                TimesExpression(DivExpression(inUseValue, limitValue), ConstantExpression(Some(100.0))),
                getParameterDouble(context, highThresholdParameter))

              // The Alert Item to add for this specific item
        ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"name\")}"),
              scopableStringFormatExpression("The number of TX packets dropped per second is %.0f, out of a total of %.0f packets. This is %.0f%%, where the alerting threshold is %.0f%%.", inUseValue, limitValue, TimesExpression(ConstantExpression[Option[Double]](Some(100.0)), DivExpression(inUseValue, limitValue)), getParameterDouble(context, highThresholdParameter)),
              title = "Affected Ports/Interfaces"
        ).asCondition()
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some network interfaces and ports are experiencing a high drop rate. Review the ports below."),
        ConstantExpression("Packet drops usually occur when the rate of packets received is higher than the device's ability to handle.")
    )
  }
}
