package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class ClusterPreemptionEnabledRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = new RuleMetadata("cross_vendor_cluster_preempt", "Clustered Devices: Cluster has preemption enabled",
    "Preemption is generally a bad idea in clustering, although sometimes it is the default setting. indeni will alert if it's on.",
    TimeSpan.fromMinutes(5), AlertSeverity.ERROR)


  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("cluster-preemption-enabled").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-preemption-enabled")),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Equals(
              inUseValue,
              ConstantExpression[Option[Double]](Some(1.0)))
          ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("This cluster member has preemption enabled. This means that it will have priority over other cluster members. If this device reboots or crashes, it'll try to assume priority in the cluster when it finishes its boot process. This may result in it crashing again, and causing a preemption loop."),
        ConditionalRemediationSteps("It is generally best to have preemption disabled. Instead, once this device returns from a crash, you can conduct the failover manually.",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Palo Alto Networks firewalls have a special way of handling preemption loops, see https://live.paloaltonetworks.com/t5/Learning-Articles/Understanding-Preemption-with-the-Configured-Device-Priority-in/ta-p/53398")
    )
  }
}
