package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.rules.library.PortIsDownRule.NAME
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class PortIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata(NAME, "All Devices: Network port(s) down",
    "indeni will alert one or more network ports is down.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {

    val actualValue = TimeSeriesExpression[Double]("network-interface-state").last
    val adminValue = TimeSeriesExpression[Double]("network-interface-admin-state").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-state", "network-interface-admin-state")),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-state", "network-interface-admin-state")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              And(
                Equals(ConstantExpression[Option[Double]](Some(0)), actualValue),
                Equals(ConstantExpression[Option[Double]](Some(1)), adminValue)
              )

              // The Alert Item to add for this specific item
              ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                EMPTY_STRING,
                title = "Ports Affected"
            ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more ports are down."),
        ConstantExpression("Review the cause for the ports being down.")
    )
  }
}

object PortIsDownRule {

  /* --- Constants --- */

  private[library] val NAME = "cross_vendor_network_port_down"
}


