package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, Equals, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class SmartEventLogHandlingRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("check_point_smartevent_log_handling_issue", "Check Point Devices: SmartEvent log handling too slow",
    "If SmartEvent can't handle logs fast enough a backlog may occur, or the storage fills up. indeni will track the log handling by SmartEvent and alert if it's too slow.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("folder-file-count").last
    val folderPath = ScopeValueExpression("path").visible().asString().noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTagsExpression(context.tsDao, Set("path"), withTagsCondition("folder-file-count")),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
        StatusTreeExpression(
                // The time-series we check the test condition against:
                SelectTimeSeriesExpression[Double](context.tsDao, Set("folder-file-count")),

                // The condition which, if true, we have an issue. Checked against the time-series we've collected
                And(
                  GreaterThanOrEqual(
                    actualValue,
                    ConstantExpression(Some(100.0))),
                  Equals(folderPath, ConstantExpression(Some("$RTDIR/distrib")))
                )

          ).withRootInfo(
                getHeadline(),
                scopableStringFormatExpression("A look at $RTDIR/distrib shows there are currently %.0f logs being handled and it appears to remain at this level constantly. This may indicate a performance issue.", actualValue),
                ConstantExpression("Contact your technical support provider, mention SK92766.")
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withoutInfo()
  }
}
