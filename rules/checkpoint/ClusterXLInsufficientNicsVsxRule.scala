package com.indeni.server.rules.library.checkpoint

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.rules.library.checkpoint.ClusterXLInsufficientNicsVsxRule.NAME
import com.indeni.server.rules.library.{PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ClusterXLInsufficientNicsVsxRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata(NAME, "Check Point ClusterXL (VSX): Required interface(s) down",
    "ClusterXL requires a certain number of interfaces to be up for the member to be considered OK.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("cphaprob-up-interfaces").last
    val requiredValue = TimeSeriesExpression[Double]("cphaprob-required-interfaces").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("vs.id"), True),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("cphaprob-up-interfaces", "cphaprob-required-interfaces")),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThan(
                requiredValue,
                inUseValue)

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("VS: ${scope(\"vs.id\")}"),
                scopableStringFormatExpression("%.0f interfaces are up vs a requirement of %.0f", inUseValue, requiredValue),
                title = "Affected VS's"
            ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some VS's have less interfaces up than required."),
        ConstantExpression("Determine why the interfaces are down and resolve the issue.")
    )
  }
}

object ClusterXLInsufficientNicsVsxRule {

  /* --- Constants --- */

  private[checkpoint] val NAME = "clusterxl_insufficient_nics_vsx"
}
