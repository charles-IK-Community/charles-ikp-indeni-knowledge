package com.indeni.server.rules.library.checkpoint

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class RoutesMissingRule(context: Context) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("chkp_firewall_routes_missing", "Check Point Firewalls: Routes defined in clish/webUI are missing",
    "Sometimes the routes that are defined in the Check Point Web UI or through clish may not be fully applied to the operating system layer. If this happens, indeni will alert.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("routes-missing-kernel")).multi(),

        StatusTreeExpression(
          IterateSnapshotDimensionExpression(Map(
            "routes" -> SnapshotExpression("routes-missing-kernel")
          )),
          com.indeni.ruleengine.expressions.conditions.True
        ).withSecondaryInfo(
            scopableStringFormatExpression("${\"routes:missing-route\"}"),
            EMPTY_STRING,
            title = "Missing Routes"
        ).asCondition()
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The configured routes have not been correctly applied to the Gaia OS. This means that some of the routes configured do not currently work."),
        ConditionalRemediationSteps("A workaround to get it to work can be to restart the routeD daemon by running \"cpstop;cpstart\" or restarting the device. However since this should not happen a case can also be opened with your technical support provider. In the case of devices in a cluster it is possible that the issue happens only for one of the nodes and a failover to the other node could lessen the impact of the issue."
        )
    )
  }
}


