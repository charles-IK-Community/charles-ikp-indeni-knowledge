package com.indeni.server.rules.library

import com.indeni.ruleengine.Context
import com.indeni.ruleengine.expressions.conditions.{Equals, Not, Or}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.library.paloalto.{DataplanePoolUsageRule, GenericDataplanePoolUsageRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan


class TemplateBasedRules {
  def generateTemplateBasedRules(context: Context) : Set[PerDeviceRule] = {
    var rules: Set[PerDeviceRule] = Set()
    // State down rules, not vendor specific
    rules = rules +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_config_unsaved",
        howLongMustBeInState = TimeSpan.fromHours(3),
        ruleFriendlyName = "All Devices: Configuration changed but not saved",
        ruleDescription = "indeni will alert if the configuration was changed on a device, but not saved.",
        metricName = "config-unsaved",
        alertIfDown = false,
        alertDescription = "The configuration has been changed on this device, but has not yet been saved. This may result in the loss of the new configuration during a power cycle or device reboot.",
        baseRemediationText = "Log into the device and save the configuration.")(
        ConditionalRemediationSteps.VENDOR_CP -> "In clish, run \"save configuration\".",
        ConditionalRemediationSteps.VENDOR_CISCO -> "For IOS, use \"write\", for NX-OS use \"copy running-config startup-config\".",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device's web interface and click on the Commit button."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_critical_process_down_novsx",
        howLongMustBeInState = TimeSpan.fromMinutes(10),
        ruleFriendlyName = "All Devices: Critical process(es) down",
        ruleDescription = "Many devices have critical processes, usually daemons, that must be up for certain functions to work. indeni will alert if any of these goes down.",
        metricName = "process-state",
        applicableMetricTag = "process-name",
        descriptionMetricTag = "description",
        alertItemsHeader = "Processes Affected",
        descriptionStringFormat = "${scope(\"description\")}",
        alertDescription = "One or more processes which are critical to the operation of this device, are down.",
        baseRemediationText = "Review the cause for the processes being down.",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"))(
        ConditionalRemediationSteps.VENDOR_CP -> "Check if \"cpstop\" was run."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_critical_process_down_vsx",
        howLongMustBeInState = TimeSpan.fromMinutes(10),
        ruleFriendlyName = "All Devices: Critical process(es) down (per VS)",
        ruleDescription = "Many devices have critical processes, usually daemons, that must be up for certain functions to work. indeni will alert if any of these goes down.",
        metricName = "process-state",
        applicableMetricTag = "process-name",
        descriptionMetricTag = "vs.name",
        alertItemsHeader = "Processes Affected",
        alertDescription = "One or more processes which are critical to the operation of this device, are down.",
        baseRemediationText = "Review the cause for the processes being down.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Check if \"cpstop\" was run."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_bond_down",
        ruleFriendlyName = "All Devices: Bond/LACP interface down",
        ruleDescription = "indeni will alert if a bond interface is down.",
        metricName = "bond-state",
        applicableMetricTag = "name",
        alertItemsHeader = "Interfaces Affected",
        descriptionStringFormat = "",
        alertDescription = "One or more bond interfaces are down.",
        baseRemediationText = "Review the cause for the interfaces being down.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Use the \"cphaconf show_bond\" command to get additional information.",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Use the \"show lacp\" command to get additional information."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_bond_slave_down",
        ruleFriendlyName = "All Devices: Bond/LACP slave interface down",
        ruleDescription = "indeni will alert if a bond interface's slave interface is down.",
        metricName = "bond-slave-state",
        applicableMetricTag = "name",
        descriptionMetricTag = "bond-name",
        alertItemsHeader = "Interfaces Affected",
        descriptionStringFormat = "Part of ${scope(\"bond-name\")}",
        alertDescription = "One or more bond slave interfaces are down.",
        baseRemediationText = "Review the cause for the interfaces being down.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Use the \"cphaconf show_bond\" command to get additional information.",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Use the \"show lacp\" command to get additional information."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_bgp_peer_down",
        ruleFriendlyName = "All Devices: BGP peer(s) down",
        ruleDescription = "indeni will alert one or more BGP peers isn't communicating well.",
        metricName = "bgp-state",
        applicableMetricTag = "name",
        alertItemsHeader = "Peers Affected",
        alertDescription = "One or more BGP peers are down.",
        baseRemediationText = "Review the cause for the peers being down.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Consider reading Tobias Lachmann's blog on BGP: https://blog.lachmann.org/?p=1771",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Consider starting at https://live.paloaltonetworks.com/t5/Configuration-Articles/BGP-Routes-are-Not-Injected-into-the-Routing-Table/ta-p/54938 . You can also log into the device over SSH and run \"less mp-log routed.log\"."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "chassis_blade_down",
        ruleFriendlyName = "Chassis Devices: Blade(s) down",
        ruleDescription = "indeni will alert one or more blades in a chassis is down.",
        metricName = "blade-state",
        applicableMetricTag = "name",
        alertItemsHeader = "Blades Affected",
        alertDescription = "One or more blades in this chassis are down.",
        baseRemediationText = "Review the cause for the blades being down.")(
        ConditionalRemediationSteps.VENDOR_CP -> "If the blade was not stopped intentionally (admin down), check to see it wasn't disconnected physically."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "vs_down",
        ruleFriendlyName = "Devices with VS's: VS(s) down",
        ruleDescription = "indeni will alert one or more virtual systems in a device is down.",
        metricName = "vs-state",
        applicableMetricTag = "name",
        alertItemsHeader = "VS's Affected",
        alertDescription = "One or more virtual systems in this device are down.",
        baseRemediationText = "Review the cause for the virtual systems being down.")(
        ConditionalRemediationSteps.VENDOR_F5 -> "For F5 vCMP's, review https://support.f5.com/csp/article/K15930"
      ) +
      new NearingCapacityWithItemsTemplateRule(context,
        ruleName = "lb_server_ping_slow",
        ruleFriendlyName = "Load Balancers: Pool member / server response time high",
        ruleDescription = "indeni will alert if the ping from the load balancer to specific servers is too high.",
        usageMetricName = "lb-server-ping-response",
        threshold = 1.0,
        applicableMetricTag = "name",
        alertItemsHeader = "Slow Servers/Pool Members",
        alertDescription = "Some pool members' or servers' response time is too high.",
        alertItemDescriptionFormat = "The response time for this pool member or server is %.0f ms",
        baseRemediationText = "Review any networking or communication issues which may cause this.")(
        ConditionalRemediationSteps.VENDOR_F5 -> "Try running the following command on the device under bash:\ntmsh -q -c 'cd /;list ltm node recursive' | awk '/^ltm node/{print \"Node: \" $3} /address/{ system(\"ping -c1 \" $2)};'"
      ) +
      new StateDownTemplateRule(context,
        ruleName = "lb_pool_members_unavailable",
        ruleFriendlyName = "Load Balancers: Pool member(s) unavailable",
        ruleDescription = "indeni will alert if a pool member which should be available is not.",
        metricName = "lb-pool-member-state",
        applicableMetricTag = "name",
        descriptionMetricTag = "pool-name",
        alertItemsHeader = "Pool Members Affected",
        alertDescription = "Certain pool members which should available are not. Review list below.",
        baseRemediationText = "Determine why the members are down and resolve the issue as soon as possible.")() +
      new StateDownTemplateRule(context,
        ruleName = "lb_server_down",
        ruleFriendlyName = "Load Balancers: Server(s) down",
        ruleDescription = "indeni will alert one or more servers that the load balancer is directing traffic to is down.",
        metricName = "lb-server-state",
        applicableMetricTag = "name",
        alertItemsHeader = "Servers Affected",
        alertDescription = "One or more servers, to which this device is forwarding traffic, are down.",
        baseRemediationText = "Review the cause for the servers being down.")() +
      new StateDownTemplateRule(context,
        ruleName = "all_devices_ntp_not_syncing",
        howLongMustBeInState = TimeSpan.fromHours(3),
        ruleFriendlyName = "All Devices: NTP sync failure(s)",
        ruleDescription = "indeni will alert if one or more of the configured NTP servers is not syncing correctly.",
        metricName = "ntp-server-state",
        applicableMetricTag = "name",
        alertItemsHeader = "NTP Servers Affected",
        alertDescription = "One or more NTP servers configured on this device is not responding.",
        baseRemediationText = "Review the cause for the NTP sync not working.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Review sk92602: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk92602",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Run \"show ntp\" and review the status of each NTP server. You can also review the dagger.log, based on https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/Management-TKB/article-id/2078"
      ) +
      new StateDownTemplateRule(context,
        ruleName = "all_devices_dns_failure",
        howLongMustBeInState = TimeSpan.fromHours(1),
        ruleFriendlyName = "All Devices: DNS lookup failure(s)",
        ruleDescription = "indeni will alert if the DNS resolution is not working on the device. It will do so by attempting to resolve www.indeni.com.",
        metricName = "dns-server-state",
        applicableMetricTag = "name",
        alertItemsHeader = "DNS Servers Affected",
        alertDescription = "One or more DNS servers configured on this device are not responding or are failing to resolve www.indeni.com.",
        baseRemediationText = "Review the cause for the DNS resolution not working.")() +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_cluster_down_novsx",
        ruleFriendlyName = "Clustered Devices (Non-VS): Cluster down",
        ruleDescription = "indeni will alert if a cluster is down or any of the members are inoperable.",
        metricName = "cluster-state",
        applicableMetricTag = "name",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"),
        alertItemsHeader = "Clustering Elements Affected",
        alertDescription = "One or more clustering elements in this device are down.",
        baseRemediationText = "Review the cause for one or more members being down or inoperable.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Review other alerts for a cause for the cluster failure.",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device over SSH and run \"less mp-log ha-agent.log\" for more information."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_cluster_down_vsx",
        ruleFriendlyName = "Clustered Devices (VS): Cluster down",
        ruleDescription = "indeni will alert if a cluster is down or any of the members are inoperable.",
        metricName = "cluster-state",
        applicableMetricTag = "name",
        descriptionMetricTag = "vs.name",
        alertItemsHeader = "Clustering Elements Affected",
        alertDescription = "One or more clustering elements in this device are down.",
        baseRemediationText = "Review the cause for one or more members being down or inoperable.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Review other alerts for a cause for the cluster failure.",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device over SSH and run \"less mp-log ha-agent.log\" for more information."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_log_servers_not_communicating",
        howLongMustBeInState = TimeSpan.fromMinutes(30),
        ruleFriendlyName = "All Devices: Communication issues with certain log servers",
        ruleDescription = "indeni will alert if any of the log servers a device is set to send logs to is not communicating.",
        metricName = "log-server-communicating",
        applicableMetricTag = "name",
        alertItemsHeader = "Log Servers Affected",
        alertDescription = "One or more logging servers are not communicating.",
        baseRemediationText = "Review the possible cause for this.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Read https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk40090"
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_mgmt_component_down_novsx",
        ruleFriendlyName = "Management Devices: Management service down (Non-Virtual)",
        ruleDescription = "Alert if the management component is down on a device.",
        metricName = "mgmt-status",
        stateDescriptionComplexMetricName = "mgmt-status-description",
        alertDescription = "The management component on this device is down.",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"),
        baseRemediationText = "This may be due to someone stopping the management component itself, a licensing or a performance issue.")(
        ConditionalRemediationSteps.VENDOR_CP -> "The management service is handled by the \"fwm\" process. Run \"cpstat mg\" for more details. Review the licenses installed on the device, as well as whether or not anyone has run cpstop recently."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_mgmt_component_down_vsx",
        ruleFriendlyName = "Management Devices: Management service down (Virtual)",
        ruleDescription = "Alert if a management component is down on a device.",
        metricName = "mgmt-status",
        applicableMetricTag = "vs.name",
        alertItemsHeader = "Management Systems Affected",
        alertDescription = "One or more management components on this device are down.",
        baseRemediationText = "This may be due to someone stopping the management component itself, a licensing or a performance issue.")(
        ConditionalRemediationSteps.VENDOR_CP -> "The management service is handled by the \"fwm\" process (for each domain/CMA). Run \"mdsstat\" for more details. Review the licenses installed on the device, as well as whether or not anyone has run cpstop recently."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_mgmt_ha_sync_down",
        howLongMustBeInState = TimeSpan.fromHours(1),
        ruleFriendlyName = "Management Devices: Management high-availability sync down",
        ruleDescription = "Alert if the HA sync between management devices is not operating correctly.",
        metricName = "mgmt-ha-sync-state",
        stateDescriptionComplexMetricName = "mgmt-ha-sync-state-description",
        applicableMetricTag = "name",
        alertItemsHeader = "Management Systems Affected",
        alertDescription = "The high availability (HA) sync isn't operating correctly.",
        baseRemediationText = "This may due to a variety of underlying issues.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Review http://supportcontent.checkpoint.com/solutions?id=sk54160"
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_hardware_element_status",
        ruleFriendlyName = "All Devices: Hardware element down",
        ruleDescription = "Alert if any hardware elements are not operating correctly.",
        metricName = "hardware-element-status",
        applicableMetricTag = "name",
        alertItemsHeader = "Hardware Elements Affected",
        alertDescription = "The hardware elements listed below are not operating correctly.",
        baseRemediationText = "Troubleshoot the hardware element as soon as possible.")(
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_connection_from_mgmt_to_device",
        howLongMustBeInState = TimeSpan.fromHours(1),
        ruleFriendlyName = "All Devices: Communication between management server and specific devices not working",
        ruleDescription = "A management server needs to communicate with its managed devices at all times. indeni will alert if the communication is broken.",
        metricName = "trust-connection-state",
        applicableMetricTag = "name",
        alertItemsHeader = "Unreachable Managed Devices",
        alertDescription = "Some of the devices managed by this device cannot be reached by the management device. Please review the list below. Note that the list may include devices that are not covered by indeni at this point, as the check is done from the management server to all managed devices.",
        baseRemediationText = "Troubleshoot any possible connectivity issues.") (
        ConditionalRemediationSteps.VENDOR_CP -> "Read https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk60522"
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_debug_on",
        ruleFriendlyName = "All Devices: Debug mode enabled",
        ruleDescription = "indeni will alert if one of the debug mechanisms on a device is enabled when the default is for it to be disabled.",
        metricName = "debug-status",
        applicableMetricTag = "name",
        alertIfDown = false,
        alertItemsHeader = "Debugs Enabled",
        alertDescription = "One or more debug flags or components is enabled. Leaving debug on for too long may result in performance issues.",
        baseRemediationText = "Turn off the debug as soon as possible.",
        itemSpecificDescription = Seq(
          "^watchdog$".r -> "In the event of a system lock-up, the watchdog process ensures that the BIG-IP system restarts and fails over. In order to force the BIG-IP system to produce a core file for diagnostic purposes, administrators must disable the watchdog process to allow the core file to be written to disk before the system restarts. Re-enable the watchdog.",
          ".*".r -> ""
        )
        )(
        ConditionalRemediationSteps.VENDOR_CP -> "If the above list includes kernel debugging, run \"fw ctl debug 0\" to clear the debugs.",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device using SSH, type \"debug \" and then begin typing the items listed above. Usually the last term in the command can be replaced with a \"show\" or something similar to identify the current settings."
      ) +
      new StateDownTemplateRule(context,
        ruleName = "cross_vendor_syslog_tcp_accessible",
        ruleFriendlyName = "All Devices: TCP syslog server is not reachable",
        ruleDescription = "indeni will alert if one of the Syslog servers configured for use over TCP is not responding.",
        metricName = "tcp-syslog-state",
        applicableMetricTag = "name",
        alertItemsHeader = "Unreachable Syslog Servers",
        alertDescription = "One of the configured syslog servers is not reachable from the device. This could result in lost messages which would result in traceability being severely impacted.",
        baseRemediationText = "Verify that the firewall is allowing the traffic and that the syslog service is running."
      )(
      )


    // Value above or below a simple threshold
    rules = rules +
      new NumericThresholdOnDoubleMetricTemplateRule(context,
        ruleName = "cross_vendor_uptime_low",
        ruleFriendlyName = "All Devices (Non-VSX): Device restarted (uptime low)",
        ruleDescription = "indeni will alert when a device has restarted.",
        severity = AlertSeverity.CRITICAL,
        metricName = "uptime-seconds",
        threshold = TimeSpan.fromMinutes(60),
        thresholdDirection = ThresholdDirection.BELOW,
        alertDescriptionFormat = "The current uptime is %.0f seconds which seems to indicate the device has restarted.",
        baseRemediationText = "Determine why the device was restarted.",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true")
      )() +
      new NumericThresholdOnDoubleMetricWithItemsTemplateRule(context,
        ruleName = "cross_vendor_uptime_low_vsx",
        ruleFriendlyName = "All Devices (VSX): Virtual systems restarted (uptime low)",
        ruleDescription = "indeni will alert when a virtual system has restarted.",
        severity = AlertSeverity.CRITICAL,
        metricName = "uptime-seconds",
        threshold = TimeSpan.fromMinutes(60),
        thresholdDirection = ThresholdDirection.BELOW,
        applicableMetricTag = "vs.name",
        alertItemsHeader = "Affected Virtual Systems",
        alertItemDescriptionFormat = "The current uptime is %.0f seconds which seems to indicate the virtual system has restarted.",
        alertDescription = "Some virtual systems on this device have restarted recently. Review the list below.",
        baseRemediationText = "Determine why the virtual system(s) was restarted."
      )() +
      new NumericThresholdOnDoubleMetricTemplateRule(context,
        ruleName = "cross_vendor_uptime_high",
        ruleFriendlyName = "All Devices: Device uptime too high",
        ruleDescription = "indeni will alert when a device's uptime is too high.",
        severity = AlertSeverity.ERROR,
        metricName = "uptime-seconds",
        threshold = TimeSpan.fromDays(365 * 10),
        thresholdDirection = ThresholdDirection.ABOVE,
        alertDescriptionFormat = "The current uptime is %.0f seconds. This alert identifies when a device has been up for a very long time and may need an upgrade.",
        baseRemediationText = "Upgrade the device. You may also change the alert's threshold, or disable the alert completely, if not needed."
      )()

    // Cluster snapshot comparison, not vendor specific
    rules = rules +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_osname",
        ruleFriendlyName = "Clustered Devices: OS name mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the OS installed is different.",
        metricName = "os-name",
        isArray = false,
        alertDescription = "The members of a cluster of devices must have the same OS's installed.",
        baseRemediationText = """Install the correct versions of software on each device.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_osversion",
        ruleFriendlyName = "Clustered Devices: OS version mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the OS installed is different.",
        metricName = "os-version",
        isArray = false,
        alertDescription = "The members of a cluster of devices must have the same OS's installed (including the same version).",
        baseRemediationText = """Install the correct versions of software on each device.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_model",
        ruleFriendlyName = "Clustered Devices: Model mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the model of device in use is different.",
        metricName = "model",
        isArray = false,
        alertDescription = "The members of a cluster of devices must have the same device models in use.",
        baseRemediationText = "Replace one of the devices to match the other.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "static_routing_table_comparison_novsx",
        ruleFriendlyName = "Clustered Devices (Non-VS): Static routing table does not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if their static routing tables are different.",
        metricName = "static-routing-table",
        isArray = true,
        alertDescription = "Devices that are part of a cluster must have the same static routing tables. Review the differences below.",
        baseRemediationText = "Ensure the static routing table matches across devices in a cluster.",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"))(
        ConditionalRemediationSteps.VENDOR_CP -> "Use the \"show configuration\" command in clish to compare the calls to \"set static-route\".") +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "static_routing_table_comparison_vsx",
        ruleFriendlyName = "Clustered Devices (VS): Static routing table does not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if their static routing tables are different.",
        metricName = "static-routing-table",
        applicableMetricTag = "vs.name",
        isArray = true,
        alertDescription = "Devices that are part of a cluster must have the same static routing tables. Review the differences below.",
        baseRemediationText = "Ensure the static routing table matches across devices in a cluster.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Use the \"show configuration\" command in clish to compare the calls to \"set static-route\".") +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_timezone",
        ruleFriendlyName = "Clustered Devices: Timezone mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the timezone setting is different.",
        metricName = "timezone",
        isArray = false,
        alertDescription = "The members of a cluster of devices must have the same timezone settings.",
        baseRemediationText = """Review the settings of each device in the cluster and ensure they are the same.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_login_banner",
        ruleFriendlyName = "Clustered Devices: Login banner mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the login banner setting is different.",
        metricName = "login-banner",
        isArray = false,
        alertDescription = "The members of a cluster of devices must have the same login banner settings.",
        baseRemediationText = """Review the settings of each device in the cluster and ensure they are the same.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_domain",
        ruleFriendlyName = "Clustered Devices: Cluster members' domain names mismatch",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the domain setting is different.",
        metricName = "domain",
        isArray = false,
        alertDescription = "The members of a cluster of devices must have the same domain settings.",
        baseRemediationText = """Review the settings of each device in the cluster and ensure they are the same.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_features_enabled_comparison",
        ruleFriendlyName = "Clustered Devices: Features enabled do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the features they have enabled are different.",
        metricName = "features-enabled",
        isArray = true,
        alertDescription = "Devices that are part of a cluster must have the same features enabled. Review the differences below.",
        baseRemediationText = "Review the licensing and enabled features or modules on each device to ensure they match.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_radius-servers_comparison",
        ruleFriendlyName = "Clustered Devices: RADIUS servers used do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the RADIUS servers they are using are different.",
        metricName = "radius-servers",
        isArray = true,
        alertDescription = "Devices that are part of a cluster should have the same RADIUS servers configured. Review the differences below.",
        baseRemediationText = "Review the RADIUS configuration on each device to ensure they match.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_tacacs-servers_comparison",
        ruleFriendlyName = "Clustered Devices: TACACS servers used do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the TACACS servers they are using are different.",
        metricName = "tacacs-servers",
        isArray = true,
        alertDescription = "Devices that are part of a cluster should have the same TACACS servers configured. Review the differences below.",
        baseRemediationText = "Review the TACACS configuration on each device to ensure they match.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_ntp-servers_comparison",
        ruleFriendlyName = "Clustered Devices: NTP servers used do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the NTP servers they are using are different.",
        metricName = "ntp-servers",
        isArray = true,
        alertDescription = "Devices that are part of a cluster must have the same NTP servers used. Review the differences below.",
        baseRemediationText = "Review the NTP configuration on each device to ensure they match.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_policy_fingerprint",
        ruleFriendlyName = "Clustered Devices: Policy mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the policy installed is different.",
        metricName = "policy-installed-fingerprint",
        isArray = false,
        alertDescription = "The members of a cluster of devices must have the same policy installed.",
        baseRemediationText = """Review the policy installed on each device in the cluster and ensure they are the same.""")(
        ConditionalRemediationSteps.VENDOR_CP -> "Normally the management server ensures the same policy was installed on all cluster members. It's possible the checkbox for ensuring this was unchecked in the most recent policy installation. Please re-install the policy.") +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_compare_md5_across_cluster",
        ruleFriendlyName = "Clustered Devices: Critical configuration files mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if critical configuration files are different.",
        metricName = "file-md5sum",
        applicableMetricTag = "path",
        isArray = false,
        alertDescription = "Devices that are part of a cluster must have the same settings in their critical configuration files. Review the differences below.",
        baseRemediationText = """Correct any differences found to ensure a complete match between device members.""",
        alertItemsHeader = "Mismatching Files",
        includeSnapshotDiff = false)() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "connected_tables_comparison",
        ruleFriendlyName = "Clustered Devices: Connected networks do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the networks they are directly connected to do not match.",
        metricName = "connected-networks-table",
        isArray = true,
        alertDescription = "Devices that are part of a cluster must have the same directly connected networks. Review the differences below.",
        baseRemediationText = "Ensure all of the required ports are configured correctly on all cluster members, including the subnet mask.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_snmp_enabled_comparison",
        ruleFriendlyName = "Clustered Devices: SNMP enablement setting does not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
        metricName = "snmp-enabled",
        isArray = false,
        alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
        baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_snmp_contact_comparison",
        ruleFriendlyName = "Clustered Devices: SNMP contact information does not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
        metricName = "snmp-contact",
        isArray = false,
        alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
        baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_snmp_location_comparison",
        ruleFriendlyName = "Clustered Devices: SNMP location information does not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
        metricName = "snmp-location",
        isArray = false,
        alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
        baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_snmp_communities_comparison",
        ruleFriendlyName = "Clustered Devices: SNMP community settings do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
        metricName = "snmp-communities",
        isArray = true,
        alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
        baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_snmp_trap_receivers_comparison",
        ruleFriendlyName = "Clustered Devices: SNMP trap receivers' settings do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
        metricName = "snmp-traps-reciever",
        isArray = true,
        alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
        baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "cross_vendor_snmp_traps_enabled_comparison",
        ruleFriendlyName = "Clustered Devices: SNMP traps enabled settings do not match across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
        metricName = "snmp-traps-status",
        isArray = true,
        alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
        baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")()

    // Capacity rules, not vendor specific
    rules = rules +
      new NearingCapacityTemplateRule(context,
        ruleName = "cross_vendor_dst_cache_overflow",
        ruleFriendlyName = "All Devices: High destination cache usage",
        ruleDescription = "indeni will alert when the number of entries stored in a device's destination cache is nearing the allowed limit.",
        usageMetricName = "destination-cache-usage",
        limitMetricName = "destination-cache-limit",
        threshold = 80.0,
        alertDescriptionFormat = "The destination cache table has %.0f entries where the limit is %.0f. This table is used to cache routing decisions and increase the speed of traffic forwarding.",
        baseRemediationText = "Identify the cause of the large destination cache. If it is due to a legitimate cause, such as a high number of hosts visible on the available networks, please contact your technical support provider.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Review sk74480: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk74480") +
      new NearingCapacityTemplateRule(context,
        ruleName = "arp_neighbor_overflow",
        ruleFriendlyName = "All Devices: High ARP cache usage",
        ruleDescription = "indeni will alert when the number of ARP entries stored by a device is nearing the allowed limit.",
        usageMetricName = "arp-total-entries",
        limitMetricName = "arp-limit",
        threshold = 80.0,
        alertDescriptionFormat = "The ARP table has %.0f entries where the limit is %.0f.",
        baseRemediationText = "Identify the cause of the large ARP table. If it is due to a legitimate cause, such as a high number of hosts visible on the available networks, please contact your technical support provider.")(
        ConditionalRemediationSteps.VENDOR_CP -> "Review sk43772: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk43772") +
      new NearingCapacityTemplateRule(context,
        ruleName = "neighbor_discovery_overflow",
        ruleFriendlyName = "All Devices: High neighbor discovery (ND) cache usage",
        ruleDescription = "indeni will alert when the number of neighbor discovery entries stored by a device is nearing the allowed limit.",
        usageMetricName = "neighbor-discovery-total-entries",
        limitMetricName = "neighbor-discovery-limit",
        threshold = 80.0,
        alertDescriptionFormat = "The neighbor discovery table has %.0f entries where the limit is %.0f.",
        baseRemediationText = "Identify the cause of the large neighbor discovery table. If it is due to a legitimate cause, such as a high number of hosts visible on the available networks, please contact your technical support provider.")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "cross_vendor_mac_table_limit",
        ruleFriendlyName = "All Devices: MAC cache usage high",
        ruleDescription = "indeni will alert when the number of MAC entries stored by a device is nearing the allowed limit.",
        usageMetricName = "mac-total-entries",
        limitMetricName = "mac-limit",
        threshold = 80.0,
        alertDescriptionFormat = "The MAC table has %.0f entries where the limit is %.0f.",
        baseRemediationText = "Identify the cause of the large MAC table. If it is due to a legitimate cause, such as a high number of hosts visible on the available networks, please contact your technical support provider.")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "routes_defined_limit",
        ruleFriendlyName = "All Devices: Maximum number of routes nearing (IPv4)",
        ruleDescription = "Many devices have a limit for the number of IPv4 routes that can be defined. indeni will alert prior to the number of routes reaching the limit.",
        usageMetricName = "routes-usage",
        limitMetricName = "routes-limit",
        threshold = 80.0,
        alertDescriptionFormat = "There are %.0f IPv4 routes defined where the limit is %.0f.",
        baseRemediationText = "Consider removing certain routes.")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "routes_defined_limit_ipv6",
        ruleFriendlyName = "All Devices: Maximum number of routes nearing (IPv6)",
        ruleDescription = "Many devices have a limit for the number of IPv6 routes that can be defined. indeni will alert prior to the number of routes reaching the limit.",
        usageMetricName = "routes-usage-ipv6",
        limitMetricName = "routes-limit-ipv6",
        threshold = 80.0,
        alertDescriptionFormat = "There are %.0f IPv6 routes defined where the limit is %.0f.",
        baseRemediationText = "Consider removing certain routes.")()

    // Multi-snapshot rules, not vendor specific
    rules = rules +
      new MultiSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_no_ntp_servers",
        ruleFriendlyName = "All Devices: No NTP servers configured",
        ruleDescription = "Many odd and complicated outages occur due to lack of clock synchronization between devices. In addition, logs may have the wrong time stamps. indeni will alert when a device has no NTP servers configured.",
        metricName = "ntp-servers",
        alertDescription = "This system does not have an NTP server configured. Many odd and complicated outages occur due to lack of clock synchronization between devices. In addition, logs may have the wrong time stamps.",
        baseRemediationText = "Configure one or more NTP servers to be used by this device for clock synchronization.",
        complexCondition = Equals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("ntp-servers").asMulti().mostRecent().noneable))(
        ConditionalRemediationSteps.VENDOR_F5 -> "Log into the Web interface and navigate to System -> Configuration -> Device -> NTP. Add the desired NTP servers and click \"update\"."
      )


    // Counter increase rules, not vendor specific
    rules = rules +
      new CounterIncreaseTemplateRule(context,
        ruleName = "cross_vendor_rx_missed_errors",
        ruleFriendlyName = "All Devices: rx_missed_errors increasing",
        ruleDescription = "indeni watches many important port/NIC specific counters. The rx_missed_errors is one of them.",
        metricName = "network-interface-rx-missed-errors",
        applicableMetricTag = "name",
        alertDescription = "Some interfaces are experiencing rx_missed_errors.",
        alertRemediationSteps = "Read http://serverfault.com/questions/702555/how-to-troubleshoot-rx-missed-errors .",
        alertItemsHeader = "Affected Interfaces"
      )()

    // Per-item capacity rules, not vendor specific
    rules = rules +
      new NearingCapacityWithItemsTemplateRule(context,
        ruleName = "license_usage_limit",
        ruleFriendlyName = "All Devices: License usage limit approaching",
        ruleDescription = "Some licenses are limited to a certain number of elements (such as maximum users). If any of the licenses is nearing its limit, an alert will be issued.",
        usageMetricName = "license-elements-used",
        limitMetricName = "license-elements-limit",
        applicableMetricTag = "name",
        threshold = 80.0,
        minimumValueToAlert = 2.0, // We don't want to alert if the license capacity is 1 and we're using one item, this is a common occurence and isn't an issue
        alertDescription = "Some licenses are nearing their limit. Review the list below.",
        alertItemDescriptionFormat = "The number of elements in use is %.0f where the limit is %.0f.",
        baseRemediationText = "Consider purchasing additional licenses.",
        alertItemsHeader = "Affected Licenses")() +
      new NearingCapacityWithItemsTemplateRule(context,
        ruleName = "system_resource_usage_limit",
        ruleFriendlyName = "All Devices: System resource usage high",
        ruleDescription = "Some devices have a number of different resource limitations (such as maximum number of interfaces, routes, etc.). indeni will track the actual usage of these resources and alert if the limit is nearing or reached.",
        usageMetricName = "system-resource-usage",
        limitMetricName = "system-resource-limit",
        applicableMetricTag = "name",
        threshold = 80.0,
        alertDescription = "Some system resources are nearing their limit. Review the list below.",
        alertItemDescriptionFormat = "The number of items in use is %.0f where the limit is %.0f.",
        baseRemediationText = "Depending on the system resource nearing capacity, review any configuration changes required.",
        alertItemsHeader = "Affected Resources")() +
      new NearingCapacityWithItemsTemplateRule(context,
        ruleName = "cross_vendor_high_memory_usage",
        ruleFriendlyName = "All Devices: High memory usage",
        ruleDescription = "indeni will alert if the memory utilization of a device is above a high threshold. If the device has multiple memory elements, each will be inspected separately and alert for.",
        usageMetricName = "memory-usage",
        applicableMetricTag = "name",
        threshold = 90.0,
        alertDescription = "Some memory elements are nearing their maximum capacity.",
        alertItemDescriptionFormat = "Current memory utilization is: %.0f%%",
        baseRemediationText = "Determine the cause for the high memory usage of the listed elements.",
        alertItemsHeader = "Memory Elements Affected")(
        ConditionalRemediationSteps.VENDOR_CP -> "Consider reading https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33781#MEMORY",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Consider opening a support ticket with Palo Alto Networks.",
        ConditionalRemediationSteps.VENDOR_CISCO -> "Review http://docwiki.cisco.com/wiki/Cisco_Nexus_7000_Series_NX-OS_Troubleshooting_Guide_--_Troubleshooting_Memory"
      )

    // Complex value verification rules, not vendor specific
    rules = rules +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_snmp_v2",
        ruleFriendlyName = "All Devices: SNMPv2c/v1 used",
        ruleDescription = "As SNMPv2 is not very secure, indeni will alert if it is used.",
        metricName = "unencrypted-snmp-configured",
        alertDescription = "Older versions of SNMP do not use encryption. This could potentially allow an attacker to obtain valuable information about the infrastructure.",
        baseRemediationText = "Configure SNMPv3 instead.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("unencrypted-snmp-configured").asSingle().mostRecent().noneable)
      )(ConditionalRemediationSteps.VENDOR_F5 -> "Review https://support.f5.com/csp/article/K13625") +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_default_certification",
        ruleFriendlyName = "All Devices: Default certificate used",
        ruleDescription = "Many devices are pre-installed with a default SSL certificate. Generally, it's good practice to replace these to ensure security when accessing these devices. indeni will alert of a default certificate it used.",
        metricName = "default-management-certificate-used",
        alertDescription = "Using the default management certificate could enable a potential attacker to perform a man-in-the-middle attack without administrators knowing it. Therefore it is always recommended to use a certificate signed by a Certificate Authority that you trust. This indeni alert checks if the default management certificate is used and alerts if it is.",
        baseRemediationText = "Install a non-default certificate.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("default-management-certificate-used").asSingle().mostRecent().noneable)
      )(ConditionalRemediationSteps.VENDOR_F5 -> "Review https://support.f5.com/csp/article/K15664") +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_network_port_duplex_half",
        ruleFriendlyName = "All Devices: Network port(s) running in half duplex",
        ruleDescription = "indeni will alert one or more network ports is running in half duplex.",
        metricName = "network-interface-duplex",
        applicableMetricTag = "name",
        alertItemsHeader = "Ports Affected",
        alertDescription = "One or more ports are set to half duplex. This is usually an error. Review the list of ports below.",
        baseRemediationText = "Many times ports are in half duplex due to an autonegotation error or a misconfiguration.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("half"), SnapshotExpression("network-interface-duplex").asSingle().mostRecent().noneable)
      )(ConditionalRemediationSteps.VENDOR_CP -> "Review sk83760: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk83760",
        ConditionalRemediationSteps.VENDOR_PANOS -> "https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Display-Port-Information-Connected-Media-Interface/ta-p/61715") +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_network_port_speed_low",
        ruleFriendlyName = "All Devices: Network port(s) running in low speed",
        ruleDescription = "indeni will alert one or more network ports is running in a speed lower than 1000Mbps.",
        metricName = "network-interface-speed",
        applicableMetricTag = "name",
        alertItemsHeader = "Ports Affected",
        alertDescription = "One or more ports are set to a speed lower than 1000Mbps. This is usually an error. Review the list of ports below.",
        baseRemediationText = "Many times ports are in a low speed due to an autonegotation error or a misconfiguration.",
        complexCondition = Or(
          Equals(SnapshotExpression("network-interface-speed").asSingle().mostRecent().noneable, RuleHelper.createComplexStringConstantExpression("10M")),
          Equals(SnapshotExpression("network-interface-speed").asSingle().mostRecent().noneable, RuleHelper.createComplexStringConstantExpression("100M"))
        )
      )() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_vuln_sweet32",
        ruleFriendlyName = "All Devices: Device is vulnerable to SWEET32",
        ruleDescription = "indeni will check if a device is currently vulnerable to SWEET32.",
        metricName = "vuln-sweet32",
        alertDescription = "This device, in its current configuration, is vulnerable to SWEET32 ( https://sweet32.info/ ). This means that an eavesdropper may be able to decrypt certain important traffic packets.",
        baseRemediationText = "Follow the vendor-recommended remediation steps.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("vuln-sweet32").asSingle().mostRecent().noneable)
      )(ConditionalRemediationSteps.VENDOR_CP -> "https://supportcontent.checkpoint.com/solutions?id=sk113114",
        ConditionalRemediationSteps.VENDOR_F5 -> "https://support.f5.com/csp/#/article/K13167034",
        ConditionalRemediationSteps.VENDOR_PANOS -> "https://live.paloaltonetworks.com/t5/Threat-Articles/Information-on-Sweet32-for-Palo-Alto-Networks-Customers/ta-p/128526") +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_http_server_enabled",
        ruleFriendlyName = "All Devices: An HTTP server is enabled on the device",
        ruleDescription = "indeni will check if a device has the HTTP service enabled. HTTP is not encrypted and is therefore a security risk.",
        metricName = "http-server-enabled",
        alertDescription = "The HTTP server allows unencrypted control traffic to network devices. It transmits all data in clear text, including passwords and other potentially confidential information.",
        baseRemediationText = "Disable the HTTP server on the device.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("http-server-enabled").asSingle().mostRecent().noneable)
      )(ConditionalRemediationSteps.OS_NXOS -> "You can do so by using the \"no feature http-server\" configuration command.\nYou can verify that HTTP has been disabled by using the \"show http-server\" command."
        ) +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "cross_vendor_telnet_enabled",
        ruleFriendlyName = "All Devices: Telnet is enabled on the device",
        ruleDescription = "indeni will check if a device has Telnet enabled. Telnet is not encrypted and is therefore a security risk.",
        metricName = "telnet-enabled",
        alertDescription = "Telnet allows unencrypted control traffic to network devices. It transmits all data in clear text, including passwords and other potentially confidential information.",
        baseRemediationText = "Disable Telnet on the device.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("telnet-enabled").asSingle().mostRecent().noneable)
      )(ConditionalRemediationSteps.OS_NXOS -> "Disable Telnet by using the \"no feature telnet\" configuration command.\nYou can verify that Telnet has been disabled by using the \"show telnet server\" command."
      )

    // Check Point rules
    rules = rules +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "chkp_no_policy_no_vsx",
        ruleFriendlyName = "Check Point Firewalls (Non-VSX): No firewall policy loaded",
        ruleDescription = "indeni will alert when a Check Point firewall is running without a policy.",
        metricName = "policy-installed-fingerprint",
        alertDescription = "It appears the firewall does not have a valid policy. It's possible this is due to \"fw unloadlocal\".",
        baseRemediationText = "Ensure a valid policy is installed.",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"),
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression(""), SnapshotExpression("policy-installed-fingerprint").asSingle().mostRecent().noneable)
      )() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "chkp_no_policy_vsx",
        ruleFriendlyName = "Check Point Firewalls (VSX): No firewall policy loaded",
        ruleDescription = "indeni will alert when a Check Point firewall is running without a policy.",
        metricName = "policy-installed-fingerprint",
        applicableMetricTag = "vs.name",
        alertItemsHeader = "Affected VS's",
        alertDescription = "It appears the firewall does not have a valid policy. It's possible this is due to \"fw unloadlocal\".",
        baseRemediationText = "Ensure a valid policy is installed.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression(""), SnapshotExpression("policy-installed-fingerprint").asSingle().mostRecent().noneable)
      )() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "check_point_radius_uid",
        ruleFriendlyName = "Check Point Devices: RADIUS server uid is not 0",
        ruleDescription = "When configuring access through RADIUS, it is important to set the uid granted to the user to 0 so they have root access.",
        metricName = "radius-super-user-id",
        alertDescription = "The RADIUS Super User UID is the uid an administrator gets when entering expert mode (after authenticating via RADIUS). If this is not uid 0, then the administrator might have permission problems, preventing some commands from operating correctly.",
        baseRemediationText = "Set the Super User UID to 0. In clish: \"set aaa radius-servers super-user-uid 0\" or via the webUI set it under User Management -> Authentication Servers.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("0"), SnapshotExpression("radius-super-user-id").asSingle().mostRecent().noneable).not
      )() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "chkp_initial_policy_no_vsx",
        ruleFriendlyName = "Check Point Firewalls (Non-VSX): Firewall policy in InitialPolicy",
        ruleDescription = "indeni will alert when a Check Point firewall is running with the InitialPolicy policy.",
        metricName = "policy-installed-fingerprint",
        alertDescription = "It appears the firewall is in InitialPolicy. This may result in traffic disruption.",
        baseRemediationText = "Ensure a valid policy is installed.",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"),
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("InitialPolicy"), SnapshotExpression("policy-installed-fingerprint").asSingle().mostRecent().noneable)
      )() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "chkp_initial_policy_vsx",
        ruleFriendlyName = "Check Point Firewalls (VSX): Firewall policy in InitialPolicy",
        ruleDescription = "indeni will alert when a Check Point firewall is running with the InitialPolicy policy.",
        metricName = "policy-installed-fingerprint",
        applicableMetricTag = "vs.name",
        alertItemsHeader = "VS's in InitialPolicy",
        alertDescription = "It appears the firewall is in InitialPolicy. This may result in traffic disruption.",
        baseRemediationText = "Ensure a valid policy is installed.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("InitialPolicy"), SnapshotExpression("policy-installed-fingerprint").asSingle().mostRecent().noneable)
      )() +
      new NearingCapacityTemplateRule(context,
        ruleName = "chkp_vmalloc_usage_high",
        ruleFriendlyName = "Check Point Firewalls: High vmalloc usage",
        ruleDescription = "indeni will alert when usage of vmalloc is high.",
        usageMetricName = "vmalloc-used-kbytes",
        limitMetricName = "vmalloc-total-kbytes",
        threshold = 80.0,
        alertDescriptionFormat = "vmalloc is using %.0f kilobytes where the limit is %.0f.",
        baseRemediationText = "Review sk84700.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "checkpoint_compare_securexl_setting_novsx",
        ruleFriendlyName = "Check Point Cluster (Non-VSX): SecureXL configuration mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SecureXL settings are different.",
        metricName = "securexl-status",
        isArray = false,
        alertDescription = "The members of a cluster of Check Point firewalls must have the same SecureXL settings.",
        baseRemediationText = """Compare the output of "fwaccel stat" across members of the cluster.""",
        metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"))() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "checkpoint_compare_jumbo_hotfix",
        ruleFriendlyName = "Check Point Cluster: Jumbo Hotfix Take mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the jumbo hot fix installed is different.",
        metricName = "hotfix-jumbo-take",
        isArray = false,
        alertDescription = "The members of a cluster of Check Point firewalls must have the same jumbo hot fix installed.",
        baseRemediationText = """Compare the output of "show installer package" (under CLISH) across members of the cluster.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "checkpoint_compare_securexl_setting_vsx",
        ruleFriendlyName = "Check Point Cluster (VSX): SecureXL configuration mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SecureXL settings are different for different VS's.",
        metricName = "securexl-status",
        applicableMetricTag = "vs.name",
        isArray = false,
        alertDescription = "The members of a cluster of Check Point firewalls must have the same SecureXL settings.",
        baseRemediationText = """Compare the output of "fwaccel stat" across members of the cluster, make sure to run the command in the correct vsenv context.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "checkpoint_compare_corexl_cores_enabled",
        ruleFriendlyName = "Check Point Cluster: CoreXL cores-enabled mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the number of CoreXL cores enabled are different.",
        metricName = "corexl-cores-enabled",
        isArray = false,
        alertDescription = "The members of a cluster of Check Point firewalls must have the same number of cores enabled.",
        baseRemediationText = """Compare the output of "fw ctl multik stat" across members of the cluster.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "checkpoint_compare_ccp_mode",
        ruleFriendlyName = "Check Point Cluster: ClusterXL CCP mode mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the CCP mode settings are different.",
        metricName = "clusterxl-ccp-mode",
        isArray = false,
        alertDescription = "The members of a cluster of Check Point firewalls must have the same CCP mode settings.",
        baseRemediationText = """Compare the output of "cphaprob -a if" across members of the cluster.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "checkpoint_compare_pbr_rules",
        ruleFriendlyName = "Check Point Cluster: PBR rules mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the PBR rules settings are different.",
        metricName = "pbr-rules",
        isArray = true,
        alertDescription = "The members of a cluster of Check Point firewalls must have the same PBR (policy based routing) settings.",
        baseRemediationText = """Compare the output of "show pbr rules" (under clish) across members of the cluster.""")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "checkpoint_compare_cluster_id",
        ruleFriendlyName = "Check Point Cluster: Cluster ID mismatch across cluster members",
        ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the cluster ID settings are different.",
        metricName = "cluster-id-number",
        isArray = false,
        alertDescription = "The members of a cluster of Check Point firewalls must have the same cluster ID set.",
        baseRemediationText = """Follow https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk25977 to ensure the cluster ID is configured the same on both members.""")() +
      new StateDownTemplateRule(context,
        ruleName = "check_point_ca_not_accessible",
        howLongMustBeInState = TimeSpan.fromHours(1),
        ruleFriendlyName = "Check Point Firewalls: Certificate authority not accessible",
        ruleDescription = "If the certificate authority is not accessible to a firewall, VPN tunnels relying on certificates may fail.",
        metricName = "ca-accessible",
        applicableMetricTag = "name",
        alertItemsHeader = "Unreachable Certificate Authorities",
        alertDescription = "Some of the certificate authority servers which this device considers to be those to be used during authentication (for example - for VPN) are not accessible. The CA servers for which an issue has been found are listed below. If the connectivity issue remains for more than a few hours, some VPN tunnels may fail.",
        baseRemediationText = "Identify why the device cannot initiate a connection with the listed servers.")() +
      new StateDownTemplateRule(context,
      ruleName = "check_point_aggressive_aging_novsx",
      ruleFriendlyName = "Check Point Firewalls (Non-VSX): Aggressive Aging enabled",
      ruleDescription = "Aggressive Aging turning on means the firewall is under an extreme load. If this happens, indeni will alert.",
      metricName = "chkp-agressive-aging",
      alertIfDown = false,
      alertDescription = "Aggressive Aging has started operating on this device.",
      baseRemediationText = "Run \"fw ctl pstat\" for more information. Determine what may be causing the excessive load on the firewall.",
      metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"))() +
      new StateDownTemplateRule(context,
      ruleName = "check_point_aggressive_aging_vsx",
      ruleFriendlyName = "Check Point Firewalls (VSX): Aggressive Aging enabled",
      ruleDescription = "Aggressive Aging turning on means the firewall is under an extreme load. If this happens, indeni will alert.",
      metricName = "chkp-agressive-aging",
      applicableMetricTag = "vs.name",
      alertIfDown = false,
      alertItemsHeader = "Affected VS's",
      alertDescription = "Aggressive Aging has started operating on this device.",
      baseRemediationText = "Run \"fw ctl pstat\" for more information. Determine what may be causing the excessive load on the firewall.")() +
      new StateDownTemplateRule(context,
      ruleName = "clusterxl_pnote_down_non_vsx",
      ruleFriendlyName = "Check Point ClusterXL (Non-VSX): Pnote(s) down",
      ruleDescription = "ClusterXL has multiple problem notifications (pnotes) - if any of them fail an alert will be issued.",
      metricName = "clusterxl-pnote-state",
      applicableMetricTag = "name",
      alertItemsHeader = "Problematic Elements",
      alertDescription = "This cluster member is down due to certain elements being in a \"problem state\".",
      baseRemediationText = "Review the list of problematic elements and take appropriate action.",
      metaCondition = !com.indeni.data.conditions.Equals("vsx", "true"),
      itemSpecificDescription = Seq (
        "(?i).*FIB.*".r -> "The FIB device is responsible for supporting dynamic routing under ClusterXL. Review the firewall logs to ensure traffic with the FIBMGR service is flowing correctly.",

        // Catch-all
        ".*".r -> "Please consult with your technical support provider about this pnote."
      ),
      // Ignore interface active check, we alert about interface count separately
      itemsToIgnore = Set ("Interface Active Check".r))() +
      new StateDownTemplateRule(context,
        ruleName = "clusterxl_pnote_down_vsx",
        ruleFriendlyName = "Check Point ClusterXL (VSX): Pnote(s) down",
        ruleDescription = "ClusterXL has multiple problem notifications (pnotes) - if any of them fail an alert will be issued.",
        metricName = "clusterxl-pnote-state",
        applicableMetricTag = "name",
        descriptionMetricTag = "vs.name",
        alertItemsHeader = "Problematic Elements",
        alertDescription = "Some VS's in this cluster member are down due to certain elements being in a \"problem state\".",
        baseRemediationText = "Review the list of problematic elements and take appropriate action.",
        itemSpecificDescription = Seq (
          "(?i).*FIB.*".r -> "The FIB device is responsible for supporting dynamic routing under ClusterXL. Review the firewall logs to ensure traffic with the FIBMGR service is flowing correctly.",

          // Catch-all
          ".*".r -> "Please consult with your technical support provider about this pnote."
        ),
        // Ignore interface active check, we alert about interface count separately
        itemsToIgnore = Set ("Interface Active Check".r))() +
      new MultiSnapshotValueCheckTemplateRule(context,
        ruleName = "checkpoint_ipassignment_errors",
        ruleFriendlyName = "Check Point Firewalls: Errors found in $FWDIR/conf/ipassignment.conf",
        ruleDescription = "The ipassignment.conf file is used for remote access VPN configuration. Any errors in the file's contents will be alerted on by indeni.",
        metricName = "ipassignment-conf-errors",
        alertDescription = "Errors in $FWDIR/conf/ipassignment.conf could cause Remote Access VPN clients to not be assigned their static IP address.",
        baseRemediationText = "See https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk105162",
        complexCondition = Not(Equals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("ipassignment-conf-errors").asMulti().mostRecent().noneable)))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "chkp_cluster_id_conflict",
        ruleFriendlyName = "Check Point ClusterXL: Cluster ID settings conflict",
        ruleDescription = "There are two ways to configure the cluster ID in a Check Point cluster - through cphaconf and through the kernel parameters. If both are used, odd issues may occur.",
        metricName = "chkp-cluster-id-conflict",
        alertDescription = "The device is mixing two methods for configuring the cluster ID or magic MAC. This is not supported and can cause undesirable consequences.",
        baseRemediationText = "Follow sk25977 on how to configure the cluster ID, depending on the version you are using.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("chkp-cluster-id-conflict").asSingle().mostRecent().noneable))()


    // Palo Alto Networks rules
    val paloAltoPoolRules: Set[DataplanePoolUsageRule] = Set(
      DataplanePoolUsageRule(context, "Packet Buffers", 80.0),
      DataplanePoolUsageRule(context, "Work Queue Entries", 80.0),
      DataplanePoolUsageRule(context, "software packet buffer 0", 80.0),
      DataplanePoolUsageRule(context, "software packet buffer 1", 80.0),
      DataplanePoolUsageRule(context, "software packet buffer 2", 80.0),
      DataplanePoolUsageRule(context, "software packet buffer 3", 80.0),
      DataplanePoolUsageRule(context, "software packet buffer 4", 80.0),
      DataplanePoolUsageRule(context, "CTD Flow", 80.0),
      DataplanePoolUsageRule(context, "CTD AV Block", 80.0),
      DataplanePoolUsageRule(context, "SML VM Fields", 80.0),
      DataplanePoolUsageRule(context, "SML VM Vchecks", 80.0),
      DataplanePoolUsageRule(context, "Detector Threats", 80.0),
      DataplanePoolUsageRule(context, "CTD DLP FLOW", 80.0),
      DataplanePoolUsageRule(context, "CTD DLP DATA", 80.0),
      DataplanePoolUsageRule(context, "CTD DECODE FILTER", 80.0),
      DataplanePoolUsageRule(context, "SML VM EmlInfo", 80.0),
      DataplanePoolUsageRule(context, "Regex Results", 80.0),
      DataplanePoolUsageRule(context, "TIMER Chunk", 80.0),
      DataplanePoolUsageRule(context, "FPTCP segs", 80.0),
      DataplanePoolUsageRule(context, "Proxy session", 80.0),
      DataplanePoolUsageRule(context, "SSL Handshake State", 80.0),
      DataplanePoolUsageRule(context, "SSL State", 80.0),
      DataplanePoolUsageRule(context, "SSL Handshake MAC State", 80.0),
      DataplanePoolUsageRule(context, "SSH Handshake State", 80.0),
      DataplanePoolUsageRule(context, "SSH State", 80.0),
      DataplanePoolUsageRule(context, "TCP host connections", 80.0),
      DataplanePoolUsageRule(context, "DFA Result", 80.0)
    )
    rules = rules ++ paloAltoPoolRules + GenericDataplanePoolUsageRule(context) +
      new NearingCapacityWithItemsTemplateRule(context,
        ruleName = "palo_alto_networks_high_logdb_usage",
        ruleFriendlyName = "Palo Alto Networks Firewalls: High log DB usage",
        ruleDescription = "indeni will alert if the log DB utilization of a device is above a high threshold.",
        usageMetricName = "logdb-usage",
        applicableMetricTag = "name",
        threshold = 95.0,
        alertDescription = "Some log DBs are nearing their quota. The device will automatically purge old logs (per https://live.paloaltonetworks.com/t5/Management-Articles/When-are-Logs-Purged-on-the-Palo-Alto-Networks-Devices/ta-p/53605). This may be a critical issue if these logs are not retained and should be.",
        alertItemDescriptionFormat = "Current log DB utilization is: %.0f%%",
        baseRemediationText = "More information is available at https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Determine-How-Much-Disk-Space-is-Allocated-to-Logs/ta-p/53828",
        alertItemsHeader = "Log DBs Nearing Quota")() +
      new CounterIncreaseTemplateRule(context,
        ruleName = "palo_alto_vpn_auth_errors",
        ruleFriendlyName = "Palo Alto Networks Firewalls: VPN dropping packets due to authentication errors",
        ruleDescription = "indeni tracks critical error metrics for VPN tunnels and alerts when these are increasing.",
        metricName = "vpn-tunnel-authentication-errors",
        applicableMetricTag = "peerip",
        alertDescription = "The VPNs listed below are experiencing packet authentication errors. This is probably due to a configuration issue.",
        alertRemediationSteps = "Review the configurations on both sides of the tunnel.",
        alertItemsHeader = "Affected VPN Tunnels"
      )() +
      new CounterIncreaseTemplateRule(context,
        ruleName = "palo_alto_vpn_decryption_errors",
        ruleFriendlyName = "Palo Alto Networks Firewalls: VPN dropping packets due to decryption errors",
        ruleDescription = "indeni tracks critical error metrics for VPN tunnels and alerts when these are increasing.",
        metricName = "vpn-tunnel-decryption-errors",
        applicableMetricTag = "peerip",
        alertDescription = "The VPNs listed below are experiencing packet decryption errors. This is probably due to a configuration issue.",
        alertRemediationSteps = "Review the configurations on both sides of the tunnel.",
        alertItemsHeader = "Affected VPN Tunnels"
      )() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "panw_update_action_download_only",
        ruleFriendlyName = "Palo Alto Networks Firewalls: Update schedule set to download only",
        ruleDescription = "indeni will alert if the update schedule for application packages is set to download only.",
        metricName = "panw-app-update-action",
        alertDescription = "The update schedule is set to download only (without installation). This is against Palo Alto Networks best practices.",
        baseRemediationText = "In the update schedule, select Download and Install.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("download-only"), SnapshotExpression("panw-app-update-action").asSingle().mostRecent().noneable)
      )()

    // Radware rules
    rules = rules +
      new NearingCapacityTemplateRule(context,
        ruleName = "radware_realserver_groups_defined_limit",
        ruleFriendlyName = "Radware Alteon: Real server groups defined limit approaching",
        ruleDescription = "Alteon devices allow for the configuration of multiple real server groups, up to a limit. indeni will alert prior to configuration reaching the limit.",
        usageMetricName = "real-server-groups-usage",
        limitMetricName = "real-server-groups-limit",
        threshold = 80.0,
        alertDescriptionFormat = "There are %.0f real server groups defined where the limit is %.0f.",
        baseRemediationText = "Consider removing certain real server groups.")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "radware_dynamic_store_limit",
        ruleFriendlyName = "Radware Alteon: Dynamic data store limit nearing",
        ruleDescription = "The dynamic data store is used by the AppShape++ feature. indeni will alert prior to the data store reaching its limit.",
        usageMetricName = "dynamic-data-store-usage",
        limitMetricName = "dynamic-data-store-limit",
        threshold = 80.0,
        alertDescriptionFormat = "The dynamic data store usage is %.0f where the limit is %.0f.",
        baseRemediationText = "Review https://kb.radware.com/Questions/Alteon/Public/ALERT-slb-Dynamic-Data-Store-is-at-FULL-capacity")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "radware_fdb_table_limit",
        ruleFriendlyName = "Radware Alteon: FDB table usage high",
        ruleDescription = "The Alteon Forwarding Data Base table maps a MAC address (for a given VLAN) with the port number for that MAC address. indeni will alert prior to the table reaching its limit.",
        usageMetricName = "fdb-usage",
        limitMetricName = "fdb-limit",
        threshold = 80.0,
        alertDescriptionFormat = "The FDB table usage is %.0f where the limit is %.0f.",
        baseRemediationText = "Review https://kb.radware.com/Questions/Alteon/Public/Alteon-Forwarding-Data-Base-(FDB)-Table")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "radware_filters_table_limit",
        ruleFriendlyName = "Radware Alteon: Filters table usage high",
        ruleDescription = "Alteon devices allow for the configuration of filters in a table. indeni will alert prior to the table reaching its limit.",
        usageMetricName = "filters-usage",
        limitMetricName = "filters-limit",
        threshold = 80.0,
        alertDescriptionFormat = "The filter table usage is %.0f where the limit is %.0f.",
        baseRemediationText = "Review the filters defined to determine if some of them may be removed.")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "radware_interfaces_defined_limit",
        ruleFriendlyName = "Radware Alteon: Interfaces defined nearing limit",
        ruleDescription = "Alteon devices allow for the configuration of multiple interfaces, up to a limit. indeni will alert prior to the limit.",
        usageMetricName = "interfaces-usage",
        limitMetricName = "interfaces-limit",
        threshold = 80.0,
        alertDescriptionFormat = "There are %.0f interfaces defined where the limit is %.0f.",
        baseRemediationText = "Consider removing certain interfaces.")() +
      new NearingCapacityTemplateRule(context,
        ruleName = "radware_realservers_defined_limit",
        ruleFriendlyName = "Radware Alteon: Real servers defined limit approaching",
        ruleDescription = "Alteon devices allow for the configuration of multiple real servers, up to a limit. indeni will alert prior to configuration reaching the limit.",
        usageMetricName = "real-servers-usage",
        limitMetricName = "real-servers-limit",
        threshold = 80.0,
        alertDescriptionFormat = "There are %.0f real servers defined where the limit is %.0f.",
        baseRemediationText = "Consider removing certain real servers.")()

    // F5 rules
    rules = rules +
      new StateDownTemplateRule(context,
        ruleName = "f5_ssl_weak_cipher",
        ruleFriendlyName = "F5 Devices: Weak cipher used with SSL profiles",
        ruleDescription = "Certain ciphers are now considered weak. indeni will alert if any SSL profiles are set to use them.",
        metricName = "ssl-weak-cipher",
        applicableMetricTag = "name",
        descriptionMetricTag = "cipher",
        alertIfDown = false,
        alertItemsHeader = "Affected Profiles",
        alertDescription = "Certain SSL profiles have a weak cipher and are potentially opening traffic to known vulnerabilities.",
        baseRemediationText = "Follow the knowledge articles listed in the affected profiles above. Since F5 devices present the attributes in alphabetical order (to the other side), be careful when adding a property.",
        itemSpecificDescription = Seq(
          ".*DES-CBC3.*".r -> "The DES-CBC3 cipher is considered weak. See https://support.f5.com/csp/#/article/K13167034",
          ".*RC4.*".r -> "The RC4 cipher is considered weak. See https://support.f5.com/csp/#/article/K16864",
          ".*".r -> "")
      )() +
      new StateDownTemplateRule(context,
        ruleName = "f5_ssl_weak_protocol",
        ruleFriendlyName = "F5 Devices: Weak security protocol used with SSL profiles",
        ruleDescription = "Certain security protocols are now considered weak. indeni will alert if any SSL profiles are set to use them.",
        metricName = "ssl-weak-protocol",
        applicableMetricTag = "name",
        descriptionMetricTag = "protocol",
        alertIfDown = false,
        alertItemsHeader = "Affected Profiles",
        alertDescription = "Certain SSL profiles have a weak security protocol and are potentially opening traffic to known vulnerabilities.",
        baseRemediationText = "Follow the knowledge articles listed in the affected profiles above.",
        itemSpecificDescription = Seq(
          ".*SSLv3.*".r -> "This profile is using a vulnerable communication protocol. See https://support.f5.com/csp/#/article/K15702",
          ".*SSLv2.*".r -> "This profile is using a vulnerable communication protocol. See https://support.f5.com/csp/#/article/K23196136",
          ".*".r -> ""))() +
      new StateDownTemplateRule(context,
        ruleName = "f5_ssl_weak_impl",
        ruleFriendlyName = "F5 Devices: SSL Ticketbleed vulnerability (CVE-2016-9244)",
        ruleDescription = "In February of 2017, F5 users were notified of a new vulnerability in certain versions of BIG-IP. indeni will alert if any devices are vulnerable.",
        metricName = "ssl-weak-impl",
        applicableMetricTag = "profile-name",
        descriptionMetricTag = "type",
        alertIfDown = false,
        alertItemsHeader = "Affected Profiles",
        alertDescription = "This device is vulnerable to CVE-2016-9244, also known as Ticketbleed. Review the affected profiles below.",
        baseRemediationText = "Read https://support.f5.com/csp/article/K05121675")() +
      new StateDownTemplateRule(context,
        ruleName = "f5_geoip_corrupt",
        ruleFriendlyName = "F5 Devices: Non-functioning geo-ip database",
        ruleDescription = "indeni will alert if the geo-ip database is corrupted.",
        metricName = "geoip-database-state",
        applicableMetricTag = "database",
        alertItemsHeader = "Affected Databased",
        alertDescription = "The geo-ip database provides meta data related to an IP address, such as city, region, country and ISP. Should the database be unavailable any attempts to retrieve geo-ip data could cause unpredictable behavior.",
        baseRemediationText = "Contact F5 support.")() +
      new SnapshotComparisonTemplateRule(context,
        ruleName = "f5_ha_group_comparison",
        ruleFriendlyName = "F5 Devices: Non-identical HA-group configuration detected",
        ruleDescription = "indeni will identify when two F5 devices are part of a device group and alert if the HA-group configuration is different.",
        metricName = "f5-ha-group",
        isArray = true,
        alertDescription = "HA-groups are one of the ways to determine if an F5 cluster should fail over or not by keeping track of trunk health and/or specific pool statuses. Should a link in a trunk fail, or a pool member stop responding this could trigger a fail-over. To minimize the risk of flapping an active bonus is highly recommended. Since this configuration is not synchronized it is ideal for it to be identical in all units of the cluster. Even more so, since F5's recommended way of manually failing over a cluster with ha-groups is to change the weight of the ha-group members. This is easily forgotten once done, which in turn could lead to the system not failing over when components do fail.",
        baseRemediationText = """Make sure that the HA-group configuration is exactly the same in both devices. You may optionally choose to ignore certain differences if they are intended.""")() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_port_not_locked_down",
        ruleFriendlyName = "F5 Devices: Self IP not locked down",
        ruleDescription = "Best practices dictate that the self IP should be locked down to admin services. indeni will alert if this is not the case.",
        metricName = "f5-port-lockdown-not-none",
        applicableMetricTag = "name",
        alertItemsHeader = "Self IP's affected",
        alertDescription = "There are self IPs configured on this device which are listening to open ports.",
        baseRemediationText = "Unless this is intentionally configured, such as a dedicated cable or VLAN for HA, it is always recommended to have the Self IP configuration set to \"Allow None\". Make sure to schedule a service window before configuring this option.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-port-lockdown-not-none").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_cookie_persistence",
        ruleFriendlyName = "F5 Devices: Unencrypted cookie persistence profiles found",
        ruleDescription = "According to best practices, cookies should be encrypted when persisting to client browser to avoid security issues. indeni will alert when this is not the case.",
        metricName = "f5-cookied-persistence-encrypted",
        applicableMetricTag = "name",
        alertItemsHeader = "Profiles Affected",
        alertDescription = "Some cookie persistence profiles do not have an encryption string configured. Not encrypting persistence cookies discloses internal information such as internal IP, port and pool name. This information could be used by an attacker to gather information about your environment.",
        baseRemediationText = "Review these instructions on how to enable persistence cookie encryption: \nhttps://support.f5.com/csp/article/K14784\n\nIt is best not to change the default profiles. Instead, create a new persistence profile with the default profile as parent. Cookie Encryption Use Policy should be set to Required in order for this alert not to be triggered.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("false"), SnapshotExpression("f5-cookied-persistence-encrypted").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_action_on_service_down",
        ruleFriendlyName = "F5 Devices: Action On Service Down should be set to \"Reject\"",
        ruleDescription = "The default option for \"Action On Service Down\" is \"None\", which maintains connections to pool member even when the monitor fails, but does not create new connections.\nIf using a good monitor that is able to determine the status of the member however, the better option in most cases is \"Reject\", which instead resets the existing connection and forces the client to establish a new one. This ensures that the client has an optimal chance of connecting to a functioning pool member.\nindeni will alert when the device configuration does not follow this best practice.",
        metricName = "f5-default-action-on-service-down",
        applicableMetricTag = "name",
        alertItemsHeader = "Pools Affected",
        alertDescription = "The default option for \"Action On Service Down\" is \"None\", which maintains connections to pool member even when the monitor fails, but does not create new connections.\nIf using a good monitor that is able to determine the status of the member however, the better option in most cases is \"Reject\", which instead resets the existing connection and forces the client to establish a new one. This ensures that the client has an optimal chance of connecting to a functioning pool member.\nAs with many things, there are exceptions to this, see the link below for more information.",
        baseRemediationText = "Read more about \"Action On Service Down\" at https://support.f5.com/csp/article/K15095",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("false"), SnapshotExpression("f5-service-down-is-reject").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_matchlass_used",
        ruleFriendlyName = "F5 Devices: iRule(s) uses the deprecated matchclass command",
        ruleDescription = "The matchclass command in iRules has been deprecated. indeni will alert if any iRules still use it.",
        metricName = "f5-matchclass-used",
        applicableMetricTag = "name",
        alertItemsHeader = "iRules Affected",
        alertDescription = "The command \"matchclass\" is used to check if a value is contained within a data group list. While still supported the command has been deprecated in favor of the more powerful and efficient \"class\" command.",
        baseRemediationText = "Information about the class command can be found at https://devcentral.f5.com/wiki/iRules.class.ashx",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-matchclass-used").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_automap_enabled",
        ruleFriendlyName = "F5 Devices: Automap enabled",
        ruleDescription = "Automap works great for assymetric routing, but can result in port exhaustion. indeni will alert if automap is used.",
        metricName = "f5-automap-used",
        applicableMetricTag = "name",
        alertItemsHeader = "Virtual Servers Affected",
        alertDescription = "Automap works great when avoiding asymmetric routing, but in scenarios with a lot of traffic there could be port exhaustion unless more than one floating IP is defined for the member VLAN. For this reason it's better to use a SNAT Pool containing the appropriate amount of IP addresses used for address translation.",
        baseRemediationText = "Information about automap, SNAT Pools and port exhaustion is available at https://support.f5.com/csp/article/K7820#exhaustion",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-automap-used").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_fallback_host_used",
        ruleFriendlyName = "F5 Devices: Fallback host used in HTTP profile",
        ruleDescription = "A fallback host redirect a user to a different page/URI. It is in most cases better to use an iRule to rewrite the request. indeni will alert if fallback is used instead of an iRule.",
        metricName = "f5-fallbackhost-used",
        applicableMetricTag = "name",
        alertItemsHeader = "Profiles Affected",
        alertDescription = "A fallback host redirect a user to a different page/URI. It is in most cases better to use an iRule to rewrite the request. That way the user maintains the same URI and can hit refresh until the page is available again.",
        baseRemediationText = "It is in most cases better to use an iRule to rewrite the request. That way the user maintains the same URI and can hit refresh until the page is available again.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-fallbackhost-used").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_audit_enabled",
        ruleFriendlyName = "F5 Devices: Audit logging is disabled",
        ruleDescription = "Audit logging is important for traceability reasons in case of an outage, or a successful intrusion attempt. indeni will alert if audit is not enabled.",
        metricName = "f5-audit-enabled",
        alertDescription = "Audit logging is important for traceability reasons in case of an outage, or a successful intrusion attempt.",
        baseRemediationText = "An administrator could verify that auditing is enabled by logging into the web interface and clicking on \"System\" -> \"Logs\" -> \"Configuration\" -> \"Options\". On that page, make sure that audit logging for \"MCP\" and \"tmsh\" is set to either \"Enable\", \"Verbose\" or \"Debug\".\nMore information about TMM logging can be found here at https://support.f5.com/csp/article/K5532",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("disable"), SnapshotExpression("f5-audit-enabled").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_default_pool_http_monitor",
        ruleFriendlyName = "F5 Devices: Default HTTP monitor used for pool",
        ruleDescription = "Using the default monitor is not advisable as it does not have a HTTP version, host header or even user agent. indeni will alert if the default HTTP monitor is used.",
        metricName = "f5-default-pool-monitor-used",
        applicableMetricTag = "pool-name",
        alertItemsHeader = "Pools Affected",
        alertDescription = "Using the default monitor is not advisable as it does not have a HTTP version, host header or even user agent. Depending on server na log settings this might generate white noise in application logs as it could be unsupported. Also you would not get reliable test results as it does not have a receive string configured. This means that status code 500, 404, 401, etc. are all considered as OK, while a connection reset is not.",
        baseRemediationText = "Create an HTTP monitor specifically for the application and configure a receive string. An example of a receive string that would only accept status code 200 would be \"HTTP/1\\.1 200\".\nNote:\nWhen changing the monitor configuration there is always a risk of application down-time in case the configuration is wrong. Thus it is recommended to only do this during a service window.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-default-pool-monitor-used").asSingle().mostRecent().noneable))() +
      new SingleSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_default_member_http_monitor",
        ruleFriendlyName = "F5 Devices: Default HTTP monitor used for pool member",
        ruleDescription = "Using the default monitor is not advisable as it does not have a HTTP version, host header or even user agent. indeni will alert if the default HTTP monitor is used.",
        metricName = "f5-default-member-monitor-used",
        applicableMetricTag = "member-name",
        alertItemsHeader = "Pool Members Affected",
        alertDescription = "Using the default monitor is not advisable as it does not have a HTTP version, host header or even user agent. Depending on server na log settings this might generate white noise in application logs as it could be unsupported. Also you would not get reliable test results as it does not have a receive string configured. This means that status code 500, 404, 401, etc. are all considered as OK, while a connection reset is not.",
        baseRemediationText = "Create an HTTP monitor specifically for the application and configure a receive string. An example of a receive string that would only accept status code 200 would be \"HTTP/1\\.1 200\".\nNote:\nWhen changing the monitor configuration there is always a risk of application down-time in case the configuration is wrong. Thus it is recommended to only do this during a service window.",
        complexCondition = Equals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-default-member-monitor-used").asSingle().mostRecent().noneable))() +
      new MultiSnapshotValueCheckTemplateRule(context,
        ruleName = "f5_precompressed_content_in_compression_profile",
        ruleFriendlyName = "F5 Devices: Precompressed content-type found in HTTP Compression profile",
        ruleDescription = "Using the F5 device to compress content is a way of accelerating application content delivery. However, compressing content that is already pre-compressed results in longer response times and is using up system resources in vain. indeni will alert when already-compressed content types are set for compression.",
        metricName = "f5-compression-profile-precompressed-content-types",
        applicableMetricTag = "name",
        alertItemsHeader = "Profiles Affected",
        alertDescription = "Using the F5 device to compress content is a way of accelerating application content delivery. However, compressing content that is already pre-compressed results in longer response times and is using up system resources in vain.",
        baseRemediationText = "Remove any pre-compressed content type from the HTTP Compression profile. For each profile mentioned below, the specific content types that should be removed are listed.",
        complexCondition = Not(Equals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("f5-compression-profile-precompressed-content-types").asMulti().mostRecent().noneable)))() +
      new NearingCapacityTemplateRule(context,
        ruleName = "f5_vcmp_disk_usage",
        ruleFriendlyName = "F5 Devices (vCMP): High disk utilization by vCMP",
        ruleDescription = "indeni will alert if the disk utilization of a vCMP is above a high threshold.",
        usageMetricName = "f5-vcmp-host-disk-usage-percentage",
        threshold = 80.0,
        alertDescriptionFormat = "The disk utilization is currently %.0f%%, when disk space runs out it is no longer possible to create new vCMP guests.",
        baseRemediationText = "Run \"show vcmp virtual-disk kil\" to see the full data per vCMP. Purchase additional hardware or delete unused virtual disks. More information about virtual disk management can be found at https://support.f5.com/kb/en-us/products/big-ip_ltm/manuals/product/vcmp-viprion-configuration-11-4-1/7.html")() +
      new NearingCapacityWithItemsTemplateRule(context,
        ruleName = "f5_lb_pool_capacity",
        ruleFriendlyName = "F5 Devices: Pools operating at a low capacity",
        ruleDescription = "indeni will alert if the the number of members available in the pool is too low, based on the percentage of members available out of the total.",
        usageMetricName = "lb-pool-capacity",
        threshold = 50.0,
        thresholdDirection = ThresholdDirection.BELOW,
        applicableMetricTag = "name",
        alertItemsHeader = "Affected Pools",
        alertDescription = "The pools listed below have members that are not able to process traffic due to either failed monitors or members being disabled. This means that the pools may not be able to handle the traffic load they are meant to.",
        alertItemDescriptionFormat = "Only %.0f%% of pool members are available.",
        baseRemediationText = "Log into the device and examine the status of the pool. Troubleshoot the application in case of failed monitors and verify that any disabled members is intentional.\nTroubleshooting steps for HTTP monitors:\n1. Log into the device through SSH.\n2. Issue the following command: echo -ne \"<monitor send string>\" | nc <member ip> <member port>.\n3. Make sure you get a response and that the response matches any receive string you have configured.\nExample:\necho -ne \"GET / HTTP/1.1\\r\\nHost:myapplication.domain.local\\r\\nUser-agent: Mozilla/5.0 (Windows NT 6.1\\r\\n\\r\\n\" | nc 10.10.10.1 8080\nTroubleshooting steps for HTTPS monitors:\n1. Log into the device through SSH.\n2. Issue the following command: curl -k https://<member ip>:<member port><URI part of the monitor send string>\n3. Make sure you get a response and that the response matches any receive string you have configured.\nNote: You might have to add additional header specified in the send string using --header.\nExample:\ncurl -vvv --header \"Host:myapplication.domain.local\" --header \"User-agent:Mozilla/5.0 (Windows NT 6.1\" https://10.10.10.1:443/\nTroubleshooting steps for TCP monitors with send strings:\n1. Log into the device through SSH\n2. Issue the following command: echo -ne \"<monitor send string>\" | nc <member ip> <member port>\n3. Make sure you get a response and that the response matches any receive string you have configured.\nExample:\necho -ne \"info\\r\\nquit\\r\\n\" | nc 10.10.10.1 8080\nTroubleshooting steps for TCP monitors without send strings:\n1. Log into the device through SSH\n2. Issue the following command: telnet <member ip> <member port>\nExample\ntelnet 10.10.10.1 8080")() +
      new NumericThresholdOnComplexMetricWithItemsTemplateRule(context,
        ruleName = "f5_high_compression_profile",
        ruleFriendlyName = "F5 Devices: Compression profile gzip level too high",
        ruleDescription = "Setting a higher compression level makes compressed content smaller, but the cost of higher CPU usage and longer time to compress the content. The difference in terms of percentage gets lower the higher the level and setting this too high is not recommended. indeni will alert if the gzip compression level is too high.",
        metricName = "f5-high-compression-profile-high-gzip-level",
        threshold = 6.0,
        thresholdDirection = ThresholdDirection.ABOVE,
        applicableMetricTag = "name",
        alertItemsHeader = "Affected Profiles",
        alertDescription = "Setting a higher compression level makes compressed content smaller, but the cost of higher CPU usage and longer time to compress the content. The difference in terms of percentage gets lower the higher the level and setting this too high is not recommended. In general the increments after level 4 comes in the ranges of 0.15-0.25%. Of course this depends on the content being served.",
        alertItemDescriptionFormat = "The compression level used is %.0f",
        baseRemediationText = "Set the compression level to a value equals, or below 6 for the reported profiles\nhttps://support.f5.com/csp/article/K3393")() +
      new NumericThresholdOnComplexMetricWithItemsTemplateRule(context,
        ruleName = "f5_high_idle_timeout",
        ruleFriendlyName = "F5 Devices: Virtual server using a TCP profile with a high idle timeout",
        ruleDescription = "Having very long TCP idle timeouts for virtual servers could make the load balancer keep too many connections open, which in turn could potentially cause memory exhaustion. indeni will alert when the idle timeout appears too high.",
        metricName = "f5-virtualserver-tcp-profile-idle-timeout",
        threshold = 1800.0,
        thresholdDirection = ThresholdDirection.ABOVE,
        applicableMetricTag = "name",
        alertItemsHeader = "Affected Profiles",
        alertDescription = "Having very long TCP idle timeouts for virtual servers could make the load balancer keep too many connections open, which in turn could potentially cause memory exhaustion.",
        alertItemDescriptionFormat = "The idle timeout used is %.0f",
        baseRemediationText = "Investigate why the high idle timeout is being used and lower it if possible.")()


    // Cisco rules
    rules = rules +
      new StateDownTemplateRule(context,
        ruleName = "cisco_interface_err_disable",
        ruleFriendlyName = "Cisco Devices: Interface(s) in error-disable state",
        ruleDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices. indeni will alert if this happens.",
        metricName = "network-interface-err-disable",
        applicableMetricTag = "name",
        alertIfDown = false,
        alertItemsHeader = "Affected Interfaces",
        alertDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices.\nThis includes:\n* Flapping links\n* Spanning Tree BPDUs detected with BPDU Guard enabled\n* Detected a physical loop\n* Uni-directional link detected by UDLD",
        baseRemediationText = "Use the \"show interface\" command to identify the reason for the err-disable interface state.\nAfter fixing the issue perform a \"shut/no shut\" on the port to re-enable it.\nIt is possible to enable automatic periodic err-disable recovery by using the \"errdisable recovery cause\" configuration command."
      )() +
      new StateDownTemplateRule(context,
        ruleName = "cisco_invalid_sfp",
        ruleFriendlyName = "Cisco Devices: Interface(s) in error-disable state",
        ruleDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices. indeni will alert if this happens.",
        metricName = "network-interface-invalid-sfp",
        applicableMetricTag = "name",
        alertIfDown = false,
        alertItemsHeader = "Affected Interfaces",
        alertDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices.\nThis includes:\n* Flapping links\n* Spanning Tree BPDUs detected with BPDU Guard enabled\n* Detected a physical loop\n* Uni-directional link detected by UDLD",
        baseRemediationText = "Use the \"show interface\" command to identify the reason for the err-disable interface state.\nAfter fixing the issue perform a \"shut/no shut\" on the port to re-enable it.\nIt is possible to enable automatic periodic err-disable recovery by using the \"errdisable recovery cause\" configuration command."
      )()

    rules
  }
}
