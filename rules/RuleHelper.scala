package com.indeni.server.rules.library

import com.indeni.data.Snapshot.{MultiDimension, SingleDimension}
import com.indeni.data.conditions._
import com.indeni.metrics.GlobalTags
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{CompareCondition, GreaterThanOrEqual, LesserThan}
import com.indeni.ruleengine.expressions.core.ConstantExpression
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.{NoneableExpression, SwitchCaseExpression}
import com.indeni.ruleengine.expressions.{EvaluationException, Expression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.time.TimeSpan
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.GenTraversable

trait RuleHelper { self: Rule =>

  protected def getParameter[A <% Ordered[A]](context: RuleContext, parameter: ParameterDefinition): NoneableExpression[A] = {
    parameter.getUiType() match  {
      case ParameterDefinition.UIType.DOUBLE => getParameterDouble(context, parameter).asInstanceOf[NoneableExpression[A]]
      case ParameterDefinition.UIType.INTEGER => getParameterInt(context, parameter).asInstanceOf[NoneableExpression[A]]
      case ParameterDefinition.UIType.TIMESPAN => getParameterTimeSpanForTimeSeries(context, parameter).asInstanceOf[NoneableExpression[A]]
      case _ => NoneableExpression[A](ConstantExpression[A](null.asInstanceOf[A]))
    }
  }

  protected def getParameterDouble(context: RuleContext, parameter: ParameterDefinition): NoneableExpression[Double] = {
    DynamicParameterExpression
      .withConstantDefault(context.parametersDao,
                           metadata.name,
                           parameter.getName,
                           ConfigurationSetId,
                           parameter.getDefaultValue.asDouble.doubleValue())
      .noneable
  }

  protected def getParameterInt(context: RuleContext, parameter: ParameterDefinition): NoneableExpression[Int] = {
    DynamicParameterExpression
      .withConstantDefault(context.parametersDao,
                           metadata.name,
                           parameter.getName,
                           ConfigurationSetId,
                           parameter.getDefaultValue.asInteger().intValue())
      .noneable
  }

  /**
    * Timespan is retrieved as seconds, because time-series for timespan/duration are Doubles representing seconds.
    *
    * @param context
    * @param parameter
    * @return
    */
  protected def getParameterTimeSpanForTimeSeries(context: RuleContext,
                                                  parameter: ParameterDefinition): NoneableExpression[Double] = {
    DynamicParameterExpression
      .withConstantDefault(context.parametersDao,
                           metadata.name,
                           parameter.getName,
                           ConfigurationSetId,
                           parameter.getDefaultValue.asTimeSpan().getTotalSeconds.toDouble)
      .noneable
  }

  /**
    * When timestamp is used for rule configuration, we need an actual TimeSpan object and not the Double value returned by
    * getParameterTimeSpanForTimeSeries.
    *
    * @param context
    * @param parameter
    * @return
    */
  protected def getParameterTimeSpanForRule(context: RuleContext, parameter: ParameterDefinition): Expression[TimeSpan] = {
    DynamicParameterExpression
      .withConstantDefault(context.parametersDao,
                           metadata.name,
                           parameter.getName,
                           ConfigurationSetId,
                           parameter.getDefaultValue.asTimeSpan())
  }

  val scopeVarPattern = "\\$\\{scope\\(\"([^\"]+)\"\\)\\}".r("scopeField")
  protected def scopableStringFormatExpression(format: String, exprs: Expression[_]*): ScopableExpression[String] =
    new ScopableExpression[String] {
      protected def evalWithScope(time: Long, scope: Scope): String = {
        def getScopeField(scope: Scope, field: String): String = {
          if (field.contains(":")) {
            val parts = field.split("\\:")
            scope.getInvisible(parts.tail.head, Some(parts.head)).get.toString
          } else {
            scope.getVisible(field).get.toString
          }
        }

        // Evaluate all the expressions into normal objects
        val evaluatedArgs = exprs.map(x => {
          val evalValue = x.eval(time)
          if (evalValue.isInstanceOf[Option[_]]) evalValue.asInstanceOf[Option[_]].get else evalValue
        })

        // First handle the "%" of the format structure (because they may be in the scope data)
        val formattedString = format.format(evaluatedArgs: _*)

        // Do like s"$scope("blahblah")" handling. Trying to get this done via code was getting insane,
        // so we're just handling it ourselves (but don't support other parameters at this time).
        scopeVarPattern.replaceAllIn(formattedString, m => getScopeField(scope, m.group("scopeField")))
      }
      override def args: Set[Expression[_]] = exprs.toSet
    }

  val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  protected def doubleToDateExpression(dateMetric: Expression[Option[Double]]): Expression[Option[String]] =
    new Expression[Option[String]] {

      override def eval(time: Long): Option[String] = {
        dateMetric.eval(time) match {
          case Some(x: Double) => Some(dateFormat.print(new DateTime(x.toLong * 1000)))
          case _ => None
        }
      }

      /**
        * @return The argument expressions.
        */
      override def args: Set[Expression[_]] = Set(dateMetric)
    }

  def getHeadline(): Expression[String] = ConstantExpression(metadata.friendlyName.replaceFirst(".*?\\: ", ""))

  protected def withTagsCondition(firstKey: String, keys: String*): TagsStoreCondition = {
    val conditions: Seq[TagsStoreCondition] = (keys :+ firstKey).map(Equals(GlobalTags.MetricNameTag, _))

    if (conditions.length == 1) conditions.head
    else Or(conditions, nonRedundantHint = true)
  }

  protected def generateCompareCondition[A <% Ordered[A], B](direction: ThresholdDirection, first: Expression[Option[A]], second: Expression[Option[B]])(
    implicit ev: A =:= B): CompareCondition[A, B] = {
    if (direction == ThresholdDirection.ABOVE) GreaterThanOrEqual(first, second)
    else LesserThan(first, second)
  }
}

object RuleHelper {
  def createComplexStringConstantExpression(value: String): Expression[Option[SingleDimension]] =
    ConstantExpression(Some(Map("value" -> value)))
  def createEmptyComplexArrayConstantExpression(): Expression[Option[MultiDimension]] =
    ConstantExpression(Some(Seq()))

}

case class SnapshotDiffExpression(thisDev: Expression[Option[Any]], otherDev: Expression[Option[Any]])
    extends ScopableExpression[String] {

  override def evalWithScope(time: Long, scope: Scope): String = {
    val thisValue = thisDev.eval(time)
    val otherValue = otherDev.eval(time)
    // TODO What happens if at least one value is `None`? (a runtime exception will be thrown)
    (thisValue, otherValue) match {
      case (Some(thisArray: MultiDimension), Some(otherArray: MultiDimension)) =>
        val thisNotInOther = thisArray.filterNot(otherArray.toSet)
        val otherNotInThis = otherArray.filterNot(thisArray.toSet)

        val otherDevice = scope.getInvisible("cluster-compare-device-name").get + " (" + scope.getInvisible("cluster-compare-device-ip").get + ")"

        (if (thisNotInOther.length > 0)
           s"Items this device has and ${otherDevice} does not:\n" + diffListToString(thisNotInOther) + "\n\n"
         else "") +
          (if (otherNotInThis.length > 0)
             s"Items $otherDevice has and this device does not:\n" + diffListToString(otherNotInThis)
           else "")
      case (Some(thisSingle: SingleDimension), Some(otherSingle: SingleDimension)) =>
        val thisVal = thisSingle.get("value").getOrElse("")
        val otherVal = otherSingle.get("value").getOrElse("")

        val otherDevice = scope.getInvisible("cluster-compare-device-name").get + " (" + scope.getInvisible("cluster-compare-device-ip").get + ")"

        s"This device is set to: $thisVal while $otherDevice is set to $otherVal."
    }
  }

  /**
    * @return The argument expressions.
    */
  override def args: Set[Expression[_]] = Set(thisDev, otherDev)

  def diffListToString(diffList: MultiDimension): String = {
    if (diffList.length == 1 && diffList.head.contains("value")) diffList.head.getOrElse("value", "")
    else diffList.map(_.toList.sortBy(_._1).map(x => s"${x._1}: ${x._2}").mkString(", ")).mkString("\n")
  }
}

case class ConditionalRemediationSteps(vendorSwitch: SwitchCaseExpression[_, String], baseText: String)
    extends Expression[String] {

  override def eval(time: Long): String =
    vendorSwitch.eval(time) match {
      case Some(text) => baseText + " " + text
      case None => baseText
    }

  override def args: Set[Expression[_]] = Set(vendorSwitch)
}

object ConditionalRemediationSteps {

  val VENDOR_CP = "checkpoint"
  val VENDOR_CISCO = "cisco"
  val OS_NXOS = "cisco" // This is a placeholder for when we support per-os.name remediation steps
  val VENDOR_F5 = "f5"
  val VENDOR_PANOS = "paloaltonetworks"
  val VENDOR_JUNIPER = "juniper"
  val VENDOR_OTHER = "other"

  def apply(baseText: String, vendorToText: (String, String)*): ConditionalRemediationSteps = {

    val vendorExpression = new ScopableExpression[Option[String]] {

      override protected def evalWithScope(time: Long, scope: Scope): Option[String] = {

        scope.getInvisible(Vendor).map {
          case s: String => s
          case other => throw new EvaluationException(s"Vendor is not a string, but: $other")
        }
      }

      // selectVendor isn't returned on purpose
      override def args: Set[Expression[_]] = Set()

    }

    val switchCase = SwitchCaseExpression(vendorExpression, vendorToText.toMap.map {
      case (vendor, text) => Option(vendor) -> text
    }, vendorToText.toMap.get(VENDOR_OTHER))

    ConditionalRemediationSteps(switchCase, baseText)

  }
}

object ThresholdDirection extends Enumeration {
  type ThresholdDirection = Value
  val ABOVE, BELOW = Value

}


case class CollectionPrinter[A](collection: Expression[GenTraversable[A]], sep: String) extends Expression[String] {
  /**
    * @param time The time of the evaluation (in milliseconds).
    * @return The result of the evaluation.
    * @throws EvaluationException if an error occurs during the evaluation.
    */
  override def eval(time: Long): String = collection.eval(time).map(_.toString.trim).mkString(sep)

  /**
    * @return The argument expressions.
    */
  override def args: Set[Expression[_]] = Set(collection)
}


case class Collection2DPrinter[A](collection: Expression[GenTraversable[GenTraversable[A]]], sepLine: String, sepInLine: String) extends Expression[String] {
  /**
    * @param time The time of the evaluation (in milliseconds).
    * @return The result of the evaluation.
    * @throws EvaluationException if an error occurs during the evaluation.
    */
  override def eval(time: Long): String =
    collection.eval(time).map(
      line => line.mkString(sepInLine)
    ).mkString(sepLine)

  /**
    * @return The argument expressions.
    */
  override def args: Set[Expression[_]] = Set(collection)
}
