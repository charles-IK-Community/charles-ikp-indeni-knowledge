#! META
name: junos-show-interfaces-extensive
description: JUNOS get interface information (including stats)
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
network-interface-state:
    why: |
       Capture the interface state. If an interface transitions from up to down an alert would be raised.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP. Interface state transitions will generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

network-interface-admin-state:
    why: |
       Capture the interface administrative state.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP. Interface state transitions will generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

network-interface-description:
    skip-documentation: true # We don't have description data

network-interface-speed:
    why: |
       Capture the interface speed in human readable format such as 1G, 10G, etc. indeni will alert if the speed is below 1G.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-duplex:
    why: |
       Capture the interface duplex in human readable format such as full or half. In modern network environments it is very uncommon to see half-duplex interfaces, and that should be an indication for a potential exception.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP. If a duplex mismatch is detected on a port syslog messages will be generated.
    can-with-snmp: true
    can-with-syslog: true

network-interface-rx-frame:
    why: |
       Capture the interface Receive Errors (CRC) counter. If this counter increases an alarm will be raised.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP and set specific alert thresholds when the counter increases.
    can-with-snmp: true
    can-with-syslog: false

network-interface-mac:
    why: |
       Capture the interface MAC address.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP for inventory purposes.
    can-with-snmp: true
    can-with-syslog: false

network-interface-type:
    why: |
       Capture the interface type, for example "Ethernet".
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-bytes:
    why: |
       Capture the interface Received Bytes counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-dropped:
    why: |
       Capture the interface Received Errors counter. Packet loss may impact traffic performance.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-packets:
    why: |
       Capture the interface Received Packets counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false

network-interface-ipv4-address:
    why: |
       Capture the interface IPv4 address. Only relevant for layer 3 interfaces, including Vlan interfaces (SVI).
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-ipv4-subnet:
    why: |
       Capture the interface IPv4 subnet mask. Only relevant for layer 3 interfaces, including Vlan interfaces (SVI).
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-carrier:
    why: |
       Capture the interface carrier state change counter. It would increase every time the interface changes state from up to down.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false

network-interface-mtu:
    why: |
       Capture the interface MTU (Maximum Transmit Unit). A low MTU may cause issues.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP and alert if the MTU is too low.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-bytes:
    why: |
       Capture the interface Transmitted Bytes counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-errors:
    why: |
       Capture the interface Transmit Errors counter. Transmission errors indicate an issue with duplex/speed matching.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-packets:
    why: |
       Capture the interface Transmitted Packets counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-overruns:
    why: |
       Capture the interface Receive Overrun errors counter.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show interfaces extensive" command. The output includes all the interface related information and statistics.
    without-indeni: |
       There are many solutions out there for tracking traffic flow on network interfaces and alerting when critical indicators are reached.
    can-with-snmp: true
    can-with-syslog: false


#! REMOTE::SSH
show interfaces extensive | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//interface-information[1]
_metrics:
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-state" 
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Interfaces - State"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    state:
                        _text: "oper-status"
        _transform:
             _value.double: |
                 {
                     if ("${temp.state}" == "up") {
                        interface_state = "1.0"
                     } else {
                        interface_state = "0.0"
                     }
                     print interface_state
                 }
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-admin-state" 
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Interfaces - Admin State"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    state:
                        _text: "admin-status"
        _transform:
             _value.double: |
                 {
                     if ("${temp.state}" == "up") {
                         admin_state = "1.0"
                     } else {
                         admin_state = "0.0"
                     }
                     print admin_state
                 }
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-description"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces"
                    "name":
                        _text: "name"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    descriptionPath:
                        _count: description
                    content:
                        _text: description
        _transform:
            _value.complex: 
                value: |
                    {
                        if ("${temp.descriptionPath}" == "0") {  #this line seems not work for complex
                            interface_description = ""
                        }else{
                            interface_description = "${temp.content}"
                        }
                        print interface_description
                    } 
    -
        _groups:
            ${root}/physical-interface[not(contains(speed,'Unlimited') or contains(speed,'Unspecified'))]:
                _tags:
                    "im.name":
                        _constant: "network-interface-speed"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Interfaces - Speed"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    speed:
                        _text: speed
        _transform:
            _value.complex:
                value: |
                    {
                        speed = "${temp.speed}"
                        gsub(/bps/, "", speed)
                        print toupper(speed)
                    }
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-duplex"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Interfaces - Duplex"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    duplex:
                        _text: "link-mode"
        _transform:
            _value.complex:
                value: |
                    {
                        if (tolower("${temp.duplex}") ~ /full/) {
                            link_mode = "full"
                        } else {
                            link_mode = "half"
                        }
                        print link_mode
                    }
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-frame"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - RX Frame Errors"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "input-error-list/input-errors"
    -
        _groups:
            ${root}/physical-interface[not(contains(current-physical-address,'Unspecified'))]:
                _tags:
                    "im.name":
                        _constant: "network-interface-mac"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - MAC Address"
                    "im.identity-tags":
                        _constant: "name"
                _value.complex:
                    value:
                        _text: "current-physical-address"
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-type"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - Type"
                    "im.identity-tags":
                        _constant: "name"
                _value.complex:
                    value:
                        _text: "link-level-type"
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-bytes"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - RX Bytes"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "traffic-statistics/input-bytes"
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-dropped"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - RX Drops"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "input-error-list/input-drops"
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-packets"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - RX Packets"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "traffic-statistics/input-packets"
    -
        _groups:
            ${root}/physical-interface[not(contains(./logical-interface/address-family/interface-address,'Unspecified'))]:
                _tags:
                    "im.name":
                        _constant: "network-interface-ipv4-address"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - IPv4 Address"
                    "im.identity-tags":
                        _constant: "name"
                _value.complex:
                    value:
                        _text: "logical-interface/address-family/interface-address/ifa-local"
    -
        _groups:
            ${root}/physical-interface[not(contains(./logical-interface/address-family/interface-address,'Unspecified'))]:
                _tags:
                    "im.name":
                        _constant: "network-interface-ipv4-subnet"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - Bitmask"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    destination_net:
                        _text: "logical-interface/address-family/interface-address/ifa-destination"
        _transform:
            _value.complex:
                value: |
                    {
                        bitmask = "${temp.destination_net}"
                        sub(/^.*\//, "", bitmask)
                        print bitmask
                    }
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-carrier"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - TX Carrier Transitions"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "output-error-list/carrier-transitions"
    -
        _groups:
            ${root}/physical-interface[not(contains(mtu,'Unlimited'))]:
                _tags:
                    "im.name":
                        _constant: "network-interface-mtu"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - MTU"
                    "im.identity-tags":
                        _constant: "name"
                _value.complex:
                    value:
                        _text: "mtu"
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-bytes"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - TX Bytes"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "traffic-statistics/output-bytes"
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-errors"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - TX Errors"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "output-error-list/output-errors"
    -
        _groups:
            ${root}/physical-interface:
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-packets"
                    "dsType":
                        _constant: "counter"
                    "name":
                        _text: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Interfaces - TX Packets"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                _value.double:
                    _text: "traffic-statistics/output-packets"
