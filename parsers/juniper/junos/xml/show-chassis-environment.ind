#! META
name: junos-show-chassis-environment
description: JUNOS show chassis environment
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
hardware-element-status:
    why: |
        The hardware elements are critical to keep the device working properly.
    how: |
        The script runs "show chassis environment" command via ssh connection to the device and retrieves the status for all the on-line hardware elements. Any failure will raise an alert.
    without-indeni: |
        An administrator could login to the device to manually run the same command "show chassis environment" to get the same information.
    can-with-snmp: true
    can-wth-syslog: true
    vendor-provided-management: |
        The stauts for the hardware elements can be retrieved via the command line.

#! REMOTE::SSH
show chassis environment | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//environment-information[1]
_metrics:
    -
        _groups:
            ${root}/environment-item[not(contains(status,'Absent'))]:
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Environment Elements"
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    name:
                        _text: name
                    class:
                        _text: class
                    status:
                        _text: status
        _transform:
            _tags:
                "name": |
                    {
                        print "${temp.class}: ${temp.name}"
                    }
            _value.double: |
                {
                     if ("${temp.status}" != "OK") {
                         print 0
                     } else {
                         print 1
                     }
                }
