#! META
name: junos-show-ospf-neighbor
description: JUNOS collect ospf neighbor information
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
ospf-state:
    why: |
        Due to the dynamic nature of OSPF, it should be closely monitored to verify that it is working correctly. Since routing is a vital part of any network, a failure or issues in dynamic routing can cause large disruptions.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show ospf neighbor" command. The output includes the status of active OSPF neighbors (but not those that are missing).
    without-indeni: |
        An administrator could login and manually run the command. SNMP traps and syslogs are also available to be aware of an OSPF issue.
    can-with-snmp: true
    can-with-syslog: true
    vendor-provided-management: |
        Listing OSPF neighbors is only available from the command line.

#! REMOTE::SSH
show ospf neighbor

#! PARSER::AWK

# Address          Interface              State     ID               Pri  Dead
# 10.222.2.2       ge-0/0/11.0            Full      192.168.36.1     128    36

# Make sure line starts with some ID
/^[0-9\.]{4}/ {
	# Verify we have the number of columns we thought we would
	if (NF == 6) {
		name_tag["name"] = $4 " priority: " $5 " address: " $1
		state_desc = $3

		up = 0
		if (match(toupper(state_desc), "FULL|2WAY")) {
		    up = 1
		}
		writeDoubleMetricWithLiveConfig("ospf-state", name_tag, "gauge", "60", up, "OSPF Neighbors", "state", "name")
	}
}
