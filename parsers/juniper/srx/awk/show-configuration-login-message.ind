#! META
name: junos-show-configuration-login-message
description: JUNOS show the login message in the configuration
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
login-banner:
    why: |
        The login banner is used to warn any illegal login attempt.
    how: |
        This script retrieves the login banner by running the command "show configuration system login message" command via SSH connection the the device.
    without-indeni: |
        An administrator can log in the device to run the same command to retrieve this information.
    can-with-snmp: false 
    can-with-syslog: false

#! REMOTE::SSH
show configuration system login message 

#! PARSER::AWK
/^(message ")/ {
    message = trim($0)
    sub(/^(message )/, "", message)
    sub(/;$/, "", message)
}

END {
  gsub(/["]/, "", message)
  gsub(/[\\]/, "\\\\", message)
  writeComplexMetricString("login-banner", null, message)
}
