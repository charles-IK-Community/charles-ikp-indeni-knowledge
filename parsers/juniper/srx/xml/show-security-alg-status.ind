#! META
name: junos-show-security-alg-status
description: JUNOS show the ALG features that are enabled
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
features-enabled:
    why: |
        Collect information about features enabled on the device. This can help for inventory management.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show security alg status" command. The output includes the enabled ALG features.
    without-indeni: |
        It is impossible to retreive enabled features information through SNMP and no logs are generated for this information.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show security alg status | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply/alg-status
_metrics:
    -
        _groups:
            ${root}/*[. = 'enabled']:
                _tags:
                    "im.name":
                        _constant: "features-enabled"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "ALG Features Enabled"
                _temp:
                    node_name:
                        _name: .
        _transform:
            _value.complex:
                name: | 
                    {
                        node = "${temp.node_name}"
                        gsub(/(alg-|-status)/, "", node)
                        print node
                    }
        _value: complex-array
