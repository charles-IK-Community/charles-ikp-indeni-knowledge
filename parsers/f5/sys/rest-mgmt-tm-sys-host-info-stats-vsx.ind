#! META
name: f5-rest-mgmt-tm-sys-host-info-stats-vsx
description: Get OS memory statistics
type: monitoring
monitoring_interval: 1 minute
includes_resource_data: true
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"
    vsx: "true"

#! COMMENTS
memory-free-kbytes:
    skip-documentation: true
memory-total-kbytes:
    skip-documentation: true
memory-usage:
    why: |
        The various memory components of an F5 unit are important to track to ensure a smooth operation. This includes the management plane's memory element (also called the linux host).
    how: |
        This script uses the F5 REST API to retrieve the current status of multiple different memory elements.
    without-indeni: |
        These memory metrics are available through SNMP, TMSH and the web user interface under Statistics/Performance.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/host-info/stats?options=kil&$select=hostId,memoryUsed,memoryTotal
protocol: HTTPS

#! PARSER::JSON

_metrics:
    -   #Collecting total memory metric
        _groups:
            "$.entries.*.nestedStats.entries[?(@.memoryTotal.value > 0)]":
                _tags:
                    "im.name":
                        _constant: "memory-total-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                    "im.identity-tags":
                        _constant: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Memory - Total"
                _temp:
                    "hostId":
                        _value: "hostId.description"
                _value.double:
                    _value: "memoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "linux-host-" "${temp.hostId}"
                    }
    -   #Collecting used memory metric
        _groups:
            "$.entries.*.nestedStats.entries[?(@.memoryTotal.value > 0)]":
                _tags:
                    "im.name":
                        _constant: "memory-free-kbytes"
                    "im.dstype.displaytype":
                        _constant: "kilobytes"
                    "im.identity-tags":
                        _constant: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Memory - Free"
                _temp:
                    "hostId":
                        _value: "hostId.description"
                    "memoryUsed":
                        _value: "memoryUsed.value"
                    "memoryTotal":
                        _value: "memoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "linux-host-" "${temp.hostId}"
                    }
            _value.double: |
                {
                    print "${temp.memoryTotal}" - "${temp.memoryUsed}"
                }
    -   #Collecting used memory metric
        _groups:
            "$.entries.*.nestedStats.entries[?(@.memoryTotal.value > 0)]":
                _tags:
                    "im.name":
                        _constant: "memory-usage"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "im.identity-tags":
                        _constant: "name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Memory - Usage"
                _temp:
                    "hostId":
                        _value: "hostId.description"
                    "memoryUsed":
                        _value: "memoryUsed.value"
                    "memoryTotal":
                        _value: "memoryTotal.value"
        _transform:
            _tags:
                "name": |
                    {
                        print "vCMP host - linux host usage - host-id ${temp.hostId}"
                    }
            _value.double: |
                {
                    memoryTotal = "${temp.memoryTotal}"
                    memoryUsed = "${temp.memoryUsed}"
                    
                    print (memoryUsed/memoryTotal)*100
                }
