#! META
name: f5-rest-mgmt-tm-sys-db-rstcause-log
description: Determine if the debug value tm.rstcause.log is set
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
debug-status:
    why: |
        Setting the tm.rstcause.log flag would make the F5 device generate TCP RST log entries in the /var/log/ltm log file. However, if there is a very high amount being sent this has an impact on performance and could also fill up available disk space. Thus, leaving the setting on by mistake could result in problems later on.
    how: |
        This alert logs into the F5 unit via iControl REST and retrieves the status of the tm.rstcause.log flag.
    without-indeni: |
        An administrator would be able to see if the flag is on by logging into the device through SSH, entering TMSH and issuing the command "list sys db tm.rstcause.log".
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/db/tm.rstcause.log?$select=value
protocol: HTTPS

#! PARSER::JSON

_metrics:
    -
        _tags:
            "im.name":
                _constant: "debug-status"
            "name":
                _constant: "tm.rstcause.log"
            "im.dstype.displaytype":
                _constant: "state"
        _temp:
            "rstValue":
                _value: "$.value"
        _transform:
            _value.double: |
                {
                    if("${temp.rstValue}" == "disable"){
                        print "0"
                    } else {
                        print "1"
                    }
                }
