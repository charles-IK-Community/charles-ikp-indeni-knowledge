#! META
name: f5-ls-service-mcpd-forcereload
description: Verify that the forcereload flag is not set
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
debug-status:
    why: |
        If the file /service/mcpd/forceload exists and an F5 reboots, an extra amount of time will be taken for the device to fully reload. This file is manually created and intentional if there is a need for the mcpd process to force a reload of the BIG-IP configuration (K13030). If this file is not removed afterwards and the device reboots, this would result in more logged downtime.
    how: |
        This alert logs into the F5 load balancer and verifies if the forcereload flag is set.
    without-indeni: |
        Login to the device with SSH and run "ls -l /service/mcpd/forceload" and verify that the file is not present.
    can-with-snmp: false
    can-with-syslog: false
    
#! REMOTE::SSH
nice -n 15  /bin/ls /service/mcpd/forceload

#! PARSER::AWK

#Source article about the mcdp reload flag
#https://support.f5.com/csp/article/K13030

#/service/mcpd/forceload
{

	debugTags["name"] = "mcpd-force-reload"
    
    #/service/mcpd/forceload
    if(match($0, /^\/service\/mcpd\/forceload$/)){
        state = 1
    } else {
        state = 0
    }
    
    writeDoubleMetric("debug-status", debugTags, "gauge", 1800, state)
    
}
