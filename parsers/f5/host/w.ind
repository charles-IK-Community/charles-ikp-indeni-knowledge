#! META
name: f5-w
description: Shows idle users logged in
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
logged-in-users:
    why: |
        An idle user logged into a device is not a good security practice. This is especially true if the user logged into the device physically (from a console), as anyone passing by in the data center could access the device. For example, consider this document: http://www.security-solutions.co.za/cisco-asa-firewall-hardening-cisco-asa-best-practices.html and the requirement to _"Enforce an idle timeout to detect and close inactive sessions"_.
    how: |
        This alert uses the F5 iControl REST API to retrieve the list of logged in admins, their source and their idle time.
    without-indeni: |
        An administrator could write a script to leverage the F5 iControl REST API or could periodically log into the device throug SSH and execute the command "w".
    can-with-snmp: false
    can-with-syslog: false

    
#! REMOTE::SSH
w

#! PARSER::AWK


# 18:37:53 up 18 days,  8:54,  2 users,  load average: 0.35, 0.28, 0.20
#USER     TTY      FROM              LOGIN@   IDLE   JCPU   PCPU WHAT
#user1    pts/0    10.10.10.11       Thu17    1.00s  1.96s  0.06s w
#user2    pts/1    10.10.10.12       Wed09    3days  0.26s  0.26s -bash

/^(?!\s|USER\s+TTY\s+FROM\s+LOGIN@\s+IDLE\s+JCPU\s+PCPU\s+WHAT)/ {

	username=$1
	from=$3
	idle=$5
	tty=$2	
	iuser++
	users[iuser, "username"]=username
	users[iuser, "from"]=from
	if ( from == "-" ) {
		users[iuser, "from"]="console"
	}
	# Here we count number of seconds of idle time
	# w uses several time formats
	# DDdays, HH:MMm, MM:SS, SS.CCs
	
	# 2days
	if ( idle ~ /days/ ) {
		
		split (idle,idleArr,"days")
		secondsIdle=idleArr[1] * 86400
		#secondsIdle = 501	

	# 7:13m
	} else if ( idle ~ /m/ ) {
		split (idle,idleArr,"m")
		split (idleArr[1],idleArr,":")
		secondsIdle = idleArr[1] * 3600 + idleArr[2] * 60
		#secondsIdle = 502

	# 11.00s
	} else if ( idle ~ /s/ ) {
		split (idle,idleArr,"\\.")
		secondsIdle = idleArr[1]
		#secondsIdle = 503		

	} else if ( idle ~ /:/ ) {
		split (idle,idleArr,":")
		secondsIdle = idleArr[1] * 60 + idleArr[2]
		#secondsIdle = 504
	}

	users[iuser, "idle"]=secondsIdle
}

#{
#  "username" : "user1",
#  "idle" : "1",
#  "from" : "10.10.10.11"
#}, {
#  "idle" : "259200",
#  "from" : "10.10.10.12",
#  "username" : "user2"
#} ]

END {
   writeComplexMetricObjectArray("logged-in-users", null, users)
}

