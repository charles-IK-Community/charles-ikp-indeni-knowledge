 #! META
name: f5-rest-net-interface-route
description: Extract configured static routes, and count them to match against routes-limit
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
static-routing-table:
    why: |
        It is important that the routing is configured the same for all cluster members of the same cluster. Otherwise there can be downtime in the event of a failover.
    how: |
        This alert uses the F5 iControl REST API to extract the configured routes for the device.
    without-indeni: |
        An administrator could log into the device, enter TMSH and run the command "show net route". The routing information is also available via the web interface in "Network" -> "Routes".
    can-with-snmp: true
    can-with-syslog: false
routes-usage:
    why: |
        If maximum route entries route has been configured and the limit is reached no more route entries can be added to the system. This alert tracks the number of added routes and warns if the limit is about to, or has been reached.
    how: |
        This alert logs into the F5 unit via iControl REST and retrieves the configured routes.
    without-indeni: |
        An administrator could log into the device, enter TMSH and run the command "show net route" and count the routes manually. The routing information is also available via the web interface in "Network" -> "Routes".
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/net/route?$select=network,gw
protocol: HTTPS

#! PARSER::JSON

_metrics:    
    -
        _groups:
            "$.items[0:]":
                _tags:
                    "im.name":
                        _constant: "static-routing-table"
                _value.complex:
                    "next-hop":
                        _value: gw
                _temp:
                    "network":
                        _value: network
        _transform:
            _value.complex:
                "network": |
                    {
                        if("${temp.network}" == "default"){
                            print "0.0.0.0"
                        } else {
                            split("${temp.network}", netMask, /\//)
                            print(netMask[1])
                        }
                    }
                "mask": |
                    {
                        #If this is the default gateway the value is "default", otherwise network/mask
                        if("${temp.network}" == "default"){
                            print "0"
                        } else {
                            split("${temp.network}", netMask, /\//)
                            print(netMask[2])
                        }
                    }
        _value: complex-array
    -
        _tags:
            "im.name":
                _constant: "routes-usage"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Routes usage"
            "im.display-type":
                _constant: "number"
        _value.double:
            _count: $.items
