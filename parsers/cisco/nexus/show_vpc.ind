#! META
name: nexus-vpc
description: Nexus VPC information
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    vpc: true

#! COMMENTS
nexus-vpc-peer-status:
    why: |
       Make sure the vPC peer connection is active.
	   vPC domains always include 2 Cisco Nexus switches acting as a single logical forwarding switch. Attached devices use peer links formed by a number of links to connect to both switches as if they were a single switch (i.e. the remote devices are configured as if it was a regular single port-channel).
	   For more infomation about Cisco Nexus vPC: http://www.cisco.com/c/en/us/products/collateral/switches/nexus-5000-series-switches/design_guide_c07-625857.html
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc" command. The output includes the device's vPC state as well as information about the vPC different vPC consistency metrics.
    without-indeni: |
       The user would have to login to the device and use the "show vpc" command to identify the inconsistency state. State transitions will generate a syslog message.
    can-with-snmp: false
    can-with-syslog: true 

nexus-vpc-peer-status-description:
    why: |
       Provide additional details on the issue if the vpc peer is down.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc" command. The output includes the device's vPC state as well as information about the vPC different vPC consistency metrics.
    without-indeni: |
       The user would have to login to the device and use the "show vpc" command to identify the inconsistency state. 
    can-with-snmp: false
    can-with-syslog: false

nexus-vpc-peer-consistency:
    why: |
       Make sure the vPC peer connection is consistent.
	   vPC domains always include 2 Cisco Nexus switches acting as a single logical forwarding switch. Attached devices use peer links formed by a number of links to connect to both switches as if they were a single switch (i.e. the remote devices are configured as if it was a regular single port-channel).
	   For more infomation about Cisco Nexus vPC: http://www.cisco.com/c/en/us/products/collateral/switches/nexus-5000-series-switches/design_guide_c07-625857.html
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc" command. The output includes the device's vPC state as well as information about the vPC different vPC consistency metrics.
       If there is any inconsistency it would reported. Inconsistencies include MTU, QOS, Spanning Tree and VLAN configuration issues. To get additional details about the inconsistency it is possible to use the "show vpc consistency-parameters global" command.
       For more information about vPC inconsistencies refer to
        http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/operations/n5k_vpc_ops.html
        http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/troubleshooting/guide/N5K_Troubleshooting_Guide/n5K_ts_vpc.html#70794
    without-indeni: |
       The user would have to login to the device and use the "show vpc" command to identify the inconsistency state. State transitions will generate a syslog message.
    can-with-snmp: false
    can-with-syslog: true 

nexus-vpc-peer-consistency-description:
    why: |
       Provide additional details on the issue if a vpc inconsistency has been identified.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc" command. The output includes the device's vPC state as well as information about the vPC different vPC consistency metrics.
    without-indeni: |
       The user would have to login to the device and use the "show vpc" command to identify the inconsistency state. 
    can-with-snmp: false
    can-with-syslog: false

nexus-vpc-type-2-consistency:
    why: |
       Make sure the vPC peer connection is consistent. Type 2 inconsistencies are lower priority issues which would allow the vPC to operate, but still present an issue that should be identified and fixed.
	   vPC domains always include 2 Cisco Nexus switches acting as a single logical forwarding switch. Attached devices use peer links formed by a number of links to connect to both switches as if they were a single switch (i.e. the remote devices are configured as if it was a regular single port-channel).
	   For more infomation about Cisco Nexus vPC: http://www.cisco.com/c/en/us/products/collateral/switches/nexus-5000-series-switches/design_guide_c07-625857.html
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc" command. The output includes the device's vPC state as well as information about the vPC different vPC consistency metrics.
       If there is any inconsistency it would reported. Inconsistencies include MTU, QOS, Spanning Tree and VLAN configuration issues. To get additional details about the inconsistency it is possible to use the "show vpc consistency-parameters global" command.
       For more information about vPC inconsistencies refer to
        http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/operations/n5k_vpc_ops.html
        http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/troubleshooting/guide/N5K_Troubleshooting_Guide/n5K_ts_vpc.html#70794
    without-indeni: |
       The user would have to login to the device and use the "show vpc" command to identify the inconsistency state. State transitions will generate a syslog message.
    can-with-snmp: false
    can-with-syslog: true 

nexus-vpc-peer-consistency-description:
    why: |
       Provide additional details on the issue if a vpc inconsistency has been identified.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc" command. The output includes the device's vPC state as well as information about the vPC different vPC consistency metrics.
    without-indeni: |
       The user would have to login to the device and use the "show vpc" command to identify the inconsistency state. 
    can-with-snmp: false
    can-with-syslog: false

nexus-vpc-per-vlan-peer-consistency:
    why: |
       Make sure the vPC peer connection is consistent. Per-VLAN peer consistency checks the VLANs enabled on both vpc peers for each vPC enabled interface.
	   vPC domains always include 2 Cisco Nexus switches acting as a single logical forwarding switch. Attached devices use peer links formed by a number of links to connect to both switches as if they were a single switch (i.e. the remote devices are configured as if it was a regular single port-channel).
	   For more infomation about Cisco Nexus vPC: http://www.cisco.com/c/en/us/products/collateral/switches/nexus-5000-series-switches/design_guide_c07-625857.html
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the vPC information using the output of the "show vpc" command. The output includes the device's vPC state as well as information about the vPC different vPC consistency metrics.
       If there is any inconsistency it would reported. Inconsistencies include MTU, QOS, Spanning Tree and VLAN configuration issues. To get additional details about the inconsistency it is possible to use the "show vpc consistency-parameters global" command.
       For more information about vPC inconsistencies refer to
        http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/operations/n5k_vpc_ops.html
        http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/troubleshooting/guide/N5K_Troubleshooting_Guide/n5K_ts_vpc.html#70794
    without-indeni: |
       The user would have to login to the device and use the "show vpc" command to identify the inconsistency state. State transitions will generate a syslog message.
    can-with-snmp: false
    can-with-syslog: true 
#! REMOTE::SSH
show vpc | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__
_metrics:
    -
        _temp:
            status:
                _text: "${root}/vpc-peer-status"
        _tags:
            "im.name":
                _constant: "nexus-vpc-peer-status"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Peer Status"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                    if ("${temp.status}" == "peer-ok") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
    -
        _value.complex:
            value: 
                _text: "${root}/vpc-peer-status-reason"
        _tags:
            "im.name":
                _constant: "nexus-vpc-peer-status-description"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Peer Status Description"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
    -
        _temp:
            status:
                _text: "${root}/vpc-peer-consistency"
        _tags:
            "im.name":
                _constant: "nexus-vpc-peer-consistency"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Peer Consistency"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                    if ("${temp.status}" == "consistent") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
    -
        _value.complex:
            value: 
                _text: "${root}/vpc-peer-consistency-status"
        _tags:
            "im.name":
                _constant: "nexus-vpc-peer-consistency-description"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Peer Consistency Description"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
    -
        _temp:
            status:
                _text: "${root}/vpc-type-2-consistency"
        _tags:
            "im.name":
                _constant: "nexus-vpc-type-2-consistency"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Type 2 Consistency"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                    if ("${temp.status}" == "consistent") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
    -
        _value.complex:
            value: 
                _text: "${root}/vpc-type-2-consistency-status"
        _tags:
            "im.name":
                _constant: "nexus-vpc-type-2-consistency-description"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Type 2 Consistency Description"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
    -
        _temp:
            status:
                _text: "${root}/vpc-per-vlan-peer-consistency"
        _tags:
            "im.name":
                _constant: "nexus-vpc-per-vlan-peer-consistency"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "VPC - Per VLAN Consistency"
            "im.dstype.displayType":
                _constant: "state"            
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                    if ("${temp.status}" == "consistent") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
