#! META
name: nexus-ntp-peer-status
description: fetch ntp server status
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
ntp-server-state:
    why: |
       Monitor NTP server state. NTP servers are used to sync the time across all hosts and network devices. This is critical for things such as event correlation and logging. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show ntp peer-status" command. The output includes the state information for all configured NTP servers.
    without-indeni: |
       It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show ntp peer-status

#! PARSER::AWK
#foghorn# show ntp peer-status
#Total peers : 2
#* - selected for sync, + -  peer mode(active),
#- - peer mode(passive), = - polled in client mode
#    remote               local                 st   poll   reach delay   vrf
#-------------------------------------------------------------------------------
#=13.80.12.54            172.16.20.20            2   64     377   0.14447 management
#*216.239.35.4           172.16.20.20            2   64     377   0.03862 management

/.*[1-9][0-9]*\.[1-9]/ {
    remote = $1
    reach = $5
    if (reach == 0) {
        state = "0.0"
    } else {
        state = "1.0"
    }

    gsub(/[\*\+\-\=]/, "", remote)
    trim(remote)

    ntptags["name"] = remote
    writeDoubleMetric("ntp-server-state", ntptags, "gauge", 300, state)
}
