#! META
name: cpembedded-console_idle_time
description: Shows idle users logged in
type: monitoring
monitoring_interval: 5 minutes
requires:
    linux-based: "true"
    os.name: "gaia-embedded"

#! COMMENTS
logged-in-users:
    skip-documentation: true

#! REMOTE::SSH
nice -n 15 who

#! PARSER::AWK

############
# Script explanation: Normally we would use other commands, but busybox only has "who" command, so we are using it instead.
###########

/pts|tty/ {
	username=$1
	terminal=$2
	idle=$3
	from=$7
	iuser++
	
	users[iuser, "username"]=username
	users[iuser, "from"]=from
	
	users[iuser, "terminal"]=terminal
	
	# Here we count number of seconds of idle time
	# w uses several time formats
	# DDdays, HH:MMm, MM:SS, SS.CCs
	
	
	# 2days
	if ( idle ~ /days/ ) {
		
		split (idle,idleArrDays,"days")
		secondsIdle=idleArrDays[1] * 86400	

	# 7:13m
	} else if ( idle ~ /m/ ) {
		split (idle,idleArrMin,"m")
		split (idleArrMin[1],idleArrSplit,":")
		secondsIdle = idleArrSplit[1] * 3600 + idleArrSplit[2] * 60

	# 11.00s
	} else if ( idle ~ /s/ ) {
		split (idle,idleArrSec,"\\.")
		secondsIdle = idleArrSec[1]	

	} else if ( idle ~ /:/ ) {
		split (idle,idleArr,":")
		secondsIdle = idleArr[1] * 60 + idleArr[2]
	}

	users[iuser, "idle"]=secondsIdle
}



END {
   writeComplexMetricObjectArray("logged-in-users", null, users)
}
