#! META
name: cplic-print
description: get contract data via cplic
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    mds:
        neq: true

#! COMMENTS
contract-expiration:
    why: |
        A contract is used to validate a subscription to the Anti-Virus or IPS services (for example). If the contract expires no new updates can be downloaded, and in some cases the signatures on the system will even be reset to a much earlier date.
    how: |
        By using the Check Point built-in command "cplic", the expiration date for the contracts are retrieved.
    without-indeni: |
        An administrator could login and manually run the command or check in SmartUpdate.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The best way is to manage this with Check Point SmartUpdate, or do it via the command line interface.

license-expiration:
    why: |
        A license is used to validate the right to use the product. If the license expires service interruption may occur and possibly being unable to manage the device.
    how: |
        By using the Check Point built-in command "cplic", the expiration date for the licenses are retrieved.
    without-indeni: |
        An administrator could login and manually run the command or check in SmartUpdate.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The best way is to manage this with Check Point SmartUpdate, or do it via the command line interface.

#! REMOTE::SSH
nice -n 15 cplic print

#! PARSER::AWK

# 1  | 38IH618   | 30Apr2017  | CPSB-ABOT-M-1Y
/^\d{1,2}  \| / {
    ddmmmyyyy = $5
    dd=substr(ddmmmyyyy, 1, length(ddmmmyyyy)-7) # need to handle 1 or 2 digits for day
	mmm=substr(ddmmmyyyy, length(dd)+1, 3)
    mm=parseMonthThreeLetter(mmm)
	yyyy=substr(ddmmmyyyy, length(ddmmmyyyy)-3, length(ddmmmyyyy))
	
    contracttags["name"] = $3 " - " $NF
    contractexpiration=date(yyyy, mm, dd)
    writeDoubleMetricWithLiveConfig("contract-expiration", contracttags, "gauge", "3600", contractexpiration, "Support Contract Expiration", "date", "")
}


# 192.168.250.5    never       CPSB-ADNC-M CPSB-EVCR-10 CK-00-1C-7F-3E-CB-38
# 10.10.6.10       30Sep2017   CPSG-C-8-U CPSB-FW CPSB-VPN CPSB-ADN CPSB-ACCL CPSB-IPSA CPSB-DLP CPSB-SSLVPN-50 CPSB-IA CPSG-VSX-25S CPSB-SWB CPSB-IPS CPSB-ASPM CPSB-URLF CPSB-AV CPSB-APCL CPSB-ABOT-L CK-043C32F48B44
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
	# extract the features, split on at least 2 spaces or more
	split($0,splitArr,/\s{2,}+/)
	gsub(/\sCK.+/, "", splitArr[3])
	
	t["features"] = splitArr[3]
	t["name"] = $NF
	
	if ($2 == "never") {
		# Since it never expires we set a expiry date very far in the future
		writeDoubleMetric("license-expiration", t, "gauge", 3600, date(2099,12,31))
	} else {
		
		ddmmmyyyy = $2
		 dd=substr(ddmmmyyyy, 1, length(ddmmmyyyy)-7) # need to handle 1 or 2 digits for day
		mmm=substr(ddmmmyyyy, length(dd)+1, 3)
		mm=parseMonthThreeLetter(mmm)
		yyyy=substr(ddmmmyyyy, length(ddmmmyyyy)-3, length(ddmmmyyyy))
	
		writeDoubleMetricWithLiveConfig("license-expiration", t, "gauge", 3600, date(yyyy, mm, dd), "License Expiration", "date", "name")
	}
}
