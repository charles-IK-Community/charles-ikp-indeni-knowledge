#! META
name: cpvsx-cphaprob_state_monitoring
description: run "cphaprob state" for cluster status monitoring on VSX
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    high-availability: "true"
    vsx: "true"
    clusterxl: "true"
    role-firewall: true

#! COMMENTS
cluster-mode:
    skip-documentation: true

cluster-member-active:
    skip-documentation: true

cluster-member-states:
    skip-documentation: true

cluster-state:
    skip-documentation: true

#! REMOTE::SSH
nice -n 15 vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && vsx stat $id && nice -n 15 cphaprob state ; done

#! PARSER::AWK

############
# Why: It is importat to know the status of the cluster.
# How: Use "cphaprob state"

# A remote peer can have the following states
# Down
# Ready
# Active
# Active Attention
# ClusterXL Inactive or Machine is Down

# A local peer can have the following states
# Down
# Ready
# Active
# Active Attention
# HA module not started.

### The cluster as a whole can have several states regarding different aspects of the cluster
## Redundancy
# The non-active member could be:
# Standby - This is the best state for a non-active member
# Ready - This member will take over if the Active one goes down, but it is not syncing state tables, so the failover will cut all connections
# Down - This member will not take over in case the active member goes offline

## Health
# Active - This member is healthy and forwarding traffic
# Active Attention - This member is not healthy and would like to fail over, but there is no other node that can take over.
###########

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

function dumpClusterActiveData() {
    addVsTags(t)
	writeDoubleMetricWithLiveConfig("cluster-member-active", t, "gauge", "60", localState, "Cluster Member State (this)", "state", "cluster-member-state-description")
}

function dumpClusterStateData() {
    addVsTags(a)
	    # If we found at least one member forwarding traffic, and no cluster members in an unhealthy state then its ok
    clusterstate=0
	if (foundActive=="true" && foundUnhealthyState=="false") {
        clusterstate=1
    }
   	
	# Write status of all members
	if (iallmembers != "") {
		writeComplexMetricObjectArray("cluster-member-states", t, allmembers)
   	}
    
	if ( vstype != "Switch") {
		writeDoubleMetricWithLiveConfig("cluster-state", t, "gauge", "60", clusterstate, "Cluster State", "state")
	}
}

function dumpClusterModeData() {
	addVsTags(t)
	writeComplexMetricString("cluster-mode", t, clustermode)
}


BEGIN {
    foundActive="false"
    foundUnhealthyState="false"
	vsid=""
	vsname=""
    t["name"] = "ClusterXL"
}

# VSID:            0
/VSID:/ {
    # Dump data of previous VS if needed
    if (vsid != "") {
		dumpClusterStateData()
    }
	vsid = trim($NF)
	# Delete the old
	delete t
	delete a
	state=""
	delete allmembers
	foundActive="false"
	status=""
	statusFlag=""
	foundUnhealthy="false"
}

# Name:            lab-CP-VSX1-R7730
/Name:/ {
	vsname = trim($NF)
}

# Type:            VSX Gateway
# Type:            Virtual Switch
/Type:/ {
	vstype = trim($NF)
}

# Determine cluster mode
# Cluster Mode:   VSX High Availability (Active Up) with IGMP Membership
/Cluster Mode/ {
    clustermode=trim(substr($0, index($0, ":") + 1))
	dumpClusterModeData()
}

# Has the cluster at least one healthy member forwarding traffic?
# Match any cluster member being Active (but not Active Attention)
/^\d+\s.*Active$/ {
	foundActive="true"
}

# The state of the local cluster member
# 1 (local)  10.11.2.21      0%              Standby
/[0-9]+ \(local\)/ {
	# Catch both "Active" and "Active Attention"
	if ($0 ~ /Active/) {
		localState=1
	} else {
		localState=0
	}
	dumpClusterActiveData()
}


## Match any unhealthy statuses in the cluster

# Match any cluster member being down
# 1 (local)  10.11.2.21      0%              Down
/^\d+\s.*Down/ {
    foundUnhealthyState="true"
}

# Match any cluster member being in Ready state
# 1 (local)  10.11.2.21      0%              Ready
/^\d+\s.*Ready/ {
    foundUnhealthyState="true"
}

# Match any cluster member being in "ClusterXL Inactive or Machine is Down" state
# 1 (local)  10.11.2.21      0%              ClusterXL Inactive or Machine is Down
/^\d+\s.*ClusterXL Inactive or Machine is Down/ {
    foundUnhealthyState="true"
}

# Match any cluster member being in "Active Attention" state
# 1 (local)  10.11.2.21      0%              Active Attention
/^\d+\s.*Active Attention/ {
    foundUnhealthyState="true"
}

# Match local cluster member not having cluster services started
# HA module not started.
/HA module not started./ {
    foundUnhealthyState="true"
}

# 1 (local)  10.11.2.21      0%              Backup
/^\d+\s.*Backup/ {
    foundUnhealthyState="true"
}


# Record all cluster members states into array
# Match any cluster state lines
/^\d+\s.*\d+\.\d+\.\d+\.\d+\s+/ {

	iallmembers++
	state=$NF
	assignedLoad=$(NF-1)
	uniqueIp=$(NF-2)
	
	if ($0 ~ /Active Attention/) {
		state="Active Attention"
		assignedLoad=$(NF-2)
		uniqueIp=$(NF-3)
	}
	
	if ($0 ~ /ClusterXL Inactive/) {
		state="ClusterXL Inactive or Machine is Down"
		assignedLoad="0%"
		uniqueIp=$(NF-7)
	}
	
	allmembers[iallmembers, "state-description"] = state
	allmembers[iallmembers, "id"] = $1
	allmembers[iallmembers, "assigned-load-percentage"] = assignedLoad
	allmembers[iallmembers, "unique-ip"] = uniqueIp
	
	if ( $2 == "\(local\)") {
		allmembers[iallmembers, "is-local"] = 1
	} else {
		allmembers[iallmembers, "is-local"] = 0	
	}
}

END {

	#dumpClusterStateData()
	# Dump data of last VS
    if (vsid != "") {
        dumpClusterStateData()
    }
}
