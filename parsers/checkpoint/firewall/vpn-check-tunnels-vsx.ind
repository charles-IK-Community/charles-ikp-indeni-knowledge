#! META
name: cpvsx-vpn-check-tunnels-vsx
description: lalal
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    os.name: gaia
    role-firewall: "true"
    vsx: "true"

#! COMMENTS
vpn-tunnel-state:
    skip-documentation: true

#! REMOTE::SSH
 nice -n 15 vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && vsx stat $id && (nice -n 15 cat $FWDIR/database/communities_objects.C  && nice -n 15 cat $FWDIR/database/netobj_objects.C && ifconfig && grep ":MySICname" $CPDIR/registry/HKLM_registry.data && echo -e "2\nQ\nQ" | nice -n 15 vpn tu); done


#! PARSER::AWK

############
# Why: Check the status of VPN tunnels, both permanent and non-permanent.
# How: Get data on what should be connected from communities_objects.C and netobj_objects.C  and compare to what is connected with "vpn tu".
###########

function setNameSetData () {
	# This function extracts the name of the data, and the data, from the data format in checkpoint .C files
	# example: 
	# :ipaddr (1.2.3.4)
	# :floodgate (not-installed)
	dataName = $1
	gsub(":", "", dataName)

	# Set the data variable
	data = $2
	gsub("\\(", "", data)
	gsub("\\)", "", data)
	gsub("\"", "", data)
}

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}


function dumpVPNdata () {

	# Add tags with vsname and vsid
	addVsTags(t)
	
	### Structuring data ###
	
	# Put the IP adresses that were unstructured recorded in array "gatewayIPs", into the "gateways" array using the gateway name as key and all IPs as the data separated by comma
	# New array created below. gateways[Gateway1] = 1.1.1.1,2.2.2.2,3.3.3.3
	for (id in gatewayIPs) {
		split(gatewayIPs[id], ipArr, ",") # GW,IP
		if (ipArr[1] in gateways) {
			gateways[ipArr[1]] = gateways[ipArr[1]] "," ipArr[2]
		} else {
			gateways[ipArr[1]] = ipArr[2]
		}
	}
	
	# This array is no longer needed
	delete gatewayIPs
	
	
	# Put the IP adresses recorded for cluster members into the cluster object in the "gateways" array
	# Then remove the cluster members from the "gateways" array
	for (id in gateways) {
		split(gateways[id], ipArr, ",") #[1] = 1.1.1.1  [2] = 2.2.2.2  [3] = 3.3.3.3
		if (id in clusters) { #clusters[Gateway-01] = GatewayCluster
			# This is a cluster member, and the cluster name is "clusters[id]"
			# We now need to append all IPs from the cluster member to the cluster
			start = 1
			end = arraylen(ipArr)
			while (end >= start) {
				gateways[clusters[id]] = gateways[clusters[id]] "," ipArr[end]
				end--
			}	
			# Deleting part of array containing the cluster member
			delete gateways[id]
		}
	}
	
	# Calculate which actual tunnels there should be, from the community setting
	# All gateways in tunnelParticipant should have a tunnel to all gateways in tunnelSatellite for each community
	# If meshedInCenter is true, then all gateways in tunnelParticipant should have tunnels to each other as well
	for (id in tunnelParticipant) { # example: tunnelParticipant["Community1"] = GW1,GW2,GW3
		split(tunnelParticipant[id], tunnelParticipantSplitArr, ",")
		for (id2 in tunnelParticipantSplitArr) { # example: tunnelParticipantSplitArr["1"] = GW1
			split(tunnelSatellite[id], tunnelSatelliteSplitArr, ",")
			for (id3 in tunnelSatelliteSplitArr) { # example: tunnelSatelliteSplitArr["1"] = GW2
				if ( (tunnelParticipantSplitArr[id2] != "" && tunnelSatelliteSplitArr[id3] != "") && (tunnelParticipantSplitArr[id2] == localClusterName || tunnelSatelliteSplitArr[id3] == localClusterName) ) { # Need to have two values to continue and only write tunnels that the device is member of
					configuredTunnelMembers[tunnelParticipantSplitArr[id2] "," tunnelSatelliteSplitArr[id3]] = "" # example: configuredTunnelMembers["GW1,GW2"] = ""
					configuredTunnelsCommunity[tunnelParticipantSplitArr[id2] "," tunnelSatelliteSplitArr[id3]] = id # example: configuredTunnelMembers[GW1,GW2] = Community1
				}
			}
		
			if (meshedInCenter[id] == "true") {
				# We need to mesh the Participant gateways as well.
				for (id4 in tunnelParticipantSplitArr) {
					if ( (tunnelParticipantSplitArr[id2] != tunnelParticipantSplitArr[id4]) && (tunnelParticipantSplitArr[id2] == localClusterName || tunnelParticipantSplitArr[id4] == localClusterName) ) { #The device cannot have a tunnel to itself. Also do not write tunnels that the device is not member of
						if (tunnelParticipantSplitArr[id2] != "" && tunnelParticipantSplitArr[id4] != "") { # Need to have two values to continue
							
							# Make sure that gateways are sorted in the same way every time. Otherwise we will end up with one tunnel "GW1,GW2" and another tunnel "GW2,GW1"
							if (tunnelParticipantSplitArr[id2] > tunnelParticipantSplitArr[id4]) {
								configuredTunnelMembers[tunnelParticipantSplitArr[id2] "," tunnelParticipantSplitArr[id4]] = ""
								configuredTunnelsCommunity[tunnelParticipantSplitArr[id2] "," tunnelParticipantSplitArr[id4]] = id
							} else {
								configuredTunnelMembers[tunnelParticipantSplitArr[id4] "," tunnelParticipantSplitArr[id2]] = ""
								configuredTunnelsCommunity[tunnelParticipantSplitArr[id4] "," tunnelParticipantSplitArr[id2]] = id
							}
						}
					}
				}
			}
		}
	}
	
	
	# Phew. Now there is a array called "configuredTunnelMembers" with a key in the form of "GW1,GW2" and an array called configuredTunnelsCommunity with the same key and data "COMMUNITY_NAME"
	
	# Determining if the tunnel is permanent. Can be "per_gateway", "per_tunnel", "community_level" or "none"
	for (id in configuredTunnelMembers) { #example: configuredTunnelMembers["GW1,GW2"] = ""
		split(id, configuredTunnelMembersSplitArr, ",")
		if (permanentTunnelsType[configuredTunnelsCommunity[id]] == "community_level") { # If the type is "community_level" then all tunnels for this community is permanent
		
			configuredTunnelsPermanent[id] = "true" #example: configuredTunnelsPermanent["GW1,GW2"] = true
		
		} else if (permanentTunnelsType[configuredTunnelsCommunity[id]] == "per_gateway") { # If the type is "per_gateway", then all tunnels containing any of the specified gateways are permanent
			
			for (id2 in permanentTunnelPerGateway) { #example: permanentTunnelPerGateway[1] = GW1
				for (id3 in configuredTunnelMembersSplitArr) { # example: configuredTunnelMembersSplitArr[1] = GW2
 					if (configuredTunnelMembersSplitArr[id3] == permanentTunnelPerGateway[id2]) {
						configuredTunnelsPermanent[id] = "true"
					}
				}
			}
		
		} else if (permanentTunnelsType[configuredTunnelsCommunity[id]] == "per_tunnel") { # If the tunnel is "per_tunnel", then the tunnel needs to match both a gateway and a peer for the same communtiy name
			for (id2 in configuredTunnelMembersSplitArr) { # example: configuredTunnelMembersSplitArr[1] = GW2
				# There are two gateways in configuredTunnelMembersSplitArr. If we do not have a match after testing both, reset variables and try the next tunnel.
				if (nrRuns == 2) {
					matrixMatchGateway = 0
					matrixMatchPeer = 0
					nrRuns = 0
				}
				# We need to find a match both in the permanentTunnelsMatrixGateway and permanentTunnelsMatrixPeersSplitArr
				for (id3 in permanentTunnelsMatrixGateway) { # example: permanentTunnelsMatrixGateway[1] = GW1
					# Check if the gateway and community matches
					if (permanentTunnelsMatrixGateway[id3] == configuredTunnelMembersSplitArr[id2] && permanentTunnelsMatrixCommunity[id3] == configuredTunnelsCommunity[id] ) {
						matrixMatchGateway++
					}
				}
				
				for (id4 in permanentTunnelsMatrixPeers) { # example: permanentTunnelsMatrixGateway[1] = GW1,GW2
					split(permanentTunnelsMatrixPeers[id4],permanentTunnelsMatrixPeersSplitArr,",")
					for (id5 in permanentTunnelsMatrixPeersSplitArr) { # example: permanentTunnelsMatrixPeersSplitArr[1] = GW1
						# Check if the gateway and community matches
						if (permanentTunnelsMatrixPeersSplitArr[id5] == configuredTunnelMembersSplitArr[id2] && permanentTunnelsMatrixCommunity[id4] == configuredTunnelsCommunity[id] ) {
							matrixMatchPeer++
						}
					}
				}
				if (matrixMatchGateway == 1 && matrixMatchPeer == 1) {
					configuredTunnelsPermanent[id] = "true" # example: configuredTunnelsPermanent["GW1,GW2"] = true
				}
				nrRuns++
			}
			
			
		} else if (permanentTunnelsType[configuredTunnelsCommunity[id]] == "none") {
			# Do nothing, since the tunnel is not permanent
		}
	}
	

	# Remove the local peer from the configuredTunnelMembers and configuredTunnelsCommunity array key
	for (id in configuredTunnelMembers) {
		newId = id
		gsub(localClusterName, "", newId)
		gsub(/,,/, "", newId)	
		gsub(/^,/, "", newId)
		gsub(/,$/, "", newId)
		configuredTunnelMembers[newId] = ""
		configuredTunnelsCommunity[newId] = configuredTunnelsCommunity[id]
		configuredTunnelsPermanent[newId] = configuredTunnelsPermanent[id]
		
		delete configuredTunnelMembers[id]
		delete configuredTunnelsCommunity[id]
		delete configuredTunnelsPermanent[id]
	}
	
	# Try to match each configured tunnel to an active connection. From the "vpn tu" output we dont know which community is up, only an IP of a remote gateway that has a tunnel that is up
	# It will match the IPs recorded for all gateway objects against the IPs that there are tunnels up to, to try to match an object to each tunnel.
	# Populate remotePeerStatus[GATEWAY] = 1 for each gateway that has an active tunnel
	
	for (id in gateways) { # example: gateways[GW1] = 1.1.1.1,2.2.2.2,3.3.3.3
		split(gateways[id], gatewaysSplitArr, ",") # IP,IP,IP
		# For each IP in the gatewaysSplitArr, see if it is present in "gatewayHasVPNUp" array
		for (id2 in gatewaysSplitArr) {
			for (id3 in gatewayHasVPNUp) {
				if ( gatewaysSplitArr[id2] == gatewayHasVPNUp[id3]) {
					remotePeerStatus[id] = 1
					tunnelIP[id] = gatewayHasVPNUp[id3]
				}
			
			}
		}
	}
	
	# For each tunnel, check if all gateways included in it are up and then write metric
	for (id in configuredTunnelMembers) {
		t["name"] = configuredTunnelsCommunity[id]
		t["remote-peer-name"] = id
		if (id in tunnelIP) {
				# Try to set it from the tunnel status IP, this is the most reliable way
				t["peerip"] = tunnelIP[id]
		} else if (id in mainVPNIP) {
				# Otherwise take the main IP
				t["peerip"] = mainVPNIP[id]
		} else {
				# If it still empty, set it to a known N/A value.
				t["peerip"] = "0.0.0.0"
		}

		if (configuredTunnelsPermanent[id] == "true") {
			t["always-on"] = "true"
		} else {
			t["always-on"] = "false"
		}

		# Set the state. If no state is found, set to zero.
		state = remotePeerStatus[id]
		if ( state != 0 && state != 1) {
			state = 0
		}
	
		writeDoubleMetricWithLiveConfig("vpn-tunnel-state", t, "gauge", 300, state, "VPN Tunnels", "state", "name")
	}
	
	## clear variables
	# Arrays:
	delete tunnelParticipant
	delete tunnelSatellite
	delete gateways
	delete meshedInCenter
	delete configuredTunnelsCommunity
	delete configuredTunnelMembers
	delete configuredTunnelsPermanent
	delete permanentTunnelsType
	delete permanentTunnels
	delete permanentTunnelsMatrixGateway
	delete permanentTunnelsMatrixCommunity
	delete permanentTunnelsMatrixPeers
	delete remotePeerStatus
	delete sectionArray
	delete vpnPeers
	delete mainVPNIP
	delete clusters
	delete gatewayIPs
	delete gatewayHasVPN
	delete permanentTunnelPerGateway
	delete permanentTunnelPerGatewayCommunity
	
	# Reset counters
	iPermGwList = ""
	iIPs = ""
	iips = ""
	nrRuns = ""
	matrixMatchGateway = ""
	matrixMatchPeer = ""
	
	# Reset section counters
	sectionDepth = ""
	sectionName = ""
	
	# Strings
	localClusterName = ""
	localName = ""
}


BEGIN {
	sectionDepth = 0
}


## VS stuff

# VSID:            0
/VSID:/ {
    # Dump data of previous VS if needed
    if (vsid != "") {
        dumpVPNdata()
    }
	vsid = trim($NF)
}

# Name:            lab-CP-VSXVSLS-1-R7730
/Name:/ {
	vsname = trim($NF)
}

######## parsing C files ########

##############################
# The C files in check point consists of data, stored in sections. Each data has a name and a value. Each section has only a name.
# The sections are in hierarchies, and thus a section can contain multiple sub-section
# The section names can be in different formats, so we match against all of them
# Since it is important to know how far down in the hierarchie we are, we also store that.
# example:
#: (fw-denmark
#		:type (gateway)
#		:custom_fields (
#			: (
#				:custom_field_id (ReferenceObject
#					:Uid ("{839B70D5-7B76-48D2-96CC-FB647074F525}")
#					:Name ("Contact Details")
#					:Table (NO_CPMI_TABLE)
#############################

## Section name sections
## Here we will set the section title and on which level they are
# The section titles have four different formats that we need to take into account

# Name format 1
# :ike_p1 (
/:.+ \($/ {

	sectionTitle = $1
	
	# Removing junk
	gsub(":", "", sectionTitle)
	sectionTitle = trim(sectionTitle)

	# Will count nr of tabs to see on which level we are
	sectionDepth = gsub(/\t/, "")
	sectionDepth++

	# Array to look up name
	sectionArray[sectionDepth] = sectionTitle
}


# Name example 2
# : (MyIntranet
# : (ReferenceObject
/: \(.+$/ {
	
	sectionTitle = $2
	
	# Removing junk
	gsub("\\(", "", sectionTitle)
	sectionTitle = trim(sectionTitle)
	
	# Will count nr of tabs to see on which level we are
	sectionDepth = gsub(/\t/, "")
	sectionDepth++
	
	# Array to look up name
	sectionArray[sectionDepth] = sectionTitle
}



#Name example 3
# :ike_p1_dh_grp (ReferenceObject
# Any line with an ":" followed by any characters then a space, followed by a "(" but not ending with a ")"
/:.+ \(.[^)]*$/ {
	
	sectionTitle = $1
	
	# Removing junk
	gsub(":", "", sectionTitle)
	sectionTitle = trim(sectionTitle)
	
	# Will count nr of tabs to see on which level we are
	sectionDepth = gsub(/\t/, "")
	sectionDepth++	

	# Array to look up name
	sectionArray[sectionDepth] = sectionTitle
}

#Name example 4
# (
/^\($/ {

	# Will count nr of tabs to see on which level we are
	sectionTitle = ""
	sectionDepth = gsub(/\t/, "")
	sectionDepth++	
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
#	)
/\t\)$/ {
	# Tracks which level we are in the sections.
	# We encountered a ) and thus we are one level higher
	sectionDepth--
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
# )
/^\)$/ {
	# Tracks which level we are in the sections.
	# We encountered a ) and thus we are one level higher
	sectionDepth--
}

######## parsing .C files ########

#:permanent_tunnels_def (community_level)
#:permanent_tunnels_def (none)
#:permanent_tunnels_def (per_tunnel)
#:permanent_tunnels_def (per_gateway)
/:permanent_tunnels_def/ {

	# [sectionArray[2]] = Community name, example: "VPN123"
	# data = type of permanent tunnel, example: "per_gateway"
	# set variables "data" and "dataName"
	setNameSetData()
		
	if (data != "none") {
		# Store an array with the community name as key, and the type of permanent tunnel as data
		permanentTunnelsType[sectionArray[2]] = data
	}
}

#: (gateway_peers_list
/: \(gateway_peers_list/ {
	# When this line is found a new combination of peer and gateway for the "per_tunnel" matrix is found, and key count needs to be increased.
	iTunnelMatrix++
}

# :Name (FWSEQA01)
/:Name/ {

	# set variables "data" and "dataName"
	setNameSetData()
	
	if (sectionArray[4] == "permanent_tunnel_matrix") {
		# If there is a permanent tunnel matrix, it needs to be collected
		# Each "gateway" has a permanent tunnel to all peers for each "gateway_and_peers" list.
		# :permanent_tunnel_list (
		#                    : (permanent_tunnel_matrix
		#                            :gateway_and_peers (
		#                                    : (gateway_peers_list
		#                                            :peers (
		#                                                    : (ReferenceObject
		#                                                            :Name (fakeGateway2)
		#                                                            :Table (network_objects)
		#                                                            :Uid ("{79C431BF-D8F2-48B6-BD66-EC9294D1CE2A}")
		#                                                    )
		#                                                    : (ReferenceObject
		#                                                            :Name (fakeGateway1)
		#                                                            :Table (network_objects)
		#                                                            :Uid ("{32CEB8DB-B3B9-4C48-9750-2C99D0A4449D}")
		#                                                    )
		#                                            )
		#                                            :gateway (ReferenceObject
		#                                                    :Name (lab-CP-GW1-cluster)
		#                                                    :Table (network_objects)
		#                                                    :Uid ("{B3D4550C-1CB1-44EC-83E0-4D59014C92F9}")
		#                                            )
		#                                            :Uid ("{D6CBE722-EE40-4915-BE07-3C4931CE78B0}")
		#                                    )
		#                                    : (gateway_peers_list
		#                                            :peers (
		#                                                    : (ReferenceObject
		#                                                            :Name (fakeGateway4)
		#                                                            :Table (network_objects)
		#                                                            :Uid ("{C6CFC3E2-60F0-4B02-A105-A1382193CE90}")
		#                                                    )
		#                                            )
		#                                            :gateway (ReferenceObject
		#                                                    :Name (fakeGateway3)
		#                                                    :Table (network_objects)
		#                                                    :Uid ("{68F599CA-D139-4802-ADAE-5C653D0CEAE9}")
		#                                            )
		#                                            :Uid ("{D25684CD-F12A-4B7C-9C88-73D9B234B4F4}")
		#                                    )
		#                            )
		#                            :tunnel_down_track (log)
		#                            :tunnel_up_track (log)
		#                            :type (permanent_tunnel_matrix)
		#                            :use_community_track_settings (true)
		#                            :Uid ("{47A41575-6205-4F2E-9F44-C7B3643B16EE}")
		#                    )
		#            )

		if (sectionArray[sectionDepth] == "gateway") { # if the sectionTitle immediately above is called "gateway" when a data with name ":Name" is found
			permanentTunnelsMatrixGateway[iTunnelMatrix] = data # Store which gateway is in this specific tunnel matrix
			permanentTunnelsMatrixCommunity[iTunnelMatrix] = sectionArray[2] # also store the community it belongs to 
		} else if (sectionArray[sectionDepth-1] == "peers") { # if the sectionTitle immediately above is called "peers" when a data with name ":Name" is found
			permanentTunnelsMatrixCommunity[iTunnelMatrix] = sectionArray[2] # store the community it belongs to 
			if (iTunnelMatrix in permanentTunnelsMatrixPeers) { # since peers can have more than one, we need to append them 
				permanentTunnelsMatrixPeers[iTunnelMatrix] = permanentTunnelsMatrixPeers[iTunnelMatrix] "," data
			} else {
				permanentTunnelsMatrixPeers[iTunnelMatrix] = data
			}
		}
	}
	
	if (sectionArray[sectionDepth-2] == "permanent_tunnels_gateway_list") {
		# If there is a permanent tunnel per gateway list, it needs to be collected
		# A gateway specified here has a permanent tunnel to all other gateways in this community.
		# Example:
		# :permanent_tunnel_list (
		#                    : (permanent_tunnels_gateway_list
		#                            :gateway_list (
		#                                    : (ReferenceObject
		#                                            :Name (lab-CP-GW1-cluster)
		#                                            :Table (network_objects)
		#                                            :Uid ("{B3D4550C-1CB1-44EC-83E0-4D59014C92F9}")
		#                                    )
		#                            )
		#                            :tunnel_down_track (log)
		#                            :tunnel_up_track (log)
		#                            :type (permanent_tunnels_gateway_list)
		#                            :use_community_track_settings (true)
		#                            :Uid ("{341B0810-A08C-491E-AB4C-1CB6B09AA702}")
		#                    )
		#            )
	
		# data = gateway name, example: lab-CP-GW5-R7730
		# sectionArray[2] = Community name, example: VPN123
		iPermGwList++
		permanentTunnelPerGateway[iPermGwList] = data
		permanentTunnelPerGatewayCommunity[iPermGwList] = sectionArray[2]
	}
	
	
	# Check if the current Name label is within the sections: "participant_gateways" or "satellite_gateways". If so then it is part of the community
	# Example:
	# :participant_gateways (
    #                    : (ReferenceObject
    #                            :Name (fakeGateway1)
    #                            :Table (network_objects)
    #                            :Uid ("{32CEB8DB-B3B9-4C48-9750-2C99D0A4449D}")
    #                    )
    #                    : (ReferenceObject
    #                            :Name (fakeGateway2)
    #                            :Table (network_objects)
    #                            :Uid ("{79C431BF-D8F2-48B6-BD66-EC9294D1CE2A}")
    #                    )
    #                    : (ReferenceObject
    #                            :Name (lab-CP-GW1-cluster)
    #                            :Table (network_objects)
    #                            :Uid ("{B3D4550C-1CB1-44EC-83E0-4D59014C92F9}")
    #                    )
    #            )

	if (sectionArray[sectionDepth-1] == "participant_gateways" && data != "") {
		# data = gateway name, example: lab-CP-GW5-R7730
		# sectionArray[2] = Community name, example: VPN123
			
		# Using the gateway name as key, write an array that gives "1" if the hostname participate in an VPN tunnel
		# This is so we can look up the gateway easier when searching the netobj_objects.C file
		gatewayHasVPN[data] = 1
		
		# Add all gateways that are participants into array tunnelParticipant that has the community name as key
		if (sectionArray[2] in tunnelParticipant) {
			tunnelParticipant[sectionArray[2]] = tunnelParticipant[sectionArray[2]] "," data
		} else {
			tunnelParticipant[sectionArray[2]] = data
		}
	}
	
	if (sectionArray[sectionDepth-1] == "satellite_gateways" && data != "") {
		# data = gateway name, example: lab-CP-GW5-R7730
		# [sectionArray[2]] = Community name, example: VPN123
			
		# Using the gateway name as key, write an array that gives "1" if the hostname participate in an VPN tunnel
		# This is so we can look up the gateway easier when searching the netobj_objects.C file
		gatewayHasVPN[data] = 1
		
		# Add all gateways that are satellite into array tunnelSatellite that has the community name as key
		if (sectionArray[2] in tunnelSatellite) {
			tunnelSatellite[sectionArray[2]] = tunnelSatellite[sectionArray[2]] "," data
		} else {
			tunnelSatellite[sectionArray[2]] = data
		}
	}
}

#:meshed_in_center (false)
#:meshed_in_center (true)
/:meshed_in_center/ {
	setNameSetData()
	# data = "true" or "false"
	# sectionArray[2] = Community name, example: VPN123
	# If this is true, then the participant gateways should also have tunnels between them
	meshedInCenter[sectionArray[2]] = data
}

# :topology (meshed)
# :topology (star)
/:topology / {
	setNameSetData()
	# data = "meshed" or "star"
	# sectionArray[2] = Community name, example: VPN123
	# If topology is meshed, then the participant gateways should also have tunnels between them
	if (data == "meshed") {
		meshedInCenter[sectionArray[2]] = "true"
	}
}

# :refname ("#_lab-CP-VSXVSLS-1-R7730")
/refname/ {
	# set variables "data" and "dataName"
	setNameSetData()
	
	# if the refname is in a section called "cluster_members" then this is relevant
	if (sectionArray[sectionDepth-1] == "cluster_members") {
		gsub(/#_/,"",data)
		# Array with GATEWAY-NAME as key, and the cluster hostname as data
		clusters[data] = sectionArray[2]  #clusters[Gateway-01] = GatewayCluster
	}
}


# :ipaddr (10.11.2.21)
/:ipaddr/ {
	setNameSetData()

	# Get IPs from all objects, both from interfacs and "main IP". But only if the gateway is member of a VPN tunnel
	if ( (sectionArray[sectionDepth-1] == "interfaces" ||  sectionDepth == 2) && gatewayHasVPN[sectionArray[2]] == 1) {
	
	# data = ip address, example: 1.1.1.1
		# sectionArray[2] = gateway name, example: lab-CP-GW5-R7730
		
		# Record IP adresses of gateways into the array gatewayIPs
		iIPs++
		gatewayIPs[iIPs] = sectionArray[2] "," data
		
		if ( sectionDepth == 2 ) {
			# This catches the "main IP", which could be useful if the VPN selection is based on that
			mainVPNIP[sectionArray[2]] = data
		}
	}
}

#:ip_resolution_mechanism_GW (mainIpVpn)
/:ip_resolution_mechanism_GW/ {
	setNameSetData()
	
	# To get how the IP used for VPN us looked up. For now we will use main IP, or value from "single_VPN_IP" if it exists
	# :ip_resolution_mechanism_GW (mainIpVpn) = use normal MainIP
	# :ip_resolution_mechanism_GW (singleIpVpn) = use data from :single_VPN_IP (IP)
	# :ip_resolution_mechanism_GW (singleNATIpVPN) = use data from :single_VPN_IP (IP)
	#
	# More advanced ones:
	#:ip_resolution_mechanism_GW (topologyCalc)
	# :ip_resolution_mechanism_GW (dnsLookup) - use from :dns_IP_resolution_GW (DNS-RECORD)
	# :ip_resolution_mechanism_GW (ongoingProb)
}

#:single_VPN_IP (1.2.3.4)
/:single_VPN_IP/ {
	setNameSetData()
	# data = ip address, example: 1.1.1.1
	# [sectionArray[2]] = gateway name, example: lab-CP-GW5-R7730
	
	# If this data is set, then this is used as source IP for VPNs
	mainVPNIP[sectionArray[2]] = data
}


#  :MySICname ("CN=lab-CP-GW2-R7730,O=lab-CP-MGMT-R7730..o2sn6g")
/MySICname/ {
	# To determine hostname
	
	# extract hostname and remove junk
	split($2, hostnameArr, ",")
	gsub(/\(|\"|=|CN/, "", hostnameArr[1])
	
	# Set both local hostname and the name of the cluster the host belongs to
	localName = hostnameArr[1]
	if (localName in clusters) {
		localClusterName = clusters[localName]
	} else {
		localClusterName = localName
	}
}

######## Parse vpn tu ########

# Peer  197.45.79.20 SAs:
/Peer.*SAs/ {
    peer = $2
    # For each peer we'll look to see that we found at least one SPI for inbound, and one for outbound
    foundinbound = "false"
    foundoutbound = "false"
    section = ""
}

# INBOUND:
/INBOUND/ {
    section = "INBOUND"
}

# OUTBOUND:
/OUTBOUND/ {
    section = "OUTBOUND"
}

#  1. 0xe61b825f   (i: 0)
/0x[a-z0-9]+/ {
    if (peer != "") {
        if (section == "INBOUND") {
            foundinbound="true"
        }
        if (section == "OUTBOUND") {
            foundoutbound="true"
        }

        # If we found both inbound and outbound, write the metric
        if (foundinbound == "true" && foundoutbound == "true") {
            ipeers++
			gatewayHasVPNUp[ipeers] = peer

            # Reset the peer so we don't get back in here
            peer = ""
        }
    }
}

######## END tasks ########

END {
	# Dump data of last VS
    if (vsid != "") {
        dumpVPNdata()
    }
}

