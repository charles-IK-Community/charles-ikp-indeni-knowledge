#! META
name: cpmds-cpca_client-lscert
description: Show list of certificates on the management (VPN, SIC etc) in MDS
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    os.name: gaia
    role-management: true
    mds: true
    vsx: true

#! COMMENTS
certificate-expiration:
    skip-documentation: true

#! REMOTE::SSH
COLUMNS=150 && export COLUMNS && nice -n 15 mdsstat |grep CMA | awk '{gsub(/\|/,"",$3); print $3}' | while read name; do mdsenv $name && nice -n 15 mdsstat $name && nice -n 15 cpca_client lscert; done

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.ip"] = vsIp
    tags["vs.name"] = vsName
}

# | CMA |MDM-VSX_Management_Server | 10.10.6.14      | up 1531    | up 1616  | up 1493  | up 1720  |
/^\| CMA \|/ {
	vsName=$3
	vsIp=$5
	
	# Remove starting "|" in the name
	gsub(/\|/,"",vsName)
}

# Info for each certificate comes from three lines in a row

# Subject = CN=lab-CPVSXR7730-1_VSW,O=lab-CPMGMTR7730..9uifq5
/Subject/ {
	split($0,nameArray,"=")
	name=nameArray[3]
	o=nameArray[4]
	gsub(/,O/,"",name)
}

# Status = Valid   Kind = SIC   Serial = 2562   DP = 0
/Status/ {
	status=$3
	kind=$6
}

# Not_Before: Sat Jul  9 11:21:26 2016   Not_After: Fri Jul  9 11:21:26 2021
/Not_Before/ {
	expire_month=$9
	expire_day=$10
	expire_time=$11
	expire_year=$12

	certTags["name"] = name
	
	addVsTags(certTags)
	writeDoubleMetricWithLiveConfig("certificate-expiration",certTags,"gauge",300,date(expire_year,parseMonthThreeLetter(expire_month),expire_day), "Certificate Expiration", "date", "name")
}
