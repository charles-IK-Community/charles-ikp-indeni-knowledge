#! META
name: chkp-check-SIC
description: check the status of the SIC for all the gateways
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: checkpoint
    mds:
        neq: true
    role-management: true

#! COMMENTS
trust-connection-state:
    why: |
        A SIC connection is established between a device and its management server. This communication can fail due to revoked or expired SIC certificates, or due to connection issues. Often this is not noticed until a policy push fails, or logs stops coming in to the management server.
    how: |
        By using the Check Point built-in "cpstat" command, an attempt is made to retrieve data over the SIC channel.
    without-indeni: |
        An administrator could login and manually run the command, or try "Test SIC" from the SmartDashboard
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The most common way to test SIC is by doing "Test SIC" from the device object in SmartDashboard.

#! REMOTE::SSH
nice -n 15 cat $FWDIR/database/netobj_objects.C && netstat -anpt 2>&1 |egrep -o '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+:18192' |egrep -v "127.0.0.1:18192|0.0.0.0:18192" | sort -u |awk '{gsub(/:18192/,"",$0); print "cpstat -h " $1 " os"}' | (while read cmd; do { echo "Process $cmd started"; ($cmd && echo $cmd) & sleep 0.1 ; pid=$(ps aux|grep "$cmd" |grep -v "grep" |awk '{print $2}') ;  PID_LIST+=" $pid"; } done ; sleep 20; for id in $PID_LIST ; do  kill $id; done ) ; sleep 1; echo

#! PARSER::AWK

############
# Script explanation:
# Step 1: Outputs the netobj_objects.C. So we can determine all hosts that should have a SIC connection.
# Step 2: For each connection to another host on port 18192 (checked with netstat), run "cpstat -h $IP os" and see if the connection is successfull.
# Step 3: Check if any connections are unsuccessfull, as well as if any connections are missing from the netstat list.
# Caveats: The cpstat command has long timeout, we will run it in paralell to reduce the risk that the script take too long.
# Since the timeout of cpstat is 35s we cannot wait for it to finish. Therefore we also need to kill all processes we started
# after 20 seconds.
###########

## Populate list with all objects in netobj_objects.C that has a SIC. Since we do not know if the SIC is working for them, we set them to 0

# : (lab-CP-GW5-R7710
/^\t: \(.+$/ { 
	if ( hostname && sic == 1) { 
		status = 0
		statusArr[tmpIpAddress] = status " " hostname
	}
	tmpIpAddresses = ""
	ip = ""; 
	sic = 0; 
	#ipCount = 0; 
	#vsMember = 0
	hostname=$0; 
	gsub(/\t\: \(/,"",hostname) } 
	
# :sic_name ("CN=lab-CP-GW2-R7730,O=lab-CP-MGMT-R7730_Management_Server..nt8cac")	
/:sic_name/ {
	# SIC conneciton found. This means that the last hostname found is relevant.
	sic = 1
} 

# :ipaddr (10.10.6.22)
/^\t\t:ipaddr \(|:mgmt_ip/ {
	# Record main IP for the above host.
	# Only catches main or mgmt IP
	ip = $2
	gsub(/\(|\)/,"",ip)
	if (ip != "0.0.0.0") {
		tmpIpAddress = ip
	}
}

#)
/^\)$/ {
	# End of $FWDIR/database/netobj_objects.C, need to write last entry

	if ( hostname && sic == 1) { 
		status = 0
		statusArr[tmpIpAddress] = status " " hostname
	}
}

## Collect the status of the cpstat command run.

# Product Name:                  SVN Foundation
/Product Name:/ {
	# Successful connection with cpstat, but not clear to which host yet.
	status = 1
}

# cpstat -h 10.10.6.10 os
/^cpstat -h/ {
	# Its now clear which IP above result is for.
	ip = $3
	if (status == 1) {
		if (ip in statusArr) {
			split(statusArr[ip],splitArr," ")
			hostname = splitArr[2]
			statusArr[ip] = status " " hostname
		}
		status = 0
	}
}


END {
	# Write metric
	for (id in statusArr) {
		split(statusArr[id],splitArr," ")
		t["name"] = splitArr[2]
		t["ip"] = id
		status = splitArr[1]
		writeDoubleMetric("trust-connection-state", t, "gauge", 300, status)
	}
}
