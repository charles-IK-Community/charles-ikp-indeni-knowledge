#! META
name: bluecoat-diagnostics-hardware-info
description: fetch status of hardware
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: bluecoat
    "os.name": sgos

#! REMOTE::HTTP
url: "/Diagnostics/Hardware/Info?stats_mode=0"
protocol: HTTPS

#! PARSER::AWK
BEGIN {
    
}


# Storage: 2 drives
/^Storage/ { FS=":" } # In the storage section, we use ":"

#	Disk in slot 1: 214 GB VMware   Virtual disk    , rev:1.0  serial: status:present
#	Disk in slot 2: 214 GB VMware   Virtual disk    , rev:1.0  serial: status:present
/^\s*Disk.*status/ {
	gsub(/^[ \t]+/, "", $1) # Trim whitespace from start of line
	match($1, "[0-9]+")
	disktags["physical-drive-id"] = substr($1,RSTART)
	disktags["physical-drive-name"] = $1
	disktags["physical-drive-status-description"] = substr($NF, 1, length($NF))
	isUp = (match($NF, "present") ? "1.0" : "0.0")

	writeDoubleMetricWithLiveConfig("physical-drive-ok", disktags, "gauge", "60", isUp, "Physical Disks", "state", "physical-drive-id|physical-drive-name")
}


# Network:
/^Network/ { FS=":" } # In the Network section, we use ":"

# 	Interface 0:0: Intel Gigabit     copper running at 1 Gbps full duplex (MAC 00:50:56:a0:f3:74)
#	Interface 1:0: Intel Gigabit     copper running at 1 Gbps full duplex (MAC 00:50:56:a0:fa:b7)
/^\s*Interface/ {
	gsub(/^[ \t]+/, "", $1) # Trim whitespace from start of line
	# Add speed information if available
	speed=""
	duplex=""

    if (match($0, "at [0-9]+ .bps")) {
    	speed=substr($0, RSTART+3, RLENGTH-3) # Remove "at "
    	sub(/ /, "", speed)
    	sub(/bps/, "", speed)
    } else { speed="Unknown" }

	if (match($0,  "duplex")) {
		duplex=(match($0, " full ") ? "full" : "half")
	} else { duplex="Unknown" }

	interfacetags["name"] = $1 " " $2

	writeComplexMetricString("network-interface-speed", interfacetags, speed)
	writeComplexMetricString("network-interface-duplex", interfacetags, duplex)
}
