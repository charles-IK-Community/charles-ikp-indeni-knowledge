#/bin/bash

#Save the path of the current command runner
COMMANDRUNNERPATH=$(dirname `which command-runner.sh` 2> /dev/null)

#Check that the path contains command-runner.sh and advise the user to configure it if it doesn't
if [ -z "$COMMANDRUNNERPATH" ];then
        echo "command-runner.sh is not in the path"
        echo "Please update your \$PATH variable with command-runner.sh"
        exit
fi

#Check that no indeni files exists, this would indicate that the repository is in the same directory as the command-runner
HASINDENIFILES=`find $COMMANDRUNNERPATH -name *.ind`

if [ ! -z "$HASINDENIFILES" ];then
        echo "It looks like you have the indeni repository in the command-runner directory, please move it to it's own directory and update the path"
        exit
fi

#Where to keep old versions
OLDDIR=$COMMANDRUNNERPATH/previousversions

#Save the destination directory as $OLDDIR/YYYY-MM-DD-HHMMSS
DESTINATION="$OLDDIR/"`date +"%Y-%m-%d-%H%M%S"`

#Where to store the zip file containing the new version
TEMPFILE="/tmp/commandrunner.zip"

#Make sure that the user knows where things ends up
echo ""
echo "******************************************************************************************************"
echo "*"
echo "*         This script will:"
echo "*         1. Backup the directory containing command-runner.sh"
echo "*         2. Replace the existing version with the latest version of commandrunner"
echo "*"
echo "******************************************************************************************************"
echo ""
echo "Current directory of command-runner.sh: $COMMANDRUNNERPATH"
echo "Directory where the backup will be stored: $DESTINATION"
echo ""

read -r -p "Do you wish to proceed? [y/N] " response

case $response in
    [yY][eE][sS]|[yY])
        echo "Updating command-runner"
        ;;
    *)
        exit
        ;;
esac

#Create directories if needed
if [ ! -d "$OLDDIR" ]; then

        echo "Directory for storing old versions does not exist, creating $OLDDIR"
        mkdir $OLDDIR

fi

FILESMOVED=0

if [ `ls -1 $CURRENTDIR 2> /dev/null | wc -l` -gt 0 ]; then

        #Save the destination directory as $OLDDIR/YYYY-MM-DD-HHMMSS
        DESTINATION="$OLDDIR/"`date +"%Y-%m-%d-%H%M%S"`

        #Create destination directory
        echo "Creating directory $DESTINATION for backing up the old version"
        mkdir $DESTINATION

        #Move the old version
        echo "Moving files to $DESTINATION"
        find $COMMANDRUNNERPATH/* ! -path "$COMMANDRUNNERPATH/previousversions*" -exec mv {} $DESTINATION \; 2> /dev/null

        FILESMOVED=1
fi

#Get the new version and save it to $TEMPFILE
echo "Downloading the latest command-runner"
wget -q --show-progress "https://indeni.atlassian.net/wiki/download/attachments/59342857/command-runner.zip?api=v2" -O $TEMPFILE

#Unzip the tempfile
echo "Unpacking command-runner from the zip file"
unzip -qq $TEMPFILE -d $COMMANDRUNNERPATH 2> /dev/null

#Make sure that we actually managed to unzip some files
if [ `ls -1 $COMMANDRUNNERPATH/command-runner*   2> /dev/null | wc -l` -gt 0 ]; then

        #Getting the directory of the new version
        TEMPDIR=`find $COMMANDRUNNERPATH/command-runner* -maxdepth 0 ! -path "$COMMANDRUNNERPATH/previousversions*"`
	
	#Checking that the tempoerary directory is not empty
        if [ -z $TEMPDIR ];then
                echo "No command-runner directory found"
                rm $TEMPFILE
                echo "Command-runner was NOT updated"
                exit
        fi

        #Moving the content of the sub directory to the currentdir
        mv $TEMPDIR/* $COMMANDRUNNERPATH

        #Removing the sub directory
        rmdir $TEMPDIR

        #Removing the temp file
        rm $TEMPFILE
        echo "Command-runner has been updated"

else

        if [ $FILESMOVED == 1 ]; then

                echo "No files seems to have been unpacked, rolling back"

                #Roll back the backup
                mv $DESTINATION/* $COMMANDRUNNERPATH

                #Remove the backup directory
                rmdir $DESTINATION

                #Remove the temp file
                rm $TEMPFILE

        else

                echo "No files seems to have been unpacked"
                rm $TEMPFILE

        fi

        echo "Command-runner was NOT updated"

fi
